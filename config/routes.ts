﻿export default [
  {
    path: '/',
    component: './web',
    layout: false,
  },
  {
    path: '/user',
    layout: false,
    routes: [
      {
        path: '/user',
        routes: [
          {
            name: 'login',
            path: '/user/login',
            component: './user/Login',
          },
        ],
      },
      {
        component: './404',
      },
    ],
  },
  {
    path: '/home',
    name: 'home',
    icon: 'home',
    component: './home',
  },
  {
    name: 'applyMgt',
    path: '/applyMgt',
    icon: 'table',
    component: '../layouts/BasicLayout',
    hideChildrenInMenu:true,
    routes: [
      {
        path: '/applyMgt/serviceConfig',
        component: './applyMgt/serviceConfig',
        icon: 'icon-xiangmu',
        name: 'serviceConfig',
      },
      {
        path: '/applyMgt/bizObj',
        component: './applyMgt/businessObject',
        icon: 'icon-fuzhidaima1',
        name: 'bizObj',
      },
      {
        path: '/applyMgt/bizObj/list',
        component: './applyMgt/businessObject/list',
      },
      {
        path: '/applyMgt/bizObj/optionList',
        component: './applyMgt/businessObject/optionList',
      },
      {
        path: '/applyMgt/design',
        component: './design',
        icon: 'icon-xiangmu',
        name: 'bizObj',
      },
      {
        component: './404',
      },
    ],
  },
  {
    path: '/Apply',
    name: 'apply',
    icon: 'apply',
    component: './Apply',
  },
];
