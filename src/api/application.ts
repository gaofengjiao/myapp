import { local } from './_local';

export const serviceConfigUrls = {
  serviceList: `${local}/a/list`,
  // 模型分类
  // 查询模型、选项集分类
  modelTypeList: `/api/model/type/list`,
  // 查询模型图
  modelTypeDiagramDetail: `/api/model/type/diagram/detail`,
  // 保存模型、选项集分类
  modelTypeDiagramSave: `/api/model/type/save`,
  // 业务对象
  modelBusinessSave: `/api/model/business/save`,
  // 选项集
  // 查询选项集详情
  modelTypeDetail: `/api/model/type/detail`,
  // 保存选项集
  modelOptionSave: `/api/model/option/save`,
  // 查询选项集列表
  modelOptionList: `/api/model/option/list`,
  // 业务对象关系
  modelBusinessRelationSave: `/api/model/business/relation/save`,
  modelBusinessRelationList: `/api/model/business/relation/list`,
  modelBusinessAttrList: `/api/model/business/attr/list`,
  modelBusinessAttrSave: `/api/model/business/attr/save`,
};
