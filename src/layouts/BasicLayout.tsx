import ProLayout from '@ant-design/pro-layout';
import { Link } from 'umi';

const BasicLayout: React.FC = (props) => {
  const { children } = props;
  const isLocalEnv = process.env.NODE_ENV === 'development';
  return (
    <ProLayout
      {...props}
      collapsedButtonRender={false}
      menuHeaderRender={false}
      collapsed={true}
      navTheme="light"
      headerRender={false}
      menuItemRender={(menuItemProps, defaultDom) => {
        if (isLocalEnv) {
          if (menuItemProps.isUrl || !menuItemProps.path) {
            return defaultDom;
          }
          return <Link to={menuItemProps.path}>{defaultDom}</Link>;
        }
        return false;
      }}
    >
      {children}
    </ProLayout>
  );
};
export default BasicLayout;
