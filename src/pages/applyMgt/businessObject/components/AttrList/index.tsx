import type { FC, MutableRefObject } from 'react';
import React, { useState, useEffect } from 'react';
import type { ActionType, ProColumns } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import { Button, message, Drawer } from 'antd';
import { modelBusinessAttrList } from '@/services/application/serviceConfig';
import CreateAttr from '../CreateAttr';
import _ from 'lodash';

interface AttrListIProps {
  id: string;
}

const AttrList: FC<AttrListIProps> = (props) => {
  const { id } = props;
  const actionRef = React.createRef<ActionType>() as MutableRefObject<ActionType>;
  const [loading, setLoading] = useState(false);
  const [visibleAttr, setVisibleAttr] = useState(false);
  const [dataSource, setDataSource] = useState<any>([]);
  // const fetchTableData = async (params: any) => {
  //     const newParams = { pageNum: params.current, pageSize: 10, ...params, modelBusinessId: id  };
  //     setLoading(true);
  //     const result = await modelBusinessAttrList(newParams);
  //     if (result) {
  //         setLoading(false);
  //         if (result.status === "0") {
  //             return {
  //                 // data: result.data,
  //                 data,
  //                 total: result.total,
  //             };
  //         }
  //         return message.error(result?.message);

  //     }
  //     return { data: [], success: true, total: null };
  // };
  const columns: ProColumns[] = [
    {
      title: '名称',
      dataIndex: 'modelBusinessAttrName',
    },
    {
      title: '编码',
      dataIndex: 'modelBusinessAttrCode',
    },
    {
      title: '描述',
      dataIndex: 'modelBusinessAttrDescribe',
    },
    {
      title: '组件',
      dataIndex: 'modelBusinessAttrHtmlType',
    },
    {
      title: '是否搜索',
      dataIndex: 'modelBusinessAttrSearchable',
    },
    {
      title: '是否存储',
      dataIndex: 'saveDatabaseField',
    },
    {
      title: '是否必填',
      dataIndex: 'modelBusinessAttrRequired',
    },
  ];
  const onCloseAttr = () => {
    setVisibleAttr(false);
  };
  const handleOnSumbitAttr = (values: any) => {
    const cloneData = _.cloneDeep(dataSource);
    cloneData.push(values);
    setDataSource(cloneData);

    const tempData = localStorage.getItem('modelBusinessList')
      ? JSON.parse(localStorage.getItem('modelBusinessList'))
      : [];
    tempData.map((item: any) => {
      if (item.modelBusinessCode === id) {
        // eslint-disable-next-line no-param-reassign
        item.properties = cloneData;
      }
      return null;
    });
    console.log(tempData, 'tempData');
    localStorage.setItem('modelBusinessList', JSON.stringify(tempData));
    setVisibleAttr(false);
  };
  // 添加属性
  const handleAddAttr = () => {
    setVisibleAttr(true);
  };
  useEffect(() => {
    // 临时
    const tempData = localStorage.getItem('modelBusinessList')
      ? JSON.parse(localStorage.getItem('modelBusinessList'))
      : [];
    const filterData = _.filter(tempData, (item: any) => item.modelBusinessCode === id);
    const data = filterData && filterData.length ? filterData[0].properties : [];
    setDataSource(data);
  }, [id]);
  return (
    <>
      <Button type="link" onClick={handleAddAttr}>
        + 添加属性
      </Button>
      <ProTable
        loading={loading}
        actionRef={actionRef}
        size="small"
        columns={columns}
        bordered
        dataSource={dataSource}
        // request={fetchTableData}
        rowKey={'modelBusinessCode'}
        tableAlertRender={false}
        toolBarRender={false}
        search={false}
        pagination={{
          size: 'default',
          defaultPageSize: 10,
        }}
      />
      <Drawer
        title="添加属性"
        getContainer={false}
        style={{ position: 'absolute' }}
        placement="right"
        closable={false}
        onClose={onCloseAttr}
        visible={visibleAttr}
        width={400}
      >
        <CreateAttr onSumbit={(values: any) => handleOnSumbitAttr(values)} onCancel={onCloseAttr} />
      </Drawer>
    </>
  );
};
export default AttrList;
