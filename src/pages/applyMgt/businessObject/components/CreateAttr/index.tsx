import type { FC } from 'react';
import { useState } from 'react';
import { Form, Input, Button, Space, Radio, Select } from 'antd';
import _ from 'lodash';

const { Option } = Select;
const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

interface CreateAttrIProps {
  onSumbit: any;
  onCancel: () => void;
}
const CreateAttr: FC<CreateAttrIProps> = (props) => {
  const { onSumbit, onCancel } = props;
  const [dicData, setDicData] = useState<any>([]);
  const [form] = Form.useForm();
  const [type, setType] = useState();
  const formTypes = [
    { code: 'Input', name: '单行文本' },
    { code: 'Select', name: '下拉框' },
    { code: 'Radio', name: '单选' },
    { code: 'InputNumber', name: '数值' },
    { code: 'Checkbox', name: '多选' },
  ];
  const onFinish = (values: any) => {
    onSumbit(values);
  };
  const handleOnChange = (value: any) => {
    const modelBusinessAttrCode = form.getFieldValue('modelBusinessAttrCode');
    const tempData = localStorage.getItem('optionDataChildrenList')
      ? JSON.parse(localStorage.getItem('optionDataChildrenList'))
      : [];
    console.log(tempData, 'optionDataChildrenList');
    const tempDicData = _.filter(
      tempData,
      (item: any) => item.modelOptionCode === modelBusinessAttrCode,
    );
    setType(value);
    console.log(tempDicData, 'tempDicData');
    setDicData(tempDicData);
  };
  return (
    <Form {...layout} form={form} onFinish={onFinish}>
      <Form.Item name="modelBusinessAttrName" label="名称" rules={[{ required: true }]}>
        <Input />
      </Form.Item>
      <Form.Item name="modelBusinessAttrCode" label="编码" rules={[{ required: true }]}>
        <Input />
      </Form.Item>
      <Form.Item name="modelBusinessAttrDescribe" label="描述" rules={[{ required: true }]}>
        <Input />
      </Form.Item>
      <Form.Item name="saveDatabaseField" label="是否存储" colon={false}>
        <Radio.Group>
          <Radio value={1}>存储数据库字段</Radio>
          <Radio value={2}>计算字段</Radio>
        </Radio.Group>
      </Form.Item>
      <Form.Item name="modelBusinessAttrRequired" label="是否必填" colon={false}>
        <Radio.Group>
          <Radio value={1}>是</Radio>
          <Radio value={0}>否</Radio>
        </Radio.Group>
      </Form.Item>
      <Form.Item name="modelBusinessAttrSearchable" label="是否可搜索" colon={false}>
        <Radio.Group>
          <Radio value={1}>是</Radio>
          <Radio value={0}>否</Radio>
        </Radio.Group>
      </Form.Item>
      <Form.Item name="modelBusinessAttrHtmlType" label="属性设置" rules={[{ required: true }]}>
        <Select placeholder="请选择" onChange={(value) => handleOnChange(value)}>
          {formTypes.map((item: any) => (
            <Option key={item.code} value={item.code}>
              {item.name}
            </Option>
          ))}
        </Select>
      </Form.Item>
      {type === 'Select' ? (
        <Form.Item name="modelBusinessAttrDicCode" label="值列表" rules={[{ required: true }]}>
          <Select placeholder="请选择">
            {dicData.map((item: any) => (
              <Option key={item.code} value={item.modelOptionCode}>
                {item.modelOptionName}
              </Option>
            ))}
          </Select>
        </Form.Item>
      ) : null}
      {
        <>
          <Form.Item name="width" label="宽度">
            <Input />
          </Form.Item>
          <Form.Item name="height" label="高度">
            <Input />
          </Form.Item>
        </>
      }
      <Form.Item {...tailLayout}>
        <Space>
          <Button type="primary" htmlType="submit">
            创建
          </Button>
          <Button type="default" onClick={onCancel}>
            取消
          </Button>
        </Space>
      </Form.Item>
    </Form>
  );
};
export default CreateAttr;
