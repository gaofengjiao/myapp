import type { FC } from 'react';
import { Form, Input, Button, Space, Radio } from 'antd';

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

interface CreateModelIProps {
  onSumbit: any;
  onCancel: () => void;
}
const CreateModel: FC<CreateModelIProps> = (props) => {
  const { onSumbit, onCancel } = props;
  const [form] = Form.useForm();
  const onFinish = (values: any) => {
    onSumbit(values);
  };
  return (
    <Form {...layout} form={form} onFinish={onFinish}>
      <Form.Item name="modelBusinessName" label="名称" rules={[{ required: true }]}>
        <Input />
      </Form.Item>
      <Form.Item name="modelBusinessCode" label="编码" rules={[{ required: true }]}>
        <Input />
      </Form.Item>
      <Form.Item name="modelBusinessDescribe" label="描述">
        <Input />
      </Form.Item>
      <Form.Item name="saveDatabase" label="是否存储数据库" colon={false}>
        <Radio.Group>
          <Radio value={1}>是</Radio>
          <Radio value={0}>否</Radio>
        </Radio.Group>
      </Form.Item>
      <Form.Item name="optimisticLockVersion" label="是否乐观锁定版本" colon={false}>
        <Radio.Group>
          <Radio value={1}>是</Radio>
          <Radio value={0}>否</Radio>
        </Radio.Group>
      </Form.Item>
      <Form.Item name="X" label="位置X" rules={[{ required: true }]}>
        <Input />
      </Form.Item>
      <Form.Item name="Y" label="位置Y" rules={[{ required: true }]}>
        <Input />
      </Form.Item>
      <Form.Item {...tailLayout}>
        <Space>
          <Button type="primary" htmlType="submit">
            创建
          </Button>
          <Button type="default" onClick={onCancel}>
            取消
          </Button>
        </Space>
      </Form.Item>
    </Form>
  );
};
export default CreateModel;
