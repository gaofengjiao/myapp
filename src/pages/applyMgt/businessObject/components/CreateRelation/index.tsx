import type { FC } from 'react';
import { useEffect, useState } from 'react';
import { Form, Button, Space, Radio, Select } from 'antd';
import _ from 'lodash';

const { Option } = Select;

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

interface CreateRelationIProps {
  onSumbit: any;
  onCancel: () => void;
}
const CreateRelation: FC<CreateRelationIProps> = (props) => {
  const { onSumbit, onCancel } = props;
  const [form] = Form.useForm();
  const onFinish = (values: any) => {
    onSumbit(values);
  };
  const [modelBusinessData, setModelBusinessData] = useState<any>([]);
  const [modelBusinessAttrData, setModelBusinessAttrData] = useState<any>([]);
  useEffect(() => {
    const data: any = localStorage.getItem('modelBusinessList')
      ? JSON.parse(localStorage.getItem('modelBusinessList'))
      : [];
    setModelBusinessData(data);
  }, []);

  const handleChangeModel = (value: any) => {
    const modelBusinessAttrFitterData: any = _.filter(
      modelBusinessData,
      (item: any) => item.modelBusinessCode === value,
    );
    const tempData =
      modelBusinessAttrFitterData && modelBusinessAttrFitterData.length
        ? modelBusinessAttrFitterData[0].properties
        : [];
    setModelBusinessAttrData(tempData);
  };

  return (
    <Form {...layout} form={form} onFinish={onFinish}>
      <Form.Item name="relationBusinessId" label="关联对象ID" rules={[{ required: true }]}>
        <Select placeholder="请选择" onChange={(value: any) => handleChangeModel(value)}>
          {modelBusinessData.map((item: any) => (
            <Option key={item.modelBusinessCode} value={item.modelBusinessCode}>
              {item.modelBusinessName}
            </Option>
          ))}
        </Select>
      </Form.Item>
      <Form.Item name="relationBusinessAttrId" label="关联对象属性ID">
        <Select placeholder="请选择">
          {modelBusinessAttrData.map((item: any) => (
            <Option key={item.modelBusinessAttrCode} value={item.modelBusinessAttrCode}>
              {item.modelBusinessAttrName}
            </Option>
          ))}
        </Select>
      </Form.Item>
      <Form.Item name="sufferRelationBusinessId" label="被关联对象ID">
        <Select placeholder="请选择" onSelect={handleChangeModel}>
          {modelBusinessData.map((item: any) => (
            <Option key={item.modelBusinessCode} value={item.modelBusinessCode}>
              {item.modelBusinessName}
            </Option>
          ))}
        </Select>
      </Form.Item>
      <Form.Item name="sufferRelationBusinessAttrId" label="被关联对象属性ID" colon={false}>
        <Select placeholder="请选择">
          {modelBusinessAttrData.map((item: any) => (
            <Option key={item.modelBusinessAttrCode} value={item.modelBusinessAttrCode}>
              {item.modelBusinessAttrName}
            </Option>
          ))}
        </Select>
      </Form.Item>
      <Form.Item name="relationType" label="关联方式" colon={false}>
        <Radio.Group>
          <Radio value={1}>一对一</Radio>
          <Radio value={2}>一对多</Radio>
          <Radio value={3}>多对一</Radio>
        </Radio.Group>
      </Form.Item>
      <Form.Item {...tailLayout}>
        <Space>
          <Button type="primary" htmlType="submit">
            创建
          </Button>
          <Button type="default" onClick={onCancel}>
            取消
          </Button>
        </Space>
      </Form.Item>
    </Form>
  );
};
export default CreateRelation;
