import React from 'react';
import './Entity.less';
import type { EntityCanvasModel, EntityProperty } from './interface';
import { EntityType } from './constants';
import { BarsOutlined, PlusOutlined } from '@ant-design/icons';

interface Props {
  entity: EntityCanvasModel;
  handleAddAttr: any;
}

export default class Entity extends React.PureComponent<Props> {
  render() {
    const { entity, handleAddAttr } = this.props;
    const getCls = () => {
      if (entity.entityType === EntityType.FACT) {
        return 'fact';
      }
      if (entity.entityType === EntityType.DIM) {
        return 'dim';
      }
      return 'other';
    };
    return (
      <div className={`entity-container ${getCls()}`}>
        <div className={`content ${getCls()}`}>
          <div className="head">
            <div>
              <BarsOutlined className="type" />
              <span>{entity?.modelBusinessName}</span>
            </div>
            <PlusOutlined onClick={() => handleAddAttr()} />
          </div>
          <div className="body">
            {entity.properties.map((property: EntityProperty) => {
              return (
                <div className="body-item" key={property.modelBusinessAttrId}>
                  <div className="name">{property.modelBusinessAttrName}</div>
                  <div className="type">{property.modelBusinessAttrHtmlType}</div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}
