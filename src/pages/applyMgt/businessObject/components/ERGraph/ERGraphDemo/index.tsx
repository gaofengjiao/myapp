import React from 'react';
// import Relation from './Relation';
import type { GraphData } from '../xflow/index';
import { BaseGraph } from '../xflow/index';
import { history } from 'umi';

interface Props {
  /** 画布数据 */
  data: GraphData;
}
export default class EREditorDemo extends React.PureComponent<Props> {
  private baseGraph!: BaseGraph;
  private graphContainer!: HTMLDivElement;
  private minimapContainer!: HTMLDivElement;
  componentDidMount() {
    const { data } = this.props;
    /** 初始化画布 */
    this.baseGraph = new BaseGraph({
      container: this.graphContainer,
      grid: {
        size: 10,
        visible: true,
      },
      // connecting: {
      //   snap: true,
      //   allowBlank: false,
      // },
      minimap: {
        enabled: true,
        container: this.minimapContainer,
        minScale: 0.5,
        maxScale: 2,
        width: 150,
        height: 100,
      },
    });
    console.log(data, 'data');
    /** 渲染画布内容 */
    this.baseGraph.updateGraph(data);

    /** 注册监听事件 */
    this.registerEvent();
  }
  componentDidUpdate() {
    // !!! updateGraph会对画布元素做Diff, 不会重复渲染, 但是如果能控制updateGraph次数, 是更好的
    this.baseGraph.updateGraph(this.props.data);
  }

  private registerEvent = () => {
    this.baseGraph.registerEvent([
      {
        eventName: 'node:dbclick',
        handler: (data: any) => {
          const { node } = data;
          history.push(`/applyMgt/bizObj/list?modelBusinessId=${node.id}`);
        },
      },
      {
        eventName: 'node:mousemove',
        handler: (nodeInfo: any) => {
          console.log(nodeInfo, 'nodeInfo');
          const tempModelBusinessList = localStorage.getItem('modelBusinessList')
            ? JSON.parse(localStorage.getItem('modelBusinessList'))
            : [];
          const {
            node: { id },
            x,
            y,
          } = nodeInfo;
          // eslint-disable-next-line array-callback-return
          tempModelBusinessList.map((item: any) => {
            if (item.modelBusinessCode === id) {
              // eslint-disable-next-line no-param-reassign
              item.x = x;
              // eslint-disable-next-line no-param-reassign
              item.y = y;
            }
          });
          localStorage.setItem('modelBusinessList', JSON.stringify(tempModelBusinessList));
        },
      },
    ]);
  };

  onHandleToolbar = (action: 'in' | 'out' | 'fit' | 'real') => {
    switch (action) {
      case 'in':
        this.baseGraph.zoomGraph(0.1);
        break;
      case 'out':
        this.baseGraph.zoomGraph(-0.1);
        break;
      case 'fit':
        this.baseGraph.zoomGraph('fit');
        break;
      case 'real':
        this.baseGraph.zoomGraph('real');
        break;
      default:
    }
  };

  private refContainer = (container: HTMLDivElement) => {
    this.graphContainer = container;
  };

  private refMinimapContainer = (container: HTMLDivElement) => {
    this.minimapContainer = container;
  };

  render() {
    return (
      <div style={{ width: '100%', height: '100%' }}>
        <div
          ref={this.refContainer}
          style={{
            width: '100%',
            height: '100%',
          }}
        />
        <div ref={this.refMinimapContainer} className="minimap-container" />
        {/* <ERGraph
          data={{
            nodes,
            edges,
          }}
        /> */}
      </div>
    );
  }
}
