import React from 'react';
import styles from './index.less';
import ERGraphDemo from './ERGraphDemo';
import type { NodeConfig, EdgeConfig } from './xflow';
import type { EntityCanvasModel, RelationCanvasModel } from './ERGraphDemo/interface';
import Entity from './ERGraphDemo/Entity';

interface ERGraphPageIProps {
  mockEntityData: any[];
  mockRelationData: any[];
  handleAddAttr: any;
}
const ERGraphPage: React.FC<ERGraphPageIProps> = (props) => {
  const { mockEntityData, mockRelationData, handleAddAttr } = props;

  const calRenderData = () => {
    const nodes: NodeConfig[] = mockEntityData.map((entity: EntityCanvasModel) => {
      const { modelBusinessCode, x, y, width, height } = entity;
      const nodeData: NodeConfig = {
        x,
        y,
        width,
        height,
        id: modelBusinessCode,
        render: (data: EntityCanvasModel) => {
          return <Entity entity={data} handleAddAttr={() => handleAddAttr(modelBusinessCode)} />;
        },
        data: entity,
      };
      return nodeData;
    });

    const edges: EdgeConfig[] = mockRelationData.map((relation: RelationCanvasModel) => {
      const {
        modelRelationId,
        relationBusinessId,
        sufferRelationBusinessId,
        relationBusinessAttrId,
        sufferRelationBusinessAttrId,
      } = relation;
      const edgeData: EdgeConfig = {
        id: modelRelationId,
        source: relationBusinessId,
        target: sufferRelationBusinessId,
        label: `${relationBusinessAttrId} - ${sufferRelationBusinessAttrId}`,
        // render: (data: RelationCanvasModel) => {
        //   return null;
        // },
        data: relation,
      };
      return edgeData;
    });

    return { nodes, edges };
  };
  const { nodes, edges } = calRenderData();
  return (
    <div className={styles.erGraphDemo}>
      <ERGraphDemo
        data={{
          nodes,
          edges,
        }}
      />
    </div>
  );
};

export default ERGraphPage;
