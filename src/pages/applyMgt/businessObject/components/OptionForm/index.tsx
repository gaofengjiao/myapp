import type { FC } from 'react';
import { useState } from 'react';
import { Form, Input, Button, Space } from 'antd';
import { EditableProTable } from '@ant-design/pro-table';
import type { ProColumns } from '@ant-design/pro-table';
import styles from './index.less';

const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 20 },
};
const tailLayout = {
  wrapperCol: { offset: 4, span: 20 },
};

interface OptionFormIProps {
  onSumbit: any;
  onCancel: () => void;
}
const waitTime = (time: number = 100) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(true);
    }, time);
  });
};
const OptionForm: FC<OptionFormIProps> = (props) => {
  const { onSumbit, onCancel } = props;
  const [dataSource, setDataSource] = useState<any>([]);
  const [editableKeys, setEditableRowKeys] = useState<React.Key[]>([]);
  const [form] = Form.useForm();
  const onFinish = (values: any) => {
    onSumbit({ ...values, optionAttrList: dataSource });
  };

  const columns: ProColumns[] = [
    {
      title: '编码',
      dataIndex: 'title',
    },
    {
      title: '值',
      key: 'state',
      dataIndex: 'state',
    },
    {
      title: '颜色',
      dataIndex: 'decs',
    },
    {
      title: '操作',
      valueType: 'option',
      render: (text, record, _, action) => [
        <a
          key="editable"
          onClick={() => {
            action?.startEditable?.(record.id);
          }}
        >
          编辑
        </a>,
        <a
          key="delete"
          onClick={() => {
            setDataSource(dataSource.filter((item: any) => item.id !== record.id));
          }}
        >
          删除
        </a>,
      ],
    },
  ];

  return (
    <Form {...layout} form={form} onFinish={onFinish}>
      <Form.Item name="modelOptionName" label="名称" rules={[{ required: true }]}>
        <Input />
      </Form.Item>
      <Form.Item name="modelOptionCode" label="编码" rules={[{ required: true }]}>
        <Input />
      </Form.Item>
      <Form.Item name="modelOptionDescribe" label="描述" rules={[{ required: true }]}>
        <Input />
      </Form.Item>
      <Form.Item label="请添加选项" rules={[{ required: true }]}>
        <EditableProTable
          className={styles.option_table}
          recordCreatorProps={{
            position: 'top',
            record: () => ({ id: (Math.random() * 1000000).toFixed(0) }),
          }}
          rowKey="id"
          bordered
          columns={columns}
          value={dataSource}
          onChange={setDataSource}
          editable={{
            editableKeys,
            onSave: async (rowKey, data, row) => {
              console.log(rowKey, data, row);
              await waitTime(2000);
            },
            onChange: setEditableRowKeys,
          }}
        />
      </Form.Item>

      <Form.Item {...tailLayout}>
        <Space>
          <Button type="primary" htmlType="submit">
            创建
          </Button>
          <Button type="default" onClick={onCancel}>
            取消
          </Button>
        </Space>
      </Form.Item>
    </Form>
  );
};
export default OptionForm;
