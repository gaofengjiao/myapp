import type { FC, MutableRefObject } from 'react';
import React, { useState, useEffect } from 'react';
import type { ActionType, ProColumns } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import { Button, Row, Drawer } from 'antd';
import { history } from 'umi';
// import { modelBusinessAttrList } from '@/services/application/serviceConfig'
import _ from 'lodash';
import OptionForm from '../OptionForm';

interface OptionListIProps {
  id: string;
}
const OptionList: FC<OptionListIProps> = (props) => {
  const { id } = props;
  const actionRef = React.createRef<ActionType>() as MutableRefObject<ActionType>;
  // const [loading, setLoading] = useState(false);
  const [visibleOption, setVisibleOption] = useState(false);
  const [dataSource, setDataSource] = useState([]);
  // const fetchTableData = async (params: any) => {
  //     const newParams = { pageNum: params.current, pageSize: 10, ...params, modelBusinessId: id  };
  //     setLoading(true);
  //     const result = await modelBusinessAttrList(newParams);
  //     if (result) {
  //         setLoading(false);
  //         if (result.status === "0") {
  //             return {
  //                 data: result.data,
  //                 total: result.total,
  //             };
  //         }
  //         return message.error(result?.message);

  //     }
  //     return { data: [], success: true, total: null };
  // };
  const columns: ProColumns[] = [
    {
      title: '名称',
      dataIndex: 'modelOptionName',
    },
    {
      title: '编码',
      dataIndex: 'modelOptionCode',
    },
    {
      title: '描述',
      dataIndex: 'modelOptionDescribe',
    },
  ];
  const onCloseOption = () => {
    setVisibleOption(false);
  };
  const handleOnSumbitOption = (values: any) => {
    const cloneData: any = _.cloneDeep(dataSource);
    cloneData.push(values);
    setDataSource(cloneData);
    localStorage.setItem('optionDataChildrenList', JSON.stringify(cloneData));
    setVisibleOption(false);
  };
  // 添加属性
  const handleAddAttr = () => {
    setVisibleOption(true);
  };
  useEffect(() => {
    const tempOptionDataChildrenList = localStorage.getItem('optionDataChildrenList')
      ? JSON.parse(localStorage.getItem('optionDataChildrenList'))
      : [];
    setDataSource(tempOptionDataChildrenList);
  }, []);
  return (
    <>
      <Row justify="space-between">
        <Button type="link" onClick={handleAddAttr}>
          +添加选项集
        </Button>
        <a onClick={() => history.push('/applyMgt/bizObj')}>返回</a>
      </Row>
      <ProTable
        // loading={loading}
        actionRef={actionRef}
        size="small"
        columns={columns}
        bordered
        // request={fetchTableData}
        dataSource={dataSource}
        rowKey={'modelOptionId'}
        tableAlertRender={false}
        toolBarRender={false}
        search={false}
        pagination={{
          size: 'default',
          defaultPageSize: 10,
        }}
      />
      <Drawer
        title="添加选项集"
        getContainer={false}
        style={{ position: 'absolute' }}
        placement="right"
        closable={false}
        onClose={onCloseOption}
        visible={visibleOption}
        width={600}
      >
        <OptionForm
          onSumbit={(values: any) => handleOnSumbitOption(values)}
          onCancel={onCloseOption}
        />
      </Drawer>
    </>
  );
};
export default OptionList;
