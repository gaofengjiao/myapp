import type { FC, MutableRefObject } from 'react';
import React, { useState, useEffect } from 'react';
import type { ActionType, ProColumns } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import { Button, Drawer } from 'antd';
// import { modelBusinessAttrList } from '@/services/application/serviceConfig'
import _ from 'lodash';
import CreateRelation from '../CreateRelation';

interface RelationListIProps {
  id: string;
}
const RelationList: FC<RelationListIProps> = (props) => {
  const { id } = props;
  const actionRef = React.createRef<ActionType>() as MutableRefObject<ActionType>;
  // const [loading, setLoading] = useState(false);
  const [visibleRelation, setVisibleRelation] = useState(false);
  const [mockRelationData, setMockRelationData] = useState([]);

  // const fetchTableData = async (params: any) => {
  //     const newParams = { pageNum: params.current, pageSize: 10, ...params, modelBusinessId: id  };
  //     setLoading(true);
  //     const result = await modelBusinessAttrList(newParams);
  //     if (result) {
  //         setLoading(false);
  //         if (result.status === "0") {
  //             return {
  //                 data: result.data,
  //                 total: result.total,
  //             };
  //         }
  //         return message.error(result?.message);

  //     }
  //     return { data: [], success: true, total: null };
  // };
  const columns: ProColumns[] = [
    {
      title: '关联对象ID',
      dataIndex: 'relationBusinessId',
    },
    {
      title: '关联对象属性ID',
      dataIndex: 'relationBusinessAttrId',
    },
    {
      title: '被关联对象ID',
      dataIndex: 'sufferRelationBusinessId',
    },
    {
      title: '被关联对象属性ID',
      dataIndex: 'sufferRelationBusinessAttrId',
    },
    {
      title: '关联方式',
      dataIndex: 'relationType',
    },
  ];
  const onCloseRelation = () => {
    setVisibleRelation(false);
  };
  const handleOnSumbitRelation = (values: any) => {
    const cloneData: any = _.cloneDeep(mockRelationData);
    cloneData.push({ ...values, modelRelationId: new Date().valueOf() });
    setMockRelationData(cloneData);
    localStorage.setItem('modelRelationList', JSON.stringify(cloneData));
    setVisibleRelation(false);
  };
  // 添加属性
  const handleAddRelation = () => {
    setVisibleRelation(true);
  };
  useEffect(() => {
    const tempMockRelationData = localStorage.getItem('modelRelationList')
      ? JSON.parse(localStorage.getItem('modelRelationList'))
      : [];
    setMockRelationData(tempMockRelationData);
  }, []);
  return (
    <>
      <Button type="link" onClick={handleAddRelation}>
        + 添加关联关系
      </Button>
      <ProTable
        // loading={loading}
        actionRef={actionRef}
        size="small"
        columns={columns}
        bordered
        // request={fetchTableData}
        dataSource={mockRelationData}
        rowKey={'modelRelationId'}
        tableAlertRender={false}
        toolBarRender={false}
        search={false}
        pagination={{
          size: 'default',
          defaultPageSize: 10,
        }}
      />
      <Drawer
        title="添加关联关系"
        destroyOnClose={true}
        getContainer={false}
        style={{ position: 'absolute' }}
        placement="right"
        closable={false}
        onClose={onCloseRelation}
        visible={visibleRelation}
        width={400}
      >
        <CreateRelation
          onSumbit={(values: any) => handleOnSumbitRelation(values)}
          onCancel={onCloseRelation}
        />
      </Drawer>
    </>
  );
};
export default RelationList;
