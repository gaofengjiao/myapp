import type { FC } from 'react';
import { useState, useEffect } from 'react';
import { Menu, Row, Modal, Form, Input } from 'antd';
import { PlusCircleOutlined } from '@ant-design/icons';
import styles from '../../index.less';
import { cloneDeep } from 'lodash';

interface SiderMenuIProps {
  onSelect: any;
}
const SiderMenu: FC<SiderMenuIProps> = (props) => {
  const { onSelect } = props;
  const [modelType, setModelType] = useState<number>(); // 1模型 2选项集
  const [visible, setVisible] = useState(false);
  const [form] = Form.useForm();
  const [modelCateData, setModelCateData] = useState<any>([]); // 模型
  const [optionData, setOptionData] = useState<any>([]); // 选项集
  /**
   * 创建分类
   *  @param type 分类类型 1 模型 2数据集
   */
  const handleCreateCategory = (type: number) => {
    setModelType(type);
    setVisible(true);
  };
  const onCancel = () => {
    setVisible(false);
  };
  const handleSave = () => {
    form.validateFields().then((values: any) => {
      if (Number(modelType) === 1) {
        const cloneData = cloneDeep(modelCateData);
        cloneData.push({ ...values, modelType: 1, modelTypeId: new Date().valueOf() });
        setModelCateData(cloneData);
        localStorage.setItem('modelCateData', JSON.stringify(cloneData));
      } else if (Number(modelType) === 2) {
        const cloneData = cloneDeep(optionData);
        cloneData.push({ ...values, modelType: 2, modelTypeId: new Date().valueOf() });
        setOptionData(cloneData);
        localStorage.setItem('optionData', JSON.stringify(cloneData));
      }
      // 添加分类成功关闭弹出框
      onCancel();
    });
  };
  useEffect(() => {
    const tempModelCateData = localStorage.getItem('modelCateData')
      ? JSON.parse(localStorage.getItem('modelCateData'))
      : [];
    const tempOptionData = localStorage.getItem('optionData')
      ? JSON.parse(localStorage.getItem('optionData'))
      : [];
    setModelCateData(tempModelCateData);
    setOptionData(tempOptionData);
  }, []);
  return (
    <>
      <Menu style={{ height: '100%' }}>
        <Menu.ItemGroup
          title={
            <Row justify="space-between">
              <span className={styles.model_group_item_title}>模型</span>
              <PlusCircleOutlined onClick={() => handleCreateCategory(1)} />
            </Row>
          }
        >
          {modelCateData.map((item: any) => {
            return (
              <Menu.Item onClick={() => onSelect({ ...item, modelType: 1 })} key={item.modelTypeId}>
                {item.modelTypeName}
              </Menu.Item>
            );
          })}
        </Menu.ItemGroup>
        <Menu.ItemGroup
          title={
            <Row justify="space-between">
              <span className={styles.model_group_item_title}>选项集</span>
              <PlusCircleOutlined onClick={() => handleCreateCategory(2)} />
            </Row>
          }
        >
          {optionData.map((item: any) => {
            return (
              <Menu.Item onClick={() => onSelect({ ...item, modelType: 2 })} key={item.modelTypeId}>
                {item.modelTypeName}
              </Menu.Item>
            );
          })}
        </Menu.ItemGroup>
      </Menu>
      <Modal
        title="分类信息"
        visible={visible}
        onOk={handleSave}
        onCancel={onCancel}
        destroyOnClose={true}
      >
        <Form form={form}>
          <Form.Item name="modelTypeName" label="名称" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item name="modelTypeCode" label="编码" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};
export default SiderMenu;
