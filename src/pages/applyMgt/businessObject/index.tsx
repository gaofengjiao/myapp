import type { FC } from 'react';
import { useState, useEffect } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { Layout, Drawer, Row, Divider, Button, Space, message } from 'antd';
import { modelBusinessSave, modelTypeDiagramDetail } from '@/services/application/serviceConfig';
import ERGraphPage from './components/ERGraph';
import CreateModel from './components/CreateModel';
import CreateAttr from './components/CreateAttr';
import SiderMenu from './components/SiderMenu';
import _ from 'lodash';
import { history } from 'umi';
import styles from './index.less';

const { Sider, Content } = Layout;

const ModelObject: FC = () => {
  // mockEntityData  mockRelationData
  const [mockEntityData, setMockEntityData] = useState([]);
  const [mockRelationData, setMockRelationData] = useState([]);
  const [visibleModel, setVisibleModel] = useState(false);
  const [visibleAttr, setVisibleAttr] = useState(false);
  const [entityId, setEntityId] = useState<string>(); // 模型图id
  const [modelTypeId, setModelTypeId] = useState();

  // 当前添加的数据

  const getEntityData = async (id: any) => {
    const result = await modelTypeDiagramDetail({ modelTypeId: id });
    if (result && result.status === '0') {
      const { modelBusinessList } = result.data;
      setMockEntityData(modelBusinessList);
      setMockRelationData(result.data.modelRelationList);
      message.success(result.message);
    } else {
      message.error(result.message);
    }
  };
  /**
   * 抽屉方法
   */
  const handleShowModel = () => {
    if (modelTypeId) {
      setVisibleModel(true);
    } else {
      message.warning('请选择分类!');
    }
  };
  const onCloseModel = () => {
    setVisibleModel(false);
  };

  const handleModelSave = async (values: any) => {
    const result = await modelBusinessSave(values);
    if (result && result.status === '0') {
      getEntityData(modelTypeId);
      message.success(result.message);
    } else {
      message.error(result.message);
    }
  };
  const handleOnSumbitModel = (values: any) => {
    // 创建成功
    onCloseModel();

    const entiryItem = {
      modelBusinessCode: values.modelBusinessCode,
      modelBusinessName: values.modelBusinessName,
      entityType: 'FACT',
      properties: [],
      x: Number(values.X),
      y: Number(values.Y),
      width: 214,
      height: 248,
    };

    //   handleModelSave(values)
    const cloneData: any = _.cloneDeep(mockEntityData);
    // 业务对象
    cloneData.push(entiryItem);
    localStorage.setItem('modelBusinessList', JSON.stringify(cloneData));
    setMockEntityData(cloneData);
  };
  const onCloseAttr = () => {
    setVisibleAttr(false);
  };
  // 添加属性
  const handleAddAttr = (id: string) => {
    setVisibleAttr(true);
    setEntityId(id);
  };
  const handleOnSumbitAttr = (values: any) => {
    console.log(values, 'values');
    const cloneData: any = _.cloneDeep(mockEntityData);
    cloneData.map((item: any) => {
      if (item.modelBusinessCode === entityId) {
        item.properties.push(values);
      }
      return null;
    });
    localStorage.setItem('modelBusinessList', JSON.stringify(cloneData));
    setMockEntityData(cloneData);
    setVisibleAttr(false);
  };

  // 获取分类信息item
  const handleCateChange = (item: any) => {
    const { modelType } = item;
    console.log(item, 'item');
    setModelTypeId(item.modelTypeId);
    console.log(modelType, '1');
    if (Number(modelType) === 2) {
      history.push(`/applyMgt/bizObj/optionList?modelTypeId=${item.modelTypeId}`);
    }
  };
  useEffect(() => {
    const tempModelRelationList = localStorage.getItem('modelRelationList')
      ? JSON.parse(localStorage.getItem('modelRelationList'))
      : [];
    setMockRelationData(tempModelRelationList);

    const temEntityData = localStorage.getItem('modelBusinessList')
      ? JSON.parse(localStorage.getItem('modelBusinessList'))
      : [];
    setMockEntityData(temEntityData);
  }, []);

  return (
    <PageContainer breadcrumb={undefined} title={false}>
      <Layout>
        <Sider theme="light" style={{ height: 'calc(100vh - 48px)',borderLeft:'1px solid #eee' }}>
          <SiderMenu onSelect={(value: any) => handleCateChange(value)} />
        </Sider>
        <Content className={styles.model_container} style={{ height: 'calc(100vh - 48px)' }}>
          <Row className={styles.header_title} justify="space-between">
            <span>业务对象</span>
            <Space>
              <Button type="primary" onClick={handleShowModel}>
                创建业务对象
              </Button>
              <Button type="primary">发布</Button>
            </Space>
          </Row>
          <Divider className={styles.header_title_divider_horizontal} />
          <ERGraphPage
            mockEntityData={mockEntityData}
            mockRelationData={mockRelationData}
            handleAddAttr={(id: string) => handleAddAttr(id)}
          />

          <Drawer
            title="添加业务对象"
            destroyOnClose={true}
            getContainer={false}
            style={{ position: 'absolute' }}
            placement="right"
            closable={false}
            onClose={onCloseModel}
            visible={visibleModel}
            width={400}
          >
            <CreateModel
              onSumbit={(values: any) => handleOnSumbitModel(values)}
              onCancel={onCloseModel}
            />
          </Drawer>

          <Drawer
            title="添加属性"
            getContainer={false}
            destroyOnClose={true}
            style={{ position: 'absolute' }}
            placement="right"
            closable={false}
            onClose={onCloseAttr}
            visible={visibleAttr}
            width={400}
          >
            <CreateAttr
              onSumbit={(values: any) => handleOnSumbitAttr(values)}
              onCancel={onCloseAttr}
            />
          </Drawer>
        </Content>
      </Layout>
    </PageContainer>
  );
};
export default ModelObject;
