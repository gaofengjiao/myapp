import type { FC } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { Layout, Tabs } from 'antd';
import AttrList from '../components/AttrList';
import RelationList from '../components/RelationList';
import { getPageQuery } from '@/utils';
import styles from '../index.less';
import { history } from '@/.umi/core/history';

const { TabPane } = Tabs;

const { Content } = Layout;
const List: FC = () => {
  const modelBusinessId: any = getPageQuery() && getPageQuery().modelBusinessId;

  const panes = [
    { title: '属性', content: <AttrList id={modelBusinessId} />, key: '1' },
    { title: '关联', content: <RelationList id={modelBusinessId} />, key: '2' },
  ];
  return (
    <PageContainer breadcrumb={undefined} title={false}>
      <Layout>
        <Content
          className={styles.model_container}
          style={{ height: 'calc(100vh - 96px)', padding: 4 }}
        >
          <Tabs
            defaultActiveKey="1"
            tabBarExtraContent={<a onClick={() => history.push('/applyMgt/bizObj')}>返回</a>}
          >
            {panes.map((item) => {
              return (
                <TabPane tab={item.title} key={item.key}>
                  {item.content}
                </TabPane>
              );
            })}
          </Tabs>
        </Content>
      </Layout>
    </PageContainer>
  );
};
export default List;
