import type { FC } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { Layout } from 'antd';
import { getPageQuery } from '@/utils';
import styles from '../index.less';
import OptionList from '../components/OptionList';

const { Content } = Layout;
const OptionSet: FC = () => {
  const modelBusinessId: any = getPageQuery() && getPageQuery().modelBusinessId;
  return (
    <PageContainer breadcrumb={undefined} title={false}>
      <Layout>
        <Content
          className={styles.model_container}
          style={{ height: 'calc(100vh - 96px)', padding: 4 }}
        >
          <h4>选项集</h4>
          <OptionList id={modelBusinessId} />
        </Content>
      </Layout>
    </PageContainer>
  );
};
export default OptionSet;
