import type { FC, MutableRefObject } from 'react';
import React, { useState } from 'react';
import type { ActionType, ProColumns } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
// import { serviceList } from '@/services/application/serviceConfig';
import { Button, message } from 'antd';

const ServiceList: FC = () => {
  const actionRef = React.createRef<ActionType>() as MutableRefObject<ActionType>;
  // const [loading, setLoading] = useState(false);
  const [dataSource, setDataSource] = useState([]);
  // const [params, setParams] = useState({});
  // const fetchTableData = async (params: any) => {
  //     const newParams = { pageNum: params.current, pageSize: 10, ...params };
  //     setLoading(true);
  //     const result = await serviceList(newParams);
  //     if (result) {
  //         setLoading(false);
  //         if (result.code === 200) {
  //             return {
  //                 data: result.rows,
  //                 total: result.total,
  //             };
  //         }
  //         return message.error(result?.msg);

  //     }
  //     return { data: [], success: true, total: null };
  // };
  const columns: ProColumns[] = [
    {
      title: '发布配置名称',
      dataIndex: 'ruleCode',
      width: 150,
    },
    {
      title: '状态',
      width: 150,
      dataIndex: 'ruleName',
    },

    {
      title: '环境',
      dataIndex: 'ruleClassType',
      width: 140,
      // render: (text: any) => (text ? getText(ruleClassType, text) : '--'),
    },
    {
      title: '描述',
      dataIndex: 'ruleType',
      width: 100,
      // render: (text: any) => (text ? getText(ruleType, text) : '--'),
    },
  ];
  return (
    <div>
      <Button type="link">创建发布配置</Button>
      <ProTable
        bordered
        // loading={loading}
        actionRef={actionRef}
        size="small"
        // params={params}
        columns={columns}
        dataSource={dataSource}
        // request={fetchTableData}
        rowKey={'ruleCode'}
        tableAlertRender={false}
        toolBarRender={false}
        search={false}
        pagination={{
          size: 'default',
          defaultPageSize: 10,
        }}
      />
    </div>
  );
};

export default ServiceList;
