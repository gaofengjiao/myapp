import type { FC } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { Tabs } from 'antd';
import ServiceList from '@/pages/applyMgt/serviceConfig/components/ServiceList';

const { TabPane } = Tabs;

const List: FC = () => {
  const panes = [
    { title: '服务配置', content: '服务配置', key: '1' },
    { title: '用户角色', content: '用户角色', key: '2' },
    { title: '发布配置', content: <ServiceList />, key: '3' },
    { title: '服务信息', content: '服务信息', key: '4' },
  ];

  return (
    <PageContainer
      breadcrumb={undefined}
      title={false}
      style={{ background: '#fff', height: 'calc(100vh - 48px)',marginLeft:0, borderLeft:'1px solid #eee'  }}
    >
      <Tabs defaultActiveKey="3" style={{marginLeft:-12}}>
        {panes.map((item) => {
          return (
            <TabPane tab={item.title} key={item.key}>
              {item.content}
            </TabPane>
          );
        })}
      </Tabs>
    </PageContainer>
  );
};
export default List;
