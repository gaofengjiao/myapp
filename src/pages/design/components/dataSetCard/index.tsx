/*
 * @Author: wwb
 * @Date: 2021-08-29 11:35:00
 * @Last Modified by: wwb
 * @Last Modified time: 2021-09-01 00:06:20
 */

import { Col } from 'antd';
import { TableOutlined, CheckOutlined } from '@ant-design/icons';
import classnames from 'classnames';
import type { DataSourceOrinigeType } from './typing';
import './index.less';

interface DataSourceCardProps {
  data: any;
  selectedDataSource: DataSourceOrinigeType[];
  index?: number;
  toggleSelectedDataSetList: (data: DataSourceOrinigeType) => void;
}

export const DataSetItem = ({
  data,
  selectedDataSource = [],
  index,
  toggleSelectedDataSetList,
}: DataSourceCardProps) => {
  return (
    <Col
      span={5}
      key={data?.modelBusinessCode}
      push={(index && index % 2) === 0 ? 0 : 1}
      className={classnames(
        'sourceItem',
        selectedDataSource.find((d: any) => d.modelBusinessCode === data.modelBusinessCode) ? 'selected' : '',
      )}
      onClick={() => toggleSelectedDataSetList(data)}
    >
      <div className="item-icon">
        <span>
          <TableOutlined style={{ fontSize: 12 }} />
        </span>
      </div>
      <div className="item-desc">
        <div className="desc-title">{data?.modelBusinessName}</div>
        <div className="desc-code">
          <small>{data?.modelBusinessCode}</small>
        </div>
      </div>
      {selectedDataSource.find((d) => d.id === data?.id) ? (
        <span className="indicator">
          <CheckOutlined style={{ fontSize: 14 }} />
        </span>
      ) : null}
    </Col>
  );
};
