/*
 * @Author: wwb
 * @Date: 2021-08-29 11:39:35
 * @Last Modified by: wwb
 * @Last Modified time: 2021-08-29 11:48:15
 */

export interface DataSourceOrinigeType {
  title: string;
  id: string;
  code: string;
  fields: { title: string; code: string }[];
}
