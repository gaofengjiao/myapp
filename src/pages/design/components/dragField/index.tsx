/*
 * @File 可拖拽单个字段组件
 * @Author: wwb
 * @Date: 2021-08-29 23:42:21
 * @Last Modified by: wwb
 * @Last Modified time: 2021-08-31 11:20:57
 */

// import react from 'react';
import type { DragSourceMonitor } from 'react-dnd';
import { useDrag } from 'react-dnd';
import ItemTypes from '../../playground/ItemTypes';
import './index.less';

const DragField = ({
  isDragging,
  title,
  treeData,
  selectedTreeNode,
  handleChangeTreeData,
  changeTreePositionData,
  ...rest
}) => {
  console.log(selectedTreeNode, 'selectedTreeNode');
  const [{ opacity }, dragRef] = useDrag({
    item: { ...rest, type: ItemTypes.Fields },
    collect: (monitor) => ({
      opacity: monitor.isDragging() ? 0.5 : 1,
    }),
    begin(monitor: DragSourceMonitor) {
      // const newTreeData = [...treeData];
      const useless = selectedTreeNode.queryFields?.find((item: any) => item.id === -1);
      // 拖拽开始时，向 cardList 数据源中插入一个占位的元素，如果占位元素已经存在，不再重复插入
      if (!useless) {
        // const [newTree, currSelected] = changeTreePositionData(newTreeData, selectedTreeNode.id, [{ ...rest, title }], 'x-component-queryField');
        // console.log(newTree,'newTree');
        // if(handleChangeTreeData){
        //   handleChangeTreeData(newTree);
        // }
        // changeCardList([{ bg: "aqua", category: '放这里', id: -1 }, ...cardList]);
      }
      return { ...rest, title };
    },
  });
  return (
    <div className="dragFieldItem" ref={dragRef} style={{ opacity }}>
      {title}
    </div>
  );
};

export default DragField;
