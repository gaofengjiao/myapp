/*
 * @Author: wwb
 * @Date: 2021-08-30 00:33:20
 * @Last Modified by: wwb
 * @Last Modified time: 2021-08-31 17:18:45
 */
import { useCallback, useRef, useState } from 'react';
import classNames from 'classnames';
import { useDrag, useDrop } from 'react-dnd';
import SectionHeader from '../sectionHeader';
import FieldCard from '../fieldCard';
import update from 'immutability-helper';
import type { XYCoord } from 'dnd-core';

import type { DropTargetMonitor } from 'react-dnd';
import _ from 'lodash';

import './index.less';
import ItemTypes from '../../playground/ItemTypes';

const DropContianer = ({
  changeTreePositionData,
  handleChangeTreeData,
  selectedTreeNode,
  treeData,
}) => {
  const boxRef = useRef(null); // 搜索条件
  const tableFieldRef = useRef(null); // 列表字段

  const [collectedProps, drop] = useDrop({
    accept: ItemTypes.Fields,
    collect: (minoter: DropTargetMonitor) => ({
      isOver: minoter.isOver(),
      item: minoter.getItem(),
    }),
  });

  const moveCard = useCallback(
    (dragIndex: number, hoverIndex: number) => {
      /**
       * 1、如果此时拖拽的组件是 右侧字段，则 dragIndex 为 undefined，则此时修改，则此时修改 cardList 中的占位元素的位置即可
       * 2、如果此时拖拽的组件是 搜索区域内字段，则 dragIndex 不为 undefined，此时替换 dragIndex 和 hoverIndex 位置的元素即可
       */
      const newTreeData = _.cloneDeep(treeData);

      if (selectedTreeNode.queryFields) {
        if (dragIndex === undefined) {
          // const lessIndex = selectedTreeNode.queryFields.findIndex((item: any) => item.code === -1);
          // console.log(lessIndex,'lessIndex')
          // const [newTree, currSelected] = changeTreePositionData(newTreeData, selectedTreeNode.id, [], 'x-component-queryField');
          // setQueryFields()
          // setQueryFields(update(selectedTreeNode.queryFields, {
          //   $splice: [[lessIndex, 1], [hoverIndex, 0, { bg: "aqua", category: '放这里', id: -1 }]],
          // }));
        } else {
          const dragCard = selectedTreeNode.queryFields[dragIndex];
          // setQueryFields(update(queryFields, {
          //   $splice: [[dragIndex, 1], [hoverIndex, 0, dragCard]],
          // }));
        }
      }
      // eslint-disable-next-line
    },
    [selectedTreeNode.queryFields],
  );

  /**
   * @description 搜索条件区域拖拽
   */

  const [{ isDragging }, dragRef, dragPreview] = useDrag({
    // item: { type: 'box', $id: inside ? 0 + $id : $id },
    item: { type: ItemTypes.Fields },
    end: (item, monitor) => {
      const dropResult = monitor.getDropResult();
      console.log(dropResult, 'dropResult');
      if (item && dropResult) {
        // alert(`You dropped into ${dropResult.name}!`);
      }
    },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });

  const [queryCollectedProps, queryDropRef] = useDrop({
    accept: ItemTypes.Fields,
    collect: (minoter: DropTargetMonitor) => ({
      isOver: minoter.isOver(),
      item: minoter.getItem(),
    }),
    hover: (item, monitor) => {
      // 只检查被hover的最小元素
      const didHover = monitor.isOver({ shallow: true });
      if (didHover) {
        // 确定屏幕上矩形范围
        // const hoverBoundingRect = boxRef.current!.getBoundingClientRect();
        // console.log(hoverBoundingRect,'hoverBoundingRect')
        // 确定鼠标位置
        // const clientOffset = monitor.getClientOffset();
        // 获取距顶部距离
        // const hoverClientX = (clientOffset as XYCoord).x - hoverBoundingRect.left;
        // console.log(hoverClientX,'hoverClientX')
      }
    },
    drop: (item, monitor) => {
      const newTreeData = _.cloneDeep(treeData);
      const [newTree, currSelected] = changeTreePositionData(
        newTreeData,
        selectedTreeNode.id,
        [{ ...item }],
        'x-component-queryField',
      );
      if (handleChangeTreeData) {
        handleChangeTreeData(newTree, currSelected);
      }
    },
  });

  /**
   * @description 字段区域拖拽
   */
  const [fieldCollectedProps, tableFieldDrop] = useDrop({
    accept: ItemTypes.Fields,
    collect: (minoter: DropTargetMonitor) => ({
      isOver: minoter.isOver(),
      item: minoter.getItem(),
    }),
    drop: (item, monitor) => {
      const newTreeData = _.cloneDeep(treeData);
      const [newTree, currSelected] = changeTreePositionData(
        newTreeData,
        selectedTreeNode.id,
        [{ ...item }],
        'x-component-tableFields',
      );
      if (handleChangeTreeData) {
        handleChangeTreeData(newTree, currSelected);
      }
    },
  });

  dragPreview(queryDropRef(boxRef));

  dragPreview(tableFieldDrop(tableFieldRef));

  return (
    <div className="dropViewContianer">
      <section>
        <SectionHeader title={'搜索'} />
        <div
          ref={boxRef}
          className={classNames('dropArea', queryCollectedProps.isOver ? 'dropOver' : '')}
        >
          {selectedTreeNode?.['x-component-queryField']?.map((field) => (
            <FieldCard key={field.id} {...field} moveCard={moveCard} />
          ))}
        </div>
      </section>
      <section>
        <SectionHeader title={'列表字段'} />
        <div
          ref={tableFieldRef}
          className={classNames('dropArea', fieldCollectedProps.isOver ? 'dropOver' : '')}
        >
          {selectedTreeNode?.['x-component-tableFields']?.map((field) => (
            <FieldCard key={field.id} {...field} moveCard={moveCard} />
          ))}
        </div>
      </section>
    </div>
  );

  // <div ref={drop} className='dropContianer' style={{ background: bg }}>Drop Target</div>
};

export default DropContianer;
