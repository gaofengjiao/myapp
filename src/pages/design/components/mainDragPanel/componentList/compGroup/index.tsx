/*
 * @Author: wwb 
 * @Date: 2021-08-31 22:26:29 
 * @Last Modified by: wwb
 * @Last Modified time: 2021-08-31 23:35:35
 */

import { Space } from 'antd';
import DragItem from '../dragItem';

const titleEnums = {
  'layout': '布局组件',
  'layoutTemplate': '布局模版',
  'dataSet': '数据组件'
}

const CompGroup = ({  dataKey, list }: any) => {
  return (
    <div className='dataSetList'>
      <Space direction='vertical' style={{ width: '100%' }}>
        <div>{titleEnums[dataKey]}</div>
        <div className='dragContent'>
          {
            list?.map((item: any) => (
              <DragItem key={item.title} dataKey={dataKey} {...item}/>
            ))
          }
        </div>
      </Space>
    </div>
  )
}

export default CompGroup;