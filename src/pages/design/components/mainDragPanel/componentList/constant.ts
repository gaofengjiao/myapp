/*
 * @Author: wwb 
 * @Date: 2021-08-31 21:38:47 
 * @Last Modified by: wwb
 * @Last Modified time: 2021-08-31 23:33:13
 */

export const ComponentDataList = {
  layout: [
    {
      title: '列',
      desc: '列'
    }
  ],
  layoutTemplate: [
    {
      title: '单列',
      desc: '单列'
    },
    {
      title: '两列',
      desc: '两列'
    },
    {
      title: '三列',
      desc: '三列'
    }
  ],
  dataSet: [
    {
      title: '列表',
      desc: '列表',
    },
    {
      title: '表单',
      desc: '表单'
    },
    {
      title: '自定义',
      desc: '自定义'
    }
  ]
}