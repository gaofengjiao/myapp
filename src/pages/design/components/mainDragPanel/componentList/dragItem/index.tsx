/*
 * @Fiel 拖拽最小 组件 单元
 * @Author: wwb 
 * @Date: 2021-08-31 21:28:03 
 * @Last Modified by: wwb
 * @Last Modified time: 2021-08-31 23:34:39
 */

// import { react, useRef } from 'react';
// import type { XYCoord } from 'dnd-core';
// import type { DragSourceMonitor, DropTargetMonitor } from 'react-dnd';
import { useDrag } from 'react-dnd';
import ItemTypes from '../../../../playground/ItemTypes';
import './index.less';

const DragItem = (dragItem: any) => {
  const [{ opacity }, dragRef] = useDrag({
    item: { ...dragItem, type: ItemTypes.Fields, isDataSet: dragItem.dataKey === 'dataSet' },
    collect: (monitor) => ({
      opacity: monitor.isDragging() ? 0.5 : 1,
    }),
    begin: () => {
      console.log(dragItem,'dragItem')
    }
  });

  return (
    <div ref={dragRef} className='dragItem' style={{ opacity }}>
      <div><img style={{ width:  80, height: 80 }} src={'https://dummyimage.com/80x80'}/></div>
      <div className='dragItemTitle'>{dragItem.title}</div>
    </div>
  )
}

export default DragItem;