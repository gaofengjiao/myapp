/*
 * @File 组件列表
 * @Author: wwb 
 * @Date: 2021-08-31 17:55:00 
 * @Last Modified by: wwb
 * @Last Modified time: 2021-08-31 22:31:57
 */

import './index.less';
import { ComponentDataList } from './constant';
import CompGroup from './compGroup';


const ComponentList = () => {
  return (
    <div className='ComponentList'>
      {
        Object.entries(ComponentDataList).map(([key,comp]) => (
          <CompGroup key={key} dataKey={key} list={comp}/>
        ))
      }
    </div>
  )
}

export default ComponentList;