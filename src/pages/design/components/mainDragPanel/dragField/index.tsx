/*
 * @Author: wwb 
 * @Date: 2021-09-01 02:08:21 
 * @Last Modified by: wwb
 * @Last Modified time: 2021-09-01 02:49:59
 */
import react from 'react';
import DragFieldItem from '../dragFieldItem';
import './index.less';

const DragField = ({ fields }: { fields: [] }) => {
  return (
    <div className='dragField'>
      {
        fields?.map((field, index) => (
          <DragFieldItem key={index} {...field}/>
        ))
      }
    </div>
  )
}

export default DragField;