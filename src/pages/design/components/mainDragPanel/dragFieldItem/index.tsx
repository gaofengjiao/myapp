/*
 * @File 可拖拽单个字段组件
 * @Author: wwb
 * @Date: 2021-08-29 23:42:21
 * @Last Modified by: wwb
 * @Last Modified time: 2021-09-01 02:27:47
 */

// import react from 'react';
import { Space } from 'antd';
import classNames from 'classnames';
// import type { DragSourceMonitor } from 'react-dnd';
import { useDrag } from 'react-dnd';
import ItemTypes from '../../../playground/ItemTypes';
import './index.less';

const DragFieldItem = (props: any) => {
  // console.log(selectedTreeNode, 'selectedTreeNode');
  const { modelBusinessAttrName = '', modelBusinessAttrHtmlType = '', modelBusinessAttrRequired = 0 } = props;
  const [{ opacity }, dragRef] = useDrag({
    item: { ...props, type: ItemTypes.Fields },
    collect: (monitor) => ({
      opacity: monitor.isDragging() ? 0.5 : 1,
    }),
  });
  return (
    <div className="dragFieldItem" ref={dragRef} style={{ opacity }}>
      <Space>
        <small>{modelBusinessAttrHtmlType}</small>
        <label className={classNames('fieldName', modelBusinessAttrRequired === 1 ? 'required': '')}>{modelBusinessAttrName}</label>
      </Space>
    </div>
  );
};

export default DragFieldItem;
