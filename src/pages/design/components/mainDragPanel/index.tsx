/*
 * @File 主界面右侧拖拽
 * @Author: wwb 
 * @Date: 2021-08-31 17:36:12 
 * @Last Modified by: wwb
 * @Last Modified time: 2021-09-01 03:02:32
 */

import { Tabs } from 'antd';
import SectionAttr from './sectionAttr';
import ComponentList from './componentList';
import DragField from './dragField';
import { useEffect, useState } from 'react';

const { TabPane } = Tabs;


const MainDragPanel = ({ selectedTreeNode }: any) => {

  const [activeKey, setActiveKey] = useState<'1'|'2'|'3'>('1');
  const [disableTab, setDisableTab] = useState<boolean>(false);

  useEffect(() => {
    if(selectedTreeNode?.fields) {
      setActiveKey('3');
      setDisableTab(true);
    } else {
      setActiveKey('1');
      setDisableTab(false);
    }
  }, [selectedTreeNode])


  return (
    <div className='mainDragPanel'>
      <Tabs activeKey={activeKey} onTabClick={key => setActiveKey(key)}>
        <TabPane key='1' tab='组件列表' disabled={disableTab}>
          <ComponentList />
        </TabPane>
        <TabPane key='2' tab='Section 属性' disabled={disableTab}>
          <SectionAttr />
        </TabPane>
        {
          selectedTreeNode?.fields ? 
          <TabPane key='3' tab='字段'>
            <DragField fields={selectedTreeNode?.fields ?? []}/>
          </TabPane>
          : null
        }
      </Tabs>
    </div>
  )
}

export default MainDragPanel;