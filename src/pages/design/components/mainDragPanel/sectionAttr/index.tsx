/*
 * @Author: wwb 
 * @Date: 2021-08-31 17:48:00 
 * @Last Modified by: wwb
 * @Last Modified time: 2021-08-31 17:52:24
 */

import react from 'react';
import { Form, Input } from 'antd';

const FormItem = Form.Item;

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 16 },
};

const SectionAttr = () => {
  
  const [form] = Form.useForm();

  return (
    <Form 
      form={form} 
      {...layout}>
      <FormItem
        name={'pageName'}
        label={'名称'}
        rules={[{ required: true, message: '请输入名称' }]}
      >
        <Input placeholder="页面名称" />
      </FormItem>
      <FormItem
        name={'pageCode'}
        label={'编码'}
        rules={[{ required: true, message: '请输入名称' }]}
      >
        <Input placeholder="页面编码" />
      </FormItem>
    </Form>
  )
}

export default SectionAttr;