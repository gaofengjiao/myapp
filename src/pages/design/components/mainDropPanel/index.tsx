/*
 * @File 选择页面时 中间渲染区域
 * @Author: wwb 
 * @Date: 2021-08-31 17:12:22 
 * @Last Modified by: wwb
 * @Last Modified time: 2021-09-01 01:50:45
 */

import { useRef } from 'react';
import classNames from 'classnames';
import type { DropTargetMonitor} from 'react-dnd';
import { useDrag, useDrop } from 'react-dnd';
import _ from 'lodash';
import ItemTypes from '../../playground/ItemTypes';
import ViewTable from './viewTable';
import './index.less';


const MainDropPanel = ({ treeData, handleBindVisible, selectedTreeNode, handleChangeTreeData, insertInToTreeChildren }: any) => {
  const pageRef = useRef(null); // pageRef
  console.log(selectedTreeNode,'selectedTreeNode - MainDropPanel')
  /**
   * @description 搜索条件区域拖拽
   */
   const [{ isDragging }, dragRef, dragPreview] = useDrag({
    item: { type: ItemTypes.Fields },
    end: (item, monitor) => {
      const dropResult = monitor.getDropResult();
      if (item && dropResult) {
        // alert(`You dropped into ${dropResult.name}!`);
      }
    },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });

  const [ collectedProps, dropRef] = useDrop({
    accept: ItemTypes.Fields,
    collect: (minoter: DropTargetMonitor) => ({
      isOver: minoter.isOver(),
      item: minoter.getItem(),
    }),
    hover: (item, monitor) => {
      // 只检查被hover的最小元素
      const didHover = monitor.isOver({ shallow: true });
      if (didHover) {
        // 确定屏幕上矩形范围
        // const hoverBoundingRect = boxRef.current!.getBoundingClientRect();
        // console.log(hoverBoundingRect,'hoverBoundingRect')
        // 确定鼠标位置
        // const clientOffset = monitor.getClientOffset();
        // 获取距顶部距离
        // const hoverClientX = (clientOffset as XYCoord).x - hoverBoundingRect.left;
        // console.log(hoverClientX,'hoverClientX')
      }
    },
    drop: (item: any, monitor) => {
      console.log(item,'item')
      const target = item.title === '列表' ? [{ type: 'object', title: '列表', id: 'table1', key: 'table1', properties: {} }]: [];
      const newTreeData = _.cloneDeep(treeData);
      const [ newTree, currSelected] = insertInToTreeChildren(
        newTreeData,
        selectedTreeNode.id,
        target,
        'children'
      )
      if (handleChangeTreeData) {
        handleChangeTreeData(newTree, currSelected);
      }
      if(item.isDataSet) {
        if(handleBindVisible) {
          handleBindVisible(true);
        }
      }
    },
  });

  dragPreview(dropRef(pageRef));

  return (
    <div className='mainDropContainer' ref={pageRef}>
      <div className={classNames('dropArea', collectedProps.isOver ? 'dropOver' : '')}>
        {
          selectedTreeNode.key === 'table1' ? <ViewTable />: null
        }
      </div>
    </div>
  )
}

export default MainDropPanel;