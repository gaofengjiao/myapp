/*
 * @Author: wwb 
 * @Date: 2021-09-01 00:20:08 
 * @Last Modified by: wwb
 * @Last Modified time: 2021-09-01 01:17:03
 */

import react, { useEffect, useState } from 'react';
import SectionHeader from '../sectionHeader';
import { Table } from 'antd';

const ViewTable = () => {
  
  const [viewDataSource, setViewDataSource] = useState<any[]>([]);

  useEffect(() => {
    const viewData = [];
    for(let i = 0; i < 10; i +=1) {
      viewData.push({
        id: i,
        field1: '模拟数据',
        field2: '模拟数据',
        field3: '模拟数据',
      })
    }
    setViewDataSource(viewData);
  },[])

  const columns = [
    {
      title: '样例字段',
      dataIndex: 'field1'
    },
    {
      title: '样例字段',
      dataIndex: 'field2'
    },
    {
      title: '样例字段',
      dataIndex: 'field3'
    }
  ]
  return (
    <div className='viewTableContainer'>
      <div className='viewBtnGroup'>
        <SectionHeader title={'Section 按钮区:'}/>
        <div className='viewBtnContent'>
          <a>+ 添加自定义按钮</a>
        </div>
      </div>
      <div className='viewTableWrapper'>
        <div className='viewTableContent'>
          <div className='viewTag'>表格</div>
          <Table 
            bordered={false}
            size='small'
            rowKey='id'
            columns={columns}
            dataSource={viewDataSource}
            pagination={{
              pageSize: 3,
              size: 'default'
            }}
          />
        </div>
      </div>
    </div>
  )
}

export default ViewTable