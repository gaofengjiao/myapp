/*
 * @File 标题 + 竖线
 * @Author: wwb
 * @Date: 2021-08-30 01:09:48
 * @Last Modified by: wwb
 * @Last Modified time: 2021-08-30 01:12:25
 */

import './index.less';

const SectionHeader = ({ title }) => {
  return (
    <div className="section-header">
      <div className="section-header-title">{<span className="headerTitle">{title}</span>}</div>
    </div>
  );
};

export default SectionHeader;
