import App from '@/pages/design/playground/main';
import { DndProvider } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';

const design = () => {
  return (
    <div style={{ background: '#fff', height: 'calc(100vh - 48px)',borderLeft:'1px solid #eee' }}>
      <DndProvider backend={HTML5Backend}>
        <App />
      </DndProvider>
    </div>
  );
};
export default design;
