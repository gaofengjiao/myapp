import type { FC } from 'react';
import { useEffect } from 'react';
import { Form, Input, Button, Space, Radio, Col } from 'antd';
import { Row } from 'antd';

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

interface DataAttrIProps {
  data: any;
  onSumbit?: any;
  onCancel?: () => void;
}
const DataAttr: FC<DataAttrIProps> = (props) => {
  const { data } = props;

  const { onSumbit, onCancel } = props;
  const [form] = Form.useForm();
  const onFinish = (values: any) => {
    onSumbit(values);
  };
  useEffect(() => {
    if (data) {
      form.setFieldsValue({ modelBusinessName: data.modelBusinessName });
      form.setFieldsValue({ modelBusinessCode: data.modelBusinessCode });
      form.setFieldsValue({ dataBusinessName: data.modelBusinessName });
      form.setFieldsValue({ dataBusinessCode: data.modelBusinessCode });
    }
  }, [data]);
  return (
    <Form {...layout} form={form} onFinish={onFinish}>
      <Row>
        <Col span={12}>
          <Form.Item name="dataBusinessName" label="数据源名称" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item name="dataBusinessCode" label="数据源编码" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
        </Col>
      </Row>
      <Row>
        <Col span={12}>
          <Form.Item name="modelBusinessName" label="业务对象名称" rules={[{ required: true }]}>
            <Input disabled />
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item name="modelBusinessCode" label="业务对象编码" rules={[{ required: true }]}>
            <Input disabled />
          </Form.Item>
        </Col>
      </Row>
      {/* <Form.Item {...tailLayout}>
                <Space>
                    <Button type="primary" htmlType="submit">修改</Button>
                    <Button type="default" onClick={onCancel}>取消</Button>
                </Space>
            </Form.Item> */}
    </Form>
  );
};
export default DataAttr;
