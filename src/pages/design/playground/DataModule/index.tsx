import type { FC } from 'react';
import { useState, useEffect } from 'react';
import { Col, Row, Tabs, Divider, Modal, Form, Select, Menu } from 'antd';
import { PlusCircleFilled } from '@ant-design/icons';
import DataAttr from './DataAttr';
import _ from 'lodash';

const { Option } = Select;
const { TabPane } = Tabs;

const DataModule: FC = () => {
  const [visible, setVisible] = useState(false);
  const [dataList, setDataList] = useState([]);
  const [modelBusinessList, setmodelBusinessList] = useState([]);
  const [form] = Form.useForm();
  const [currentItem, setCurrentItem] = useState<any>();
  const [selectCurrentItem, setSelectCurrentItem] = useState();
  const onCancel = () => {
    setVisible(false);
  };
  const handleSave = () => {
    form.validateFields().then((values: any) => {
      const cloneData: any = _.cloneDeep(dataList);
      cloneData.push(currentItem);
      const uniqData = _.uniqBy(cloneData,'modelBusinessCode')
      localStorage.setItem('dataSetList', JSON.stringify(cloneData));
      setDataList(cloneData);
      // 添加分类成功关闭弹出框
      onCancel();
    });
  };
  const handleOnChange = (value: any) => {
    const currData = _.filter(modelBusinessList, (item: any) => item.modelBusinessCode === value);
    console.log(value, 'currData1');
    console.log(currData, 'currData1');
    if (currData && currData.length) {
      console.log(currData, 'currData2');
      setCurrentItem(currData[0]);
    }
  };
  const onSelect = (item: any) => {
    setSelectCurrentItem(item);
  };
  useEffect(() => {
    const tempData = localStorage.getItem('modelBusinessList')
      ? JSON.parse(localStorage.getItem('modelBusinessList'))
      : [];
    setmodelBusinessList(tempData);
    const tempDataSetList = localStorage.getItem('dataSetList')
      ? JSON.parse(localStorage.getItem('dataSetList'))
      : [];
    setDataList(tempDataSetList);
  }, []);
  return (
    <Row className='dataModule'>
      <Col span={4} style={{ padding: 12 }}>
        <a onClick={() => setVisible(true)}>
          <PlusCircleFilled />
        </a>
        <Menu style={{ height: '100%' }}>
          {dataList.map((item: any) => {
            return (
              <Menu.Item onClick={() => onSelect({ ...item })} key={item.modelBusinessCode}>
                {item.modelBusinessName}
              </Menu.Item>
            );
          })}
        </Menu>
      </Col>
      <Col span={1} style={{ textAlign: 'center' }}>
        <Divider type="vertical" style={{ height: 'calc(100vh - 100px)' }} />
      </Col>
      <Col span={19}>
        <Tabs defaultActiveKey="1">
          <TabPane tab="数据属性" key="1">
            {selectCurrentItem ? <DataAttr data={selectCurrentItem} /> : null}
          </TabPane>
          <TabPane tab="处理逻辑" key="2"></TabPane>
          <TabPane tab="字段" key="3"></TabPane>
        </Tabs>
      </Col>
      <Modal
        title="选择业务对象"
        visible={visible}
        onOk={handleSave}
        onCancel={onCancel}
        destroyOnClose={true}
      >
        <Form form={form}>
          <Form.Item name="pageTypeCode" label="业务对象" rules={[{ required: true }]}>
            <Select onChange={(value) => handleOnChange(value)}>
              {modelBusinessList.map((item: any) => (
                <Option key={item.modelBusinessCode} value={item.modelBusinessCode}>
                  {item.modelBusinessName}
                </Option>
              ))}
            </Select>
          </Form.Item>
        </Form>
      </Modal>
    </Row>
  );
};
export default DataModule;
