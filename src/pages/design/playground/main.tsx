import { useEffect, useState } from 'react';
import {
  Designer,
  // DesignerToolsWidget,
  // ViewToolsWidget,
  Workspace,
  // OutlineTreeWidget,
  // DragSourceWidget,
  // HistoryWidget,
  MainPanel,
  // CompositePanel,
  WorkspacePanel,
  // ToolbarPanel,
  // ViewportPanel,
  // ViewPanel,
  // SettingsPanel,
  // ComponentTreeWidget,
  // useSelected,
} from '@/pages/design/visualSource/@designable/react';
// import { transformToSchema } from '@/pages/design/visualSource/@designable/formily'
import { Row, Col, Space, Button, Tabs, Tree, Form, Input, Drawer } from 'antd';
import {
  FormOutlined,
  UngroupOutlined,
  HomeOutlined,
  // ArrowLeftOutlined,
  // ApiOutlined,
} from '@ant-design/icons';
import classnames from 'classnames';
// import { SettingsForm } from '@/visualSource/@designable/react-settings-form'
import {
  createDesigner,
  GlobalRegistry,
  Shortcut,
  KeyCode,
} from '@/pages/design/visualSource/@designable/core';
import { createDesignableField, createDesignableForm } from '../src';
// import {
// LogoWidget,
// ActionsWidget,
// PreviewWidget,
// SchemaEditorWidget,
// MarkupSchemaWidget,
// } from './widgets';
import { saveSchema } from './service';
import './main.less';
// import { SettingsForm } from '@/pages/design/visualSource/@designable/react-settings-form';
import _ from 'lodash';
import { DataSetItem } from '../components/dataSetCard';
// import type { DataSourceOrinigeType } from '../components/dataSetCard/typing';
import { changeTreePositionData, insertInToTreeChildren } from './utils';

// import DragField from '../components/dragField';

import DropContianer from '../components/dropContianer';
import MainDropPanel from '../components/mainDropPanel';
import MainDragPanel from '../components/mainDragPanel';

import DataModule from './DataModule';

const { TabPane } = Tabs;
const FormItem = Form.Item;

const defaultTreeData = [
  {
    title: '产品管理',
    key: '0-0',
    id: '0-0',
    isGroup: true,
    children: [
      {
        title: '默认Section',
        key: '0-1',
        id: '0-1',
        isPage: true,
        children: [],
      },
    ],
  },
];

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 14 },
};

GlobalRegistry.registerDesignerLocales({
  'zh-CN': {
    sources: {
      Inputs: '输入控件',
      Layouts: '布局组件',
      Arrays: '自增组件',
      Customer: '自定义组件',
    },
  },
  'en-US': {
    sources: {
      Inputs: 'Inputs',
      Layouts: 'Layouts',
      Arrays: 'Arrays',
      Customer: 'Customer',
    },
  },
});

// const Root = createDesignableForm({
//   registryName: 'Root',
// });

// const DesignableField = createDesignableField({
//   registryName: 'DesignableField',
// });

const SaveShortCut = new Shortcut({
  codes: [
    [KeyCode.Meta, KeyCode.S],
    [KeyCode.Control, KeyCode.S],
  ],
  handler(ctx) {
    saveSchema(ctx.engine);
  },
});

const engine = createDesigner({
  shortcuts: [SaveShortCut],
});

/**
 *
 * CompositePanel： 左侧配置面板
 * Workspace： 中间拖拽生成区块
 *  - ViewportPanel： 视图面板
 *    -ViewPanel type="DESIGNABLE" 设计面板(拖拽生成区)
 *    -ViewPanel type="JSONTREE" json schema 结构树
 *    -ViewPanel type="MARKUP" 代码块 jsx
 *    -ViewPanel type="PREVIEW" 预览界面
 *
 *
 * SettingsPanel： 右侧属性配置面板
 */

type designType = 'formily' | 'demo' | undefined;

const App = () => {
  const [form] = Form.useForm();
  // const [dataSourceForm] = Form.useForm(); // 数据源form
  // const [sectionForm] = Form.useForm(); // sectionForm
  const [activeKey, setActiveKey] = useState<'1' | '2'>('1');
  const [currSelectedNode, setCurrSelectedNode] = useState<any>('Root');
  const [treeData, setTreeData] = useState(defaultTreeData);
  const [selectedTreeNode, setSelectedTreeNode] = useState<any>(defaultTreeData[0]); // 选中的树节点信息
  const [treeSelectedKeys, setTreeSelectedKeys] = useState<string[]>(['0-0']); // 选中的树节点key

  const [selectedDataSource, setSelectedDataSource] = useState<any[]>([]); // 选中的数据源

  const [treeSelectedDataSet, setTreeSelectedDataSet] = useState<any[]>([]); // 选择数据源进行绑定，绑定数据源

  const [bindDataSetList, setBindDataSetList] = useState([]); // 绑定的数据集

  const [designPageId, setDesignPageId] = useState(''); // 选中的页面id
  const [designType, setDesignType] = useState<designType>(undefined); // 渲染设计器 类型
  const [bindDataDrawVisible, setBindDataDrawVisible] = useState(false);

  useEffect(() => {
    if (localStorage.getItem('dataSetList')) {
      const localData = localStorage.getItem('dataSetList');
      const bindDataSet = localData && JSON.parse(localData);
      setBindDataSetList(bindDataSet);
    }
  }, []);

  const formatTreeNodes = (child: any, result: any, pIndex: number) => {
    // eslint-disable-next-line no-param-reassign
    result[pIndex].children = [];
    if (child.children && child.children.length) {
      child.children.forEach((son, sIndex) => {
        result[pIndex].children.push({
          id: son.id,
          key: son.id,
          title: son.props['x-component-props']?.title,
          'x-component': son.props['x-component'],
          'x-component-props': son.props['x-component-props'],
        });
        formatTreeNodes(son, result[pIndex].children, sIndex);
      });
    } else {
      result[pIndex].children.push({
        id: child.id,
        key: child.id,
        title: child.props['x-component-props']?.title,
        'x-component': child.props['x-component'],
        'x-component-props': child.props['x-component-props'],
      });
    }
  };

  const handleChange = (node: any, selected: any) => {
    if (node) {
      if (node.depth === 0 && selected[0] === node.id) {
        setCurrSelectedNode('Root');
      } else {
        setCurrSelectedNode(node.id);
      }
      const result: any[] = [];
      if (node.children && node.children.length) {
        node.children.forEach((child: any, index: number) => {
          if (child.children && child.children.length) {
            // eslint-disable-next-line no-param-reassign
            // child.children = [];
            result[index] = {
              id: child.id,
              key: child.id,
              title: child.props['x-component-props']?.title,
              'x-component': child.props['x-component'],
              'x-component-props': child.props['x-component-props'],
            };
            formatTreeNodes(child, result, index);
          } else {
            result[index] = {
              id: child.id,
              key: child.id,
              title: child.props['x-component-props']?.title,
              'x-component': child.props['x-component'],
              'x-component-props': child.props['x-component-props'],
            };
          }
        });

        // console.log(result,'result')
        /**
         * 重新生成 左侧树结构
         */
        for (let i = 0; i < treeData.length; i += 1) {
          const { children } = treeData[i];
          for (let k = 0; k < children.length; k += 1) {
            const child = children[k];
            if (child.key === designPageId) {
              child.children = result;
              break;
            }
          }
        }
        setTreeData([...treeData]);
      }
    }
  };

  const onSelectTreeNode = (selectedKeys, e) => {
    // 这里要设置为当前节点id, selectedKeys 二次点击后会取消选中
    setTreeSelectedKeys([e.node.id]);
    setSelectedTreeNode(e.node);
    // 判断点击左侧是哪一种节点
    if (e.node.isPage) {
      setDesignType('demo');
      setDesignPageId(e.node.id);
    } else if (e.node.isGroup) {
      setDesignType(undefined);
      setDesignPageId(e.node.id);
    } else {
      setDesignType('formily');
    }
  };

  const getTreeNode = (list: any[], id: string, fields: any[]) => {
    // eslint-disable-next-line no-restricted-syntax
    if (Array.isArray(list)) {
      for (let i = 0; i < list.length; i += 1) {
        const item = list[i];
        if (item?.id === id) {
          item.fields = fields;
        }
        if (item?.children) {
          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          getTreeNode(item.children, id, fields);
        }
      }
    }
  };

  /**
   * @description 选中容器，绑定数据源
   */
  const toggleBindDataSource = (data: any) => {
    console.log(data, 'data');
    const newTreeData = _.cloneDeep(treeData);
    if (treeSelectedDataSet.length) {
      if (treeSelectedDataSet[0].modelBusinessCode !== data.modelBusinessCode) {
        const [newTree, currSelected] = changeTreePositionData(
          newTreeData,
          treeSelectedKeys[0],
          data.properties,
        );
        setTreeSelectedDataSet([data]);
        setTreeData([...newTree]);
        setSelectedTreeNode(currSelected);
      } else {
        const [newTree, currSelected] = changeTreePositionData(
          newTreeData,
          treeSelectedKeys[0],
          [],
        );
        setTreeSelectedDataSet([]);
        setTreeData([...newTree]);
        setSelectedTreeNode(currSelected);
      }
    } else {
      const [newTree, currSelected] = changeTreePositionData(
        newTreeData,
        treeSelectedKeys[0],
        data.properties,
      );

      console.log(newTree, 'newTree');

      setTreeSelectedDataSet([data]);
      setTreeData([...newTree]);
      setSelectedTreeNode(currSelected);
    }
    setBindDataDrawVisible(false);
  };

  /**
   * @description 更新treeData
   * @param data
   */
  const handleChangeTreeData = (data: any[], selectedNodes?: any) => {
    setTreeData(data);
    if (selectedNodes) {
      setSelectedTreeNode(selectedNodes);
      setTreeSelectedKeys([selectedNodes.id]);
      setDesignType('demo');
    }
  };

  /**
   * @description Drawer visible 变化回调
   */
  const afterVisibleChange = (visible: boolean) => {
    if (!visible) {
    }
  };

  const handleBindVisible = (visible: boolean) => {
    setBindDataDrawVisible(visible);
  };

  /**
   * @description 中间渲染区域 分区渲染内容
   * @param node 展示选中节点渲染内容区块
   * @param showOutLine boolean 展示formily 设计器
   */
  const renderCenterArea = (node: any, designType: designType) => {
    console.log(node, 'node');
    if (node && designType === 'formily') {
      return (
        <div className="dropContainer">
          <DropContianer
            selectedTreeNode={selectedTreeNode}
            changeTreePositionData={changeTreePositionData}
            handleChangeTreeData={handleChangeTreeData}
            treeData={treeData}
          />
          {/* <Designer engine={engine}>
            <MainPanel>
              <Workspace id="form">
                <WorkspacePanel>
                  
                </WorkspacePanel>
              </Workspace> */}
          {/* {activeKey === '1' && designPageId ? (
                <div className="rightPanel">
                  <Tabs>
                    <TabPane tab="组件列表" key="components">
                      <SettingsPanel>
                        {currSelectedNode === 'Root' ? (
                          <div>组件列表</div>
                        ) : (
                          <SettingsForm uploadAction="https://www.mocky.io/v2/5cc8019d300000980a055e76" />
                        )}
                      </SettingsPanel>
                    </TabPane>
                    <TabPane tab="Section 属性" key="sectionAttr">
                      <Form form={sectionForm} {...layout}>
                        <FormItem
                          name={'pageName'}
                          label={'名称'}
                          rules={[{ required: true, message: '请输入名称' }]}
                        >
                          <Input placeholder="页面名称" />
                        </FormItem>
                        <FormItem
                          name={'pageCode'}
                          label={'编码'}
                          rules={[{ required: true, message: '请输入名称' }]}
                        >
                          <Input placeholder="页面编码" />
                        </FormItem>
                      </Form>
                    </TabPane>
                    {selectedTreeNode?.fields && selectedTreeNode?.fields.length ? (
                      <TabPane tab="字段" key="field">
                        <div className="dragFieldWrapper">
                          {selectedTreeNode.fields.map((field) => (
                            <DragField
                              key={field.id}
                              {...field}
                              treeData={treeData}
                              selectedTreeNode={selectedTreeNode}
                              handleChangeTreeData={handleChangeTreeData}
                              changeTreePositionData={changeTreePositionData}
                            />
                          ))}
                        </div>
                      </TabPane>
                    ) : null}
                  </Tabs>
                </div>
              ) : (
                <SettingsPanel title="panels.dataSource">
                  <div>111212</div>
                </SettingsPanel>
              )} */}
          {/* </MainPanel>
          </Designer> */}
        </div>
      );
    }
    if (node && node.title === '列表' && !bindDataDrawVisible && designType === 'demo') {
      return (
        <DropContianer
          selectedTreeNode={selectedTreeNode}
          changeTreePositionData={changeTreePositionData}
          handleChangeTreeData={handleChangeTreeData}
          treeData={treeData}
        />
      );
    }
    if (node && designType === 'demo') {
      return (
        <div className="mainDropPanel">
          <MainDropPanel
            handleBindVisible={handleBindVisible}
            treeData={treeData}
            handleChangeTreeData={handleChangeTreeData}
            selectedTreeNode={selectedTreeNode}
            changeTreePositionData={changeTreePositionData}
            insertInToTreeChildren={insertInToTreeChildren}
          />
        </div>
      );
    }
    return (
      <Form form={form} {...layout}>
        <Tabs defaultActiveKey={'1'}>
          <TabPane tab={'页面属性'} key="1">
            <Row>
              <Col span={12}>
                <FormItem
                  name={'pageName'}
                  label={'名称'}
                  rules={[{ required: true, message: '请输入名称' }]}
                >
                  <Input placeholder="页面名称" />
                </FormItem>
                <FormItem
                  name={'pageTitle'}
                  label={'标题'}
                  rules={[{ required: true, message: '请输入名称' }]}
                >
                  <Input placeholder="页面标题" />
                </FormItem>
              </Col>
              <Col span={12}>
                <FormItem
                  name={'code'}
                  label={'编码'}
                  rules={[{ required: true, message: '请输入名称' }]}
                >
                  <Input placeholder="页面编码" />
                </FormItem>
              </Col>
            </Row>
          </TabPane>
          <TabPane tab={'页面参数'} key="2"></TabPane>
        </Tabs>
      </Form>
    );
  };

  return (
    <div className="tydicDesign">
      <div className="headerTop">
        <Row
          align="middle"
          justify="space-around"
          gutter={48}
          style={{ height: '100%', padding: '0 24px' }}
        >
          <Col span={6}>
            <label>产品管理</label>
          </Col>
          <Col span={12} style={{ height: '100%' }}>
            <div className="operation-tab">
              <div
                className={classnames('tab-panel-wrapper', activeKey === '1' ? 'active' : '')}
                onClick={() => setActiveKey('1')}
              >
                <a className="tab-panel">
                  <div>
                    <FormOutlined style={{ fontSize: 16 }} />
                  </div>
                  <span>页面设计</span>
                </a>
              </div>
              <div
                className={classnames('tab-panel-wrapper', activeKey === '2' ? 'active' : '')}
                onClick={() => setActiveKey('2')}
              >
                <a className="tab-panel">
                  <div>
                    <UngroupOutlined style={{ fontSize: 16 }} />
                  </div>
                  <span>数据源</span>
                </a>
              </div>
            </div>
          </Col>
          <Col span={6} style={{ textAlign: 'right' }}>
            <Space>
              <Button type="primary">保存编辑</Button>
              <Button type="primary">发布</Button>
            </Space>
          </Col>
        </Row>
      </div>
      {activeKey === '1' ? (
        <Row style={{ height: '100vh' }} gutter={12}>
          {/* 左侧树 */}
          <Col span={5} style={{ borderRight: 'solid 1px #eee', paddingTop: 8 }}>
            <Tree
              treeData={treeData}
              blockNode
              defaultExpandAll
              checkStrictly
              checkedKeys={treeSelectedKeys}
              selectedKeys={treeSelectedKeys}
              onSelect={onSelectTreeNode}
              titleRender={(node: any) => {
                if (node.isPage) {
                  return (
                    <Space>
                      <span>{node.title}</span>
                      <span>
                        <HomeOutlined />
                      </span>
                    </Space>
                  );
                }
                return (
                  <Space>
                    <span>{node.title}</span>
                  </Space>
                );
              }}
            />
          </Col>
          <Col span={12}>{renderCenterArea(selectedTreeNode, designType)}</Col>
          <Col span={5}>
            {designType === undefined ? null : (
              <MainDragPanel selectedTreeNode={selectedTreeNode} />
            )}
          </Col>
        </Row>
      ) : (
        <DataModule />
      )}
      <Drawer
        title={'组件添加数据源'}
        visible={bindDataDrawVisible}
        placement="right"
        width={500}
        // onClose={() => setBindDataDrawVisible(false)}
        afterVisibleChange={afterVisibleChange}
      >
        <Row>
          <Col span={24}>
            <Row wrap gutter={[24, 12]}>
              {bindDataSetList.map((d: any, index: number) => (
                <DataSetItem
                  key={d.modelBusinessCode}
                  data={d}
                  index={index}
                  selectedDataSource={treeSelectedDataSet}
                  toggleSelectedDataSetList={toggleBindDataSource}
                />
              ))}
            </Row>
          </Col>
        </Row>
      </Drawer>
    </div>
  );
};

export default App;
