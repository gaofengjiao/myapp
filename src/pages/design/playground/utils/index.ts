/*
 * @Author: wwb
 * @Date: 2021-08-29 22:06:48
 * @Last Modified by: wwb
 * @Last Modified time: 2021-09-01 01:54:50
 */

/**
 * @description 指定节点添加属性
 * @param tree 数据源
 * @param id 树节点
 * @param fields 绑定的属性
 * @returns
 */
export const changeTreePositionData = (tree: any, id: string, fields: any[], key?: string) => {
  let currSelected = {};
  const searchTarget = (source: any[], searchId: string, treeKey?: string) => {
    for (let i = 0; i < source.length; i += 1) {
      const item = source[i];
      if (item.id === searchId) {
        if (treeKey) {
          if (treeKey === 'x-component-queryField' || key === 'x-component-tableFields') {
            item[treeKey] = item[treeKey] ? [...item[treeKey], ...fields] : fields;
          }
        } else {
          item.fields = fields;
        }
        currSelected = item;
        return;
      }
      if (item.children && item.children.length) {
        searchTarget(item.children, searchId, key);
      }
    }
  };
  searchTarget(tree, id);
  return [tree, currSelected];
};

export const insertInToTreeChildren = (tree: any, id: string, fields: any[], key?: string) => {
  let currSelected: any = {};
  const searchTarget = (source: any[], searchId: string, treeKey?: string) => {
    for (let i = 0; i < source.length; i += 1) {
      const item = source[i];
      if (item.id === searchId) {
        if (treeKey) {
          item[treeKey] = item[treeKey] ? [...item[treeKey], ...fields] : fields;
          currSelected = { ...fields[0] };
        }
        return;
      }
      if (item.children && item.children.length) {
        searchTarget(item.children, searchId, key);
      }
    }
  };
  searchTarget(tree, id);
  return [tree, currSelected];
};
