import React, { useMemo } from 'react';
import type { IDesignerProps } from '@/pages/design/visualSource/@designable/core';
import { GlobalRegistry } from '@/pages/design/visualSource/@designable/core';
import { createForm } from '@/pages/design/visualSource/@formily/core';
import type { IFormLayoutProps } from '@/pages/design/visualSource/@formily/antd';
import { Form } from '@/pages/design/visualSource/@formily/antd';
import { observer } from '@/pages/design/visualSource/@formily/react';
import { usePrefix } from '@/pages/design/visualSource/@designable/react';
import { Form as FormPropsSchema } from '../../schemas';
import './styles.less';

export interface IDesignableFormFactoryProps extends IDesignerProps {
  registryName: string;
  component?: React.JSXElementConstructor<unknown>;
}

export const createDesignableForm = (options: IDesignableFormFactoryProps) => {
  const realOptions: IDesignableFormFactoryProps = {
    component: Form,
    droppable: true,
    draggable: false,
    propsSchema: FormPropsSchema,
    ...options,
    defaultProps: {
      labelCol: 6,
      wrapperCol: 12,
      ...options.defaultProps,
    },
  };

  const FormComponent = realOptions.component || Form;

  const DesignableForm: React.FC<IFormLayoutProps> = observer((props) => {
    const prefix = usePrefix('designable-form');
    const form = useMemo(
      () =>
        createForm({
          designable: true,
        }),
      [],
    );
    return (
      <FormComponent {...props} style={{ ...props.style }} className={prefix} form={form}>
        {props.children}
      </FormComponent>
    );
  });

  if (!realOptions.registryName) throw new Error('Can not found registryName');

  realOptions.title = `components.${realOptions.registryName}`;

  GlobalRegistry.registerDesignerProps({
    [realOptions.registryName]: realOptions,
  });

  return DesignableForm;
};
