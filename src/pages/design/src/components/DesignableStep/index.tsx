/*
 * @Author: wwb
 * @Date: 2021-08-22 16:14:01
 * @Last Modified by: wwb
 * @Last Modified time: 2021-08-22 16:22:34
 */

import { Steps } from 'antd';

const { Step } = Steps;

export const DesignableStep = () => {
  return (
    <Steps current={1}>
      <Step title="Finished" description="This is a description." />
      <Step title="In Progress" subTitle="Left 00:00:08" description="This is a description." />
      <Step title="Waiting" description="This is a description." />
    </Steps>
  );
};
