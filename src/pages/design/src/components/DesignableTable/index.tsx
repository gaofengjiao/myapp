import React from 'react';
import type { TableProps } from 'antd';
import { Table } from 'antd';
import { Droppable } from '../Droppable';
import { TreeNode } from '@/pages/design/visualSource/@designable/core';
import {
  useTreeNode,
  TreeNodeWidget,
  useNodeIdProps,
} from '@/pages/design/visualSource/@designable/react';
import { ArrayBase } from '@/pages/design/visualSource/@formily/antd';
import { observer } from '@/pages/design/visualSource/@formily/react';
// import { LoadTemplate } from '../LoadTemplate'
import cls from 'classnames';
import {
  createNodeId,
  queryNodesByComponentPath,
  // hasNodeByComponentPath,
  // findNodeByComponentPath,
  // createEnsureTypeItemsNode,
} from '../../shared';
import { useDropTemplate } from '../../hooks';

// const ensureObjectItemsNode = createEnsureTypeItemsNode('object')

export const DesignableTable: React.FC<TableProps<any>> = observer((props) => {
  const node = useTreeNode();
  const nodeId = useNodeIdProps();
  const designer = useDropTemplate('DesignableTable', (source) => {
    const sortHandleNode = new TreeNode({
      componentName: 'DesignableField',
      props: {
        type: 'void',
        'x-component': 'DesignableTable.Column',
        'x-component-props': {
          title: `Title`,
        },
      },
      children: [
        {
          componentName: 'DesignableField',
          props: {
            type: 'void',
            'x-component': 'DesignableTable.SortHandle',
          },
        },
      ],
    });
    const indexNode = new TreeNode({
      componentName: 'DesignableField',
      props: {
        type: 'void',
        'x-component': 'DesignableTable.Column',
        'x-component-props': {
          title: `Title`,
        },
      },
      children: [
        {
          componentName: 'DesignableField',
          props: {
            type: 'void',
            'x-component': 'DesignableTable.Index',
          },
        },
      ],
    });
    const columnNode = new TreeNode({
      componentName: 'DesignableField',
      props: {
        type: 'void',
        'x-component': 'DesignableTable.Column',
        'x-component-props': {
          title: `Title`,
        },
      },
      children: source.map((node) => {
        node.props.title = undefined;
        return node;
      }),
    });

    const operationNode = new TreeNode({
      componentName: 'DesignableField',
      props: {
        type: 'void',
        'x-component': 'DesignableTable.Column',
        'x-component-props': {
          title: `Title`,
        },
      },
      children: [
        {
          componentName: 'DesignableField',
          props: {
            type: 'void',
            'x-component': 'DesignableTable.Remove',
          },
        },
        {
          componentName: 'DesignableField',
          props: {
            type: 'void',
            'x-component': 'DesignableTable.MoveDown',
          },
        },
        {
          componentName: 'DesignableField',
          props: {
            type: 'void',
            'x-component': 'DesignableTable.MoveUp',
          },
        },
      ],
    });
    const objectNode = new TreeNode({
      componentName: 'DesignableField',
      props: {
        type: 'object',
      },
      children: [sortHandleNode, indexNode, columnNode, operationNode],
    });
    // const additionNode = new TreeNode({
    //   componentName: 'DesignableField',
    //   props: {
    //     type: 'void',
    //     title: 'Addition',
    //     'x-component': 'DesignableTable.Addition',
    //   },
    // })
    return [objectNode];
  });
  const columns = queryNodesByComponentPath(node, [
    'DesignableTable',
    '*',
    'DesignableTable.Column',
  ]);
  // const additions = queryNodesByComponentPath(node, [
  //   'DesignableTable',
  //   'DesignableTable.Addition',
  // ])
  const defaultRowKey = () => {
    return node.id;
  };
  const createColumnId = (props: any) => {
    return createNodeId(designer, props.className.match(/data-id\:([^\s]+)/)?.[1]);
  };

  useDropTemplate('DesignableTable.Column', (source) => {
    return source.map((node) => {
      node.props.title = undefined;
      return node;
    });
  });

  return (
    <div {...nodeId}>
      <ArrayBase disabled>
        <Table
          size="small"
          bordered
          {...props}
          scroll={{ x: '100%' }}
          className={cls('ant-formily-array-table', props.className)}
          style={{ marginBottom: 10, ...props.style }}
          rowKey={defaultRowKey}
          dataSource={[{ id: '1' }]}
          pagination={false}
          components={{
            header: {
              cell: (props: any) => {
                return (
                  <th {...props} {...createColumnId(props)}>
                    {props.children}
                  </th>
                );
              },
            },
            body: {
              cell: (props: any) => {
                return (
                  <td {...props} {...createColumnId(props)}>
                    {props.children}
                  </td>
                );
              },
            },
          }}
        >
          {columns.map((node, key) => {
            const children = node.children.map((child) => {
              return <TreeNodeWidget node={child} key={child.id} />;
            });
            return (
              <Table.Column
                {...node.props['x-component-props']}
                dataIndex={node.id}
                className={`data-id:${node.id}`}
                key={key}
                render={(value, record, key) => {
                  return (
                    <ArrayBase.Item key={key} index={key}>
                      {children.length > 0 ? children : 'Droppable'}
                    </ArrayBase.Item>
                  );
                }}
              />
            );
          })}
          {columns.length === 0 && <Table.Column render={() => <Droppable />} />}
        </Table>
        {/* {additions.map((child) => {
            return <TreeNodeWidget node={child} key={child.id} />
          })} */}
      </ArrayBase>
      {/* <LoadTemplate
          actions={[
            {
              title: 'Common.addTableSortHandle',
              onClick: () => {
                if (
                  hasNodeByComponentPath(node, [
                    'DesignableTable',
                    '*',
                    'DesignableTable.Column',
                    'DesignableTable.SortHandle',
                  ])
                )
                  return
                const tableColumn = new TreeNode({
                  componentName: 'DesignableField',
                  props: {
                    type: 'void',
                    'x-component': 'DesignableTable.Column',
                    'x-component-props': {
                      title: `Title`,
                    },
                  },
                  children: [
                    {
                      componentName: 'DesignableField',
                      props: {
                        type: 'void',
                        'x-component': 'DesignableTable.SortHandle',
                      },
                    },
                  ],
                })
                ensureObjectItemsNode(node).prepend(tableColumn)
              },
            },
            {
              title: 'Common.addIndex',
              onClick: () => {
                if (
                  hasNodeByComponentPath(node, [
                    'DesignableTable',
                    '*',
                    'DesignableTable.Column',
                    'DesignableTable.Index',
                  ])
                )
                  return
                const tableColumn = new TreeNode({
                  componentName: 'DesignableField',
                  props: {
                    type: 'void',
                    'x-component': 'DesignableTable.Column',
                    'x-component-props': {
                      title: `Title`,
                    },
                  },
                  children: [
                    {
                      componentName: 'DesignableField',
                      props: {
                        type: 'void',
                        'x-component': 'DesignableTable.Index',
                      },
                    },
                  ],
                })
                const sortNode = findNodeByComponentPath(node, [
                  'DesignableTable',
                  '*',
                  'DesignableTable.Column',
                  'DesignableTable.SortHandle',
                ])
                if (sortNode) {
                  sortNode.parent.insertAfter(tableColumn)
                } else {
                  ensureObjectItemsNode(node).prepend(tableColumn)
                }
              },
            },
            {
              title: 'Common.addTableColumn',
              onClick: () => {
                const operationNode = findNodeByComponentPath(node, [
                  'DesignableTable',
                  '*',
                  'DesignableTable.Column',
                  (name) => {
                    return (
                      name === 'DesignableTable.Remove' ||
                      name === 'DesignableTable.MoveDown' ||
                      name === 'DesignableTable.MoveUp'
                    )
                  },
                ])
                const tableColumn = new TreeNode({
                  componentName: 'DesignableField',
                  props: {
                    type: 'void',
                    'x-component': 'DesignableTable.Column',
                    'x-component-props': {
                      title: `Title`,
                    },
                  },
                })
                if (operationNode) {
                  operationNode.parent.insertBefore(tableColumn)
                } else {
                  ensureObjectItemsNode(node).append(tableColumn)
                }
              },
            },
            {
              title: 'Common.addOperation',
              onClick: () => {
                const oldOperationNode = findNodeByComponentPath(node, [
                  'DesignableTable',
                  '*',
                  'DesignableTable.Column',
                  (name) => {
                    return (
                      name === 'DesignableTable.Remove' ||
                      name === 'DesignableTable.MoveDown' ||
                      name === 'DesignableTable.MoveUp'
                    )
                  },
                ])
                const oldAdditionNode = findNodeByComponentPath(node, [
                  'DesignableTable',
                  'DesignableTable.Addition',
                ])
                if (!oldOperationNode) {
                  const operationNode = new TreeNode({
                    componentName: 'DesignableField',
                    props: {
                      type: 'void',
                      'x-component': 'DesignableTable.Column',
                      'x-component-props': {
                        title: `Title`,
                      },
                    },
                    children: [
                      {
                        componentName: 'DesignableField',
                        props: {
                          type: 'void',
                          'x-component': 'DesignableTable.Remove',
                        },
                      },
                      {
                        componentName: 'DesignableField',
                        props: {
                          type: 'void',
                          'x-component': 'DesignableTable.MoveDown',
                        },
                      },
                      {
                        componentName: 'DesignableField',
                        props: {
                          type: 'void',
                          'x-component': 'DesignableTable.MoveUp',
                        },
                      },
                    ],
                  })
                  ensureObjectItemsNode(node).append(operationNode)
                }
                if (!oldAdditionNode) {
                  const additionNode = new TreeNode({
                    componentName: 'DesignableField',
                    props: {
                      type: 'void',
                      title: 'Addition',
                      'x-component': 'DesignableTable.Addition',
                    },
                  })
                  ensureObjectItemsNode(node).insertAfter(additionNode)
                }
              },
            },
          ]}
        /> */}
    </div>
  );
});

ArrayBase.mixin(DesignableTable);
