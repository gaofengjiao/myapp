/*
 * @File 当前文件配置组件属性 / 组件样式
 * @Author: wwb
 * @Date: 2021-08-22 18:18:09
 * @Last Modified by: wwb
 * @Last Modified time: 2021-08-23 12:06:23
 */

export const DesignableStep = {
  type: 'object',
  properties: {
    title: {
      type: 'string',
      'x-decorator': 'FormItem',
      'x-component': 'Input',
    },
  },
};
