/*
 * @Author: wwb
 * @Date: 2021-08-20 15:45:11
 * @Last Modified by: wwb
 * @Last Modified time: 2021-08-30 16:47:02
 */

import { GlobalDragSource } from '@/pages/design/visualSource/@designable/core';

GlobalDragSource.appendSourcesByGroup('customer', [
  {
    componentName: 'DesignableField',
    props: {
      type: 'void',
      'x-decorator': 'customer',
      'x-component': 'DesignableStep',
    },
  },
  {
    componentName: 'DesignableField',
    props: {
      type: 'void',
      'x-decorator': 'customer',
      'x-component': 'DesignableTable',
    },
  },
]);
