import { EventDriver } from '@/pages/design/visualSource/@designable/shared';
import { Engine } from '../models/Engine';
export declare class MouseClickDriver extends EventDriver<Engine> {
  onMouseClick: (e: MouseEvent) => void;
  attach(): void;
  detach(): void;
}
