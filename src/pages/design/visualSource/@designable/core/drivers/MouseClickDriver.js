var __extends =
  (this && this.__extends) ||
  (function () {
    var extendStatics = function (d, b) {
      extendStatics =
        Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array &&
          function (d, b) {
            d.__proto__ = b;
          }) ||
        function (d, b) {
          for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p];
        };
      return extendStatics(d, b);
    };
    return function (d, b) {
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : ((__.prototype = b.prototype), new __());
    };
  })();
import { EventDriver } from '@/pages/design/visualSource/@designable/shared';
import { MouseClickEvent } from '../events';
var MouseClickDriver = /** @class */ (function (_super) {
  __extends(MouseClickDriver, _super);
  function MouseClickDriver() {
    var _this = (_super !== null && _super.apply(this, arguments)) || this;
    _this.onMouseClick = function (e) {
      var target = e.target;
      if (
        target === null || target === void 0
          ? void 0
          : target.closest('*[data-click-stop-propagation]')
      ) {
        return;
      }
      _this.dispatch(
        new MouseClickEvent({
          clientX: e.clientX,
          clientY: e.clientY,
          pageX: e.pageX,
          pageY: e.pageY,
          target: e.target,
          view: e.view,
        }),
      );
    };
    return _this;
  }
  MouseClickDriver.prototype.attach = function () {
    this.addEventListener('click', this.onMouseClick, {
      once: true,
    });
  };
  MouseClickDriver.prototype.detach = function () {
    this.removeEventListener('click', this.onMouseClick, {
      once: true,
    });
  };
  return MouseClickDriver;
})(EventDriver);
export { MouseClickDriver };
