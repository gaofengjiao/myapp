var __read =
  (this && this.__read) ||
  function (o, n) {
    var m = typeof Symbol === 'function' && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o),
      r,
      ar = [],
      e;
    try {
      while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    } catch (error) {
      e = { error: error };
    } finally {
      try {
        if (r && !r.done && (m = i['return'])) m.call(i);
      } finally {
        if (e) throw e.error;
      }
    }
    return ar;
  };
var __spread =
  (this && this.__spread) ||
  function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
  };
import { ClosestDirection, CursorType } from '../models';
import { DragStartEvent, DragMoveEvent, DragStopEvent, ViewportScrollEvent } from '../events';
import { Point } from '@/pages/design/visualSource/@designable/shared';
export var useDragDropEffect = function (engine) {
  engine.subscribeTo(DragStartEvent, function (event) {
    if (engine.cursor.type !== CursorType.Move) return;
    var target = event.data.target;
    var el =
      target === null || target === void 0
        ? void 0
        : target.closest(
            '\n       *[' +
              engine.props.nodeIdAttrName +
              '],\n       *[' +
              engine.props.sourceIdAttrName +
              '],\n       *[' +
              engine.props.outlineNodeIdAttrName +
              ']\n      ',
          );
    if (!(el === null || el === void 0 ? void 0 : el.getAttribute)) return;
    var sourceId =
      el === null || el === void 0 ? void 0 : el.getAttribute(engine.props.sourceIdAttrName);
    var outlineId =
      el === null || el === void 0 ? void 0 : el.getAttribute(engine.props.outlineNodeIdAttrName);
    var nodeId =
      el === null || el === void 0 ? void 0 : el.getAttribute(engine.props.nodeIdAttrName);
    engine.workbench.eachWorkspace(function (currentWorkspace) {
      var _a, _b;
      var operation = currentWorkspace.operation;
      if (nodeId || outlineId) {
        var node_1 = engine.findNodeById(outlineId || nodeId);
        if (operation.focusNode && operation.focusNode.contains(node_1)) {
          operation.setDragNodes([operation.focusNode]);
          return;
        } else {
          operation.focusClean();
        }
        if (node_1) {
          if (
            ((_a = node_1 === null || node_1 === void 0 ? void 0 : node_1.designerProps) === null ||
            _a === void 0
              ? void 0
              : _a.draggable) === false
          )
            return;
          if (node_1 === node_1.root) return;
          var validSelected = engine.getAllSelectedNodes().filter(function (node) {
            var _a;
            if (node) {
              return (
                ((_a = node === null || node === void 0 ? void 0 : node.designerProps) === null ||
                _a === void 0
                  ? void 0
                  : _a.draggable) !== false && node !== node.root
              );
            }
            return false;
          });
          if (
            validSelected.some(function (selectNode) {
              return selectNode === node_1;
            })
          ) {
            operation.setDragNodes(operation.sortNodes(validSelected));
          } else {
            operation.setDragNodes([node_1]);
          }
        }
      } else if (sourceId) {
        var sourceNode = engine.findSourceNodeById(sourceId);
        if (sourceNode) {
          if (
            ((_b =
              sourceNode === null || sourceNode === void 0 ? void 0 : sourceNode.designerProps) ===
              null || _b === void 0
              ? void 0
              : _b.draggable) === false
          )
            return;
          operation.setDragNodes([sourceNode]);
        }
      }
    });
  });
  engine.subscribeTo(DragMoveEvent, function (event) {
    if (engine.cursor.type !== CursorType.Move) return;
    var target = event.data.target;
    var el =
      target === null || target === void 0
        ? void 0
        : target.closest(
            '\n      *[' +
              engine.props.nodeIdAttrName +
              '],\n      *[' +
              engine.props.outlineNodeIdAttrName +
              ']\n    ',
          );
    var nodeId =
      el === null || el === void 0 ? void 0 : el.getAttribute(engine.props.nodeIdAttrName);
    var outlineId =
      el === null || el === void 0 ? void 0 : el.getAttribute(engine.props.outlineNodeIdAttrName);
    engine.workbench.eachWorkspace(function (currentWorkspace) {
      var operation = currentWorkspace.operation;
      var tree = operation.tree;
      var point = new Point(event.data.topClientX, event.data.topClientY);
      var dragNodes = operation.getDragNodes();
      if (!dragNodes.length) return;
      var touchNode = tree.findById(outlineId || nodeId);
      operation.dragWith(point, touchNode);
    });
  });
  engine.subscribeTo(ViewportScrollEvent, function (event) {
    var _a, _b;
    if (engine.cursor.type !== CursorType.Move) return;
    var point = new Point(engine.cursor.position.topClientX, engine.cursor.position.topClientY);
    var currentWorkspace =
      (_a = event === null || event === void 0 ? void 0 : event.context) === null || _a === void 0
        ? void 0
        : _a.workspace;
    if (!currentWorkspace) return;
    var operation = currentWorkspace.operation;
    if (!((_b = operation.getDragNodes()) === null || _b === void 0 ? void 0 : _b.length)) return;
    var tree = operation.tree;
    var viewport = currentWorkspace.viewport;
    var outline = currentWorkspace.outline;
    var viewportTarget = viewport.elementFromPoint(point);
    var outlineTarget = outline.elementFromPoint(point);
    var viewportNodeElement =
      viewportTarget === null || viewportTarget === void 0
        ? void 0
        : viewportTarget.closest(
            '\n      *[' +
              engine.props.nodeIdAttrName +
              '],\n      *[' +
              engine.props.outlineNodeIdAttrName +
              ']\n    ',
          );
    var outlineNodeElement =
      outlineTarget === null || outlineTarget === void 0
        ? void 0
        : outlineTarget.closest(
            '\n    *[' +
              engine.props.nodeIdAttrName +
              '],\n    *[' +
              engine.props.outlineNodeIdAttrName +
              ']\n  ',
          );
    var nodeId =
      viewportNodeElement === null || viewportNodeElement === void 0
        ? void 0
        : viewportNodeElement.getAttribute(engine.props.nodeIdAttrName);
    var outlineNodeId =
      outlineNodeElement === null || outlineNodeElement === void 0
        ? void 0
        : outlineNodeElement.getAttribute(engine.props.outlineNodeIdAttrName);
    var touchNode = tree.findById(outlineNodeId || nodeId);
    operation.dragWith(point, touchNode);
  });
  engine.subscribeTo(DragStopEvent, function () {
    if (engine.cursor.type !== CursorType.Move) return;
    engine.workbench.eachWorkspace(function (currentWorkspace) {
      var operation = currentWorkspace.operation;
      var dragNodes = operation.getDragNodes();
      var closestNode = operation.getClosestNode();
      var closestDirection = operation.getClosestDirection();
      var selection = operation.selection;
      if (!dragNodes.length) return;
      if (dragNodes.length && closestNode && closestDirection) {
        if (
          closestDirection === ClosestDirection.After ||
          closestDirection === ClosestDirection.Under
        ) {
          if (closestNode.allowSibling(dragNodes)) {
            selection.batchSafeSelect(
              closestNode.insertAfter.apply(closestNode, __spread(dragNodes)),
            );
          }
        } else if (
          closestDirection === ClosestDirection.Before ||
          closestDirection === ClosestDirection.Upper
        ) {
          if (closestNode.allowSibling(dragNodes)) {
            selection.batchSafeSelect(
              closestNode.insertBefore.apply(closestNode, __spread(dragNodes)),
            );
          }
        } else if (
          closestDirection === ClosestDirection.Inner ||
          closestDirection === ClosestDirection.InnerAfter
        ) {
          if (closestNode.allowAppend(dragNodes)) {
            selection.batchSafeSelect(closestNode.append.apply(closestNode, __spread(dragNodes)));
            operation.setDropNode(closestNode);
          }
        } else if (closestDirection === ClosestDirection.InnerBefore) {
          if (closestNode.allowAppend(dragNodes)) {
            selection.batchSafeSelect(closestNode.prepend.apply(closestNode, __spread(dragNodes)));
            operation.setDropNode(closestNode);
          }
        }
      }
      operation.dragClean();
    });
  });
};
