import { ICustomEvent } from '@/pages/design/visualSource/@designable/shared';
import { AbstractCursorEvent } from './AbstractCursorEvent';
export declare class DragStopEvent extends AbstractCursorEvent implements ICustomEvent {
  type: string;
}
