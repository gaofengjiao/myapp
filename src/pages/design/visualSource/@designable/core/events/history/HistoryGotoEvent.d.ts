import { ICustomEvent } from '@/pages/design/visualSource/@designable/shared';
import { AbstractHistoryEvent } from './AbstractHistoryEvent';
export declare class HistoryGotoEvent extends AbstractHistoryEvent implements ICustomEvent {
  type: string;
}
