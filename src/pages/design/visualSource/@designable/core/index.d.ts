import { Engine } from './models/Engine';
import { IEngineProps } from './types';
export * from './registry';
export * from './models';
export * from './events';
export * from './types';
export declare const createDesigner: (props?: IEngineProps<Engine>) => Engine;
