var __assign =
  (this && this.__assign) ||
  function () {
    __assign =
      Object.assign ||
      function (t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
      };
    return __assign.apply(this, arguments);
  };
var __read =
  (this && this.__read) ||
  function (o, n) {
    var m = typeof Symbol === 'function' && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o),
      r,
      ar = [],
      e;
    try {
      while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    } catch (error) {
      e = { error: error };
    } finally {
      try {
        if (r && !r.done && (m = i['return'])) m.call(i);
      } finally {
        if (e) throw e.error;
      }
    }
    return ar;
  };
var __spread =
  (this && this.__spread) ||
  function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
  };
import { DEFAULT_DRIVERS, DEFAULT_EFFECTS, DEFAULT_SHORTCUTS } from './presets';
import { Engine } from './models/Engine';
export * from './registry';
export * from './models';
export * from './events';
export * from './types';
export var createDesigner = function (props) {
  if (props === void 0) {
    props = {};
  }
  var drivers = props.drivers || [];
  var effects = props.effects || [];
  var shortcuts = props.shortcuts || [];
  return new Engine(
    __assign(__assign({}, props), {
      effects: __spread(effects, DEFAULT_EFFECTS),
      drivers: __spread(drivers, DEFAULT_DRIVERS),
      shortcuts: __spread(shortcuts, DEFAULT_SHORTCUTS),
    }),
  );
};
