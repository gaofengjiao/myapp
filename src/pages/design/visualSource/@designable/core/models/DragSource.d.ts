import { TreeNode, ITreeNode } from './TreeNode';
export declare class DragSource {
  tree: TreeNode;
  prefix: string;
  constructor();
  get size(): number;
  setSources(sources: Record<string, ITreeNode[]>): void;
  setSourcesByGroup(group: string, sources: ITreeNode[]): void;
  appendSourcesByGroup(group: string, sources: ITreeNode[]): void;
  getAllGroup(): TreeNode[];
  getAllSources(): TreeNode[];
  getSourcesByGroup(group: string): TreeNode[];
  mapSourcesByGroup(group: string, callback?: (node: TreeNode) => void): void[];
  eachSourcesByGroup(group: string, callback?: (node: TreeNode) => void): void;
  reduceSourcesByGroup<T>(
    group: string,
    callback?: (previousValue: T, currentValue: TreeNode, index: number, array: TreeNode[]) => T,
    init?: T,
  ): T;
}
export declare const GlobalDragSource: DragSource;
