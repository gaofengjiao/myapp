var __read =
  (this && this.__read) ||
  function (o, n) {
    var m = typeof Symbol === 'function' && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o),
      r,
      ar = [],
      e;
    try {
      while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    } catch (error) {
      e = { error: error };
    } finally {
      try {
        if (r && !r.done && (m = i['return'])) m.call(i);
      } finally {
        if (e) throw e.error;
      }
    }
    return ar;
  };
var __spread =
  (this && this.__spread) ||
  function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
  };
import { each, uid } from '@/pages/design/visualSource/@designable/shared';
import { TreeNode } from './TreeNode';
var DragSource = /** @class */ (function () {
  function DragSource() {
    this.tree = new TreeNode({
      isSourceNode: true,
      componentName: 'SourceRoot',
    });
    this.prefix = uid();
  }
  Object.defineProperty(DragSource.prototype, 'size', {
    get: function () {
      return this.getAllSources().length;
    },
    enumerable: false,
    configurable: true,
  });
  DragSource.prototype.setSources = function (sources) {
    var _this = this;
    each(sources, function (data, group) {
      _this.setSourcesByGroup(group, data);
    });
  };
  DragSource.prototype.setSourcesByGroup = function (group, sources) {
    var parent = this.tree.findById(group);
    var nodes = sources.map(function (node) {
      return new TreeNode(node);
    });
    if (parent) {
      parent.setChildren.apply(parent, __spread(nodes));
    } else {
      var newParent = new TreeNode({
        componentName: 'SourceGroup',
        id: this.prefix + '_' + group,
      });
      newParent.setChildren.apply(newParent, __spread(nodes));
      this.tree.append(newParent);
    }
  };
  DragSource.prototype.appendSourcesByGroup = function (group, sources) {
    var parent = this.tree.findById(group);
    var nodes = sources.map(function (node) {
      return new TreeNode(node);
    });
    if (parent) {
      parent.append.apply(parent, __spread(nodes));
    } else {
      var newParent = new TreeNode({
        componentName: 'SourceGroup',
        id: this.prefix + '_' + group,
      });
      newParent.setChildren.apply(newParent, __spread(nodes));
      this.tree.append(newParent);
    }
  };
  DragSource.prototype.getAllGroup = function () {
    var nodes = this.tree.findAll(function (node) {
      return node.componentName === 'SourceGroup';
    });
    return nodes;
  };
  DragSource.prototype.getAllSources = function () {
    var _this = this;
    return this.getAllGroup().reduce(function (buf, groupNode) {
      return buf.concat(_this.getSourcesByGroup(groupNode.id));
    }, []);
  };
  DragSource.prototype.getSourcesByGroup = function (group) {
    var parent = this.tree.findById(this.prefix + '_' + group);
    return parent === null || parent === void 0 ? void 0 : parent.children;
  };
  DragSource.prototype.mapSourcesByGroup = function (group, callback) {
    var _a;
    var sources = this.getSourcesByGroup(group);
    return (_a = sources === null || sources === void 0 ? void 0 : sources.map) === null ||
      _a === void 0
      ? void 0
      : _a.call(sources, callback);
  };
  DragSource.prototype.eachSourcesByGroup = function (group, callback) {
    var _a;
    var sources = this.getSourcesByGroup(group);
    return (_a = sources === null || sources === void 0 ? void 0 : sources.forEach) === null ||
      _a === void 0
      ? void 0
      : _a.call(sources, callback);
  };
  DragSource.prototype.reduceSourcesByGroup = function (group, callback, init) {
    var _a;
    var sources = this.getSourcesByGroup(group);
    return (_a = sources === null || sources === void 0 ? void 0 : sources.reduce) === null ||
      _a === void 0
      ? void 0
      : _a.call(sources, callback, init);
  };
  return DragSource;
})();
export { DragSource };
export var GlobalDragSource = new DragSource();
