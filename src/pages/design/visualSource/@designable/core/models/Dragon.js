var __read =
  (this && this.__read) ||
  function (o, n) {
    var m = typeof Symbol === 'function' && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o),
      r,
      ar = [],
      e;
    try {
      while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    } catch (error) {
      e = { error: error };
    } finally {
      try {
        if (r && !r.done && (m = i['return'])) m.call(i);
      } finally {
        if (e) throw e.error;
      }
    }
    return ar;
  };
var __spread =
  (this && this.__spread) ||
  function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
  };
import { observable, define, action } from '@/pages/design/visualSource/@formily/reactive';
import {
  calcDistanceOfPointToRect,
  calcDistancePointToEdge,
  isFn,
  isNearAfter,
  isPointInRect,
} from '@/pages/design/visualSource/@designable/shared';
import { DragNodeEvent, DropNodeEvent } from '../events';
export var ClosestDirection;
(function (ClosestDirection) {
  ClosestDirection['Before'] = 'BEFORE';
  ClosestDirection['ForbidBefore'] = 'FORBID_BEFORE';
  ClosestDirection['After'] = 'After';
  ClosestDirection['ForbidAfter'] = 'FORBID_AFTER';
  ClosestDirection['Upper'] = 'UPPER';
  ClosestDirection['ForbidUpper'] = 'FORBID_UPPER';
  ClosestDirection['Under'] = 'UNDER';
  ClosestDirection['ForbidUnder'] = 'FORBID_UNDER';
  ClosestDirection['Inner'] = 'INNER';
  ClosestDirection['ForbidInner'] = 'FORBID_INNER';
  ClosestDirection['InnerAfter'] = 'INNER_AFTER';
  ClosestDirection['ForbidInnerAfter'] = 'FORBID_INNER_AFTER';
  ClosestDirection['InnerBefore'] = 'INNER_BEFORE';
  ClosestDirection['ForbidInnerBefore'] = 'FORBID_INNER_BEFORE';
  ClosestDirection['Forbid'] = 'FORBID';
})(ClosestDirection || (ClosestDirection = {}));
var Dragon = /** @class */ (function () {
  function Dragon(props) {
    this.dragNodes = [];
    this.touchNode = null;
    this.dropNode = null;
    this.closestNode = null;
    this.closestRect = null;
    this.closestOffsetRect = null;
    this.closestDirection = null;
    this.sensitive = true;
    this.forceBlock = false;
    this.viewport = null;
    this.operation = props.operation;
    this.viewport = props.viewport;
    this.sensitive = props.sensitive;
    this.forceBlock = props.forceBlock;
    this.rootNode = this.operation.tree;
    this.makeObservable();
  }
  /**
   * 相对最近节点的位置
   * @readonly
   * @type {ClosestDirection}
   * @memberof Dragon
   */
  Dragon.prototype.getClosestDirection = function (point) {
    var _a, _b, _c;
    var closestNode = this.closestNode;
    if (!closestNode) return ClosestDirection.Forbid;
    var closestNodeParent = closestNode.parent;
    var closestRect = this.viewport.getValidNodeRect(closestNode);
    var isInline =
      ((_b = (_a = this.closestNode) === null || _a === void 0 ? void 0 : _a.designerProps) ===
        null || _b === void 0
        ? void 0
        : _b.inlineLayout) ||
      ((_c =
        closestNodeParent === null || closestNodeParent === void 0
          ? void 0
          : closestNodeParent.designerProps) === null || _c === void 0
        ? void 0
        : _c.inlineChildrenLayout);
    if (!closestRect) {
      return;
    }
    var isAfter = isNearAfter(point, closestRect, this.forceBlock ? false : isInline);
    if (isPointInRect(point, closestRect, this.sensitive)) {
      if (!closestNode.allowAppend(this.dragNodes)) {
        if (!closestNode.allowSibling(this.dragNodes)) {
          if (isInline) {
            return isAfter ? ClosestDirection.ForbidAfter : ClosestDirection.ForbidBefore;
          } else {
            return isAfter ? ClosestDirection.ForbidUnder : ClosestDirection.ForbidUpper;
          }
        } else {
          if (isInline) {
            return isAfter ? ClosestDirection.After : ClosestDirection.Before;
          } else {
            return isAfter ? ClosestDirection.Under : ClosestDirection.Upper;
          }
        }
      }
      if (closestNode.contains.apply(closestNode, __spread(this.dragNodes))) {
        return isAfter ? ClosestDirection.InnerAfter : ClosestDirection.InnerBefore;
      } else {
        return ClosestDirection.Inner;
      }
    } else if (closestNode === closestNode.root) {
      return isAfter ? ClosestDirection.InnerAfter : ClosestDirection.InnerBefore;
    } else {
      if (!closestNode.allowSibling(this.dragNodes)) {
        if (isInline) {
          return isAfter ? ClosestDirection.ForbidAfter : ClosestDirection.ForbidBefore;
        } else {
          return isAfter ? ClosestDirection.ForbidUnder : ClosestDirection.ForbidUpper;
        }
      }
      if (isInline) {
        return isAfter ? ClosestDirection.After : ClosestDirection.Before;
      } else {
        return isAfter ? ClosestDirection.Under : ClosestDirection.Upper;
      }
    }
  };
  Dragon.prototype.setClosestDirection = function (direction) {
    this.closestDirection = direction;
  };
  /**
   * 拖拽过程中最近的节点
   *
   * @readonly
   * @type {TreeNode}
   * @memberof Dragon
   */
  Dragon.prototype.getClosestNode = function (point) {
    var _this = this;
    var _a, _b;
    if (this.touchNode) {
      var touchNodeRect = this.viewport.getValidNodeRect(this.touchNode);
      if (!touchNodeRect) return;
      if (
        (_b = (_a = this.touchNode) === null || _a === void 0 ? void 0 : _a.children) === null ||
        _b === void 0
          ? void 0
          : _b.length
      ) {
        var touchDistance = calcDistancePointToEdge(point, touchNodeRect);
        var minDistance_1 = touchDistance;
        var minDistanceNode_1 = this.touchNode;
        this.touchNode.eachChildren(function (node) {
          var rect = _this.viewport.getElementRectById(node.id);
          if (!rect) return;
          var distance = isPointInRect(point, rect, _this.sensitive)
            ? 0
            : calcDistanceOfPointToRect(point, rect);
          if (distance <= minDistance_1) {
            minDistance_1 = distance;
            minDistanceNode_1 = node;
          }
        });
        return minDistanceNode_1;
      } else {
        return this.touchNode;
      }
    }
    return null;
  };
  Dragon.prototype.setClosestNode = function (node) {
    this.closestNode = node;
  };
  /**
   * 从最近的节点中计算出节点矩形
   *
   * @readonly
   * @type {DOMRect}
   * @memberof Dragon
   */
  Dragon.prototype.getClosestRect = function () {
    var closestNode = this.closestNode;
    var closestDirection = this.closestDirection;
    if (!closestNode || !closestDirection) return;
    var closestRect = this.viewport.getValidNodeRect(closestNode);
    if (
      closestDirection === ClosestDirection.InnerAfter ||
      closestDirection === ClosestDirection.InnerBefore
    ) {
      return this.viewport.getChildrenRect(closestNode);
    } else {
      return closestRect;
    }
  };
  Dragon.prototype.setClosestRect = function (rect) {
    this.closestRect = rect;
  };
  Dragon.prototype.getClosestOffsetRect = function () {
    var closestNode = this.closestNode;
    var closestDirection = this.closestDirection;
    if (!closestNode || !closestDirection) return;
    var closestRect = this.viewport.getValidNodeOffsetRect(closestNode);
    if (
      closestDirection === ClosestDirection.InnerAfter ||
      closestDirection === ClosestDirection.InnerBefore
    ) {
      return this.viewport.getChildrenOffsetRect(closestNode);
    } else {
      return closestRect;
    }
  };
  Dragon.prototype.setClosestOffsetRect = function (rect) {
    this.closestOffsetRect = rect;
  };
  Dragon.prototype.setDragNodes = function (nodes) {
    if (nodes === void 0) {
      nodes = [];
    }
    var dragNodes = nodes.reduce(function (buf, node) {
      var _a;
      if (
        isFn(
          (_a = node === null || node === void 0 ? void 0 : node.designerProps) === null ||
            _a === void 0
            ? void 0
            : _a.getDragNodes,
        )
      ) {
        var transformed = node.designerProps.getDragNodes(node);
        return transformed ? buf.concat(transformed) : buf;
      }
      return buf.concat([node]);
    }, []);
    this.dragNodes = dragNodes;
    this.trigger(
      new DragNodeEvent({
        target: this.operation.tree,
        source: dragNodes,
      }),
    );
  };
  Dragon.prototype.setTouchNode = function (node) {
    this.touchNode = node;
    if (!node) {
      this.closestNode = null;
      this.closestDirection = null;
      this.closestOffsetRect = null;
      this.closestRect = null;
    }
  };
  Dragon.prototype.calculate = function (props) {
    var point = props.point,
      touchNode = props.touchNode,
      closestNode = props.closestNode,
      closestDirection = props.closestDirection;
    this.setTouchNode(touchNode);
    this.closestNode = closestNode || this.getClosestNode(point);
    this.closestDirection = closestDirection || this.getClosestDirection(point);
    this.closestRect = this.getClosestRect();
    this.closestOffsetRect = this.getClosestOffsetRect();
  };
  Dragon.prototype.setDropNode = function (node) {
    this.dropNode = node;
    this.trigger(
      new DropNodeEvent({
        target: this.operation.tree,
        source: node,
      }),
    );
  };
  Dragon.prototype.trigger = function (event) {
    if (this === null || this === void 0 ? void 0 : this.operation) {
      return this.operation.dispatch(event);
    }
  };
  Dragon.prototype.clear = function () {
    this.dragNodes = [];
    this.touchNode = null;
    this.dropNode = null;
    this.closestNode = null;
    this.closestDirection = null;
    this.closestOffsetRect = null;
    this.closestRect = null;
  };
  Dragon.prototype.makeObservable = function () {
    define(this, {
      dragNodes: observable.shallow,
      touchNode: observable.ref,
      closestNode: observable.ref,
      closestDirection: observable.ref,
      closestRect: observable.ref,
      setDragNodes: action,
      setTouchNode: action,
      setDropNode: action,
      setClosestNode: action,
      setClosestDirection: action,
      setClosestOffsetRect: action,
      setClosestRect: action,
      clear: action,
      calculate: action,
    });
  };
  return Dragon;
})();
export { Dragon };
