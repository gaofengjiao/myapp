var __read =
  (this && this.__read) ||
  function (o, n) {
    var m = typeof Symbol === 'function' && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o),
      r,
      ar = [],
      e;
    try {
      while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    } catch (error) {
      e = { error: error };
    } finally {
      try {
        if (r && !r.done && (m = i['return'])) m.call(i);
      } finally {
        if (e) throw e.error;
      }
    }
    return ar;
  };
var __spread =
  (this && this.__spread) ||
  function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
  };
import { TreeNode } from './TreeNode';
import { Selection } from './Selection';
import { Hover } from './Hover';
import { action, define, observable } from '@/pages/design/visualSource/@formily/reactive';
import { Dragon } from './Dragon';
import {
  cancelIdle,
  each,
  isFn,
  requestIdle,
} from '@/pages/design/visualSource/@designable/shared';
var Operation = /** @class */ (function () {
  function Operation(workspace) {
    this.requests = {
      snapshot: null,
    };
    this.engine = workspace.engine;
    this.workspace = workspace;
    this.tree = new TreeNode({
      componentName: 'Root',
      operation: this,
      children: this.engine.props.defaultComponentTree || [],
    });
    this.selection = new Selection({
      operation: this,
    });
    this.hover = new Hover({
      operation: this,
    });
    this.outlineDragon = new Dragon({
      operation: this,
      sensitive: false,
      forceBlock: true,
      viewport: this.workspace.outline,
    });
    this.viewportDragon = new Dragon({
      operation: this,
      viewport: this.workspace.viewport,
    });
    this.selection.select(this.tree);
    this.makeObservable();
  }
  Operation.prototype.dispatch = function (event, callback) {
    if (this.workspace.dispatch(event) === false) return;
    if (isFn(callback)) return callback();
  };
  Operation.prototype.getSelectedNodes = function () {
    var _this = this;
    return this.selection.selected.map(function (id) {
      return _this.tree.findById(id);
    });
  };
  Operation.prototype.switchFocusNode = function (node) {
    if (this.focusNode === node) {
      this.focusNode = null;
    } else {
      this.focusNode = node;
    }
  };
  Operation.prototype.focusClean = function () {
    this.focusNode = null;
  };
  Operation.prototype.setDragNodes = function (nodes) {
    this.outlineDragon.setDragNodes(nodes);
    this.viewportDragon.setDragNodes(nodes);
  };
  Operation.prototype.getDragNodes = function () {
    var _a;
    if ((_a = this.outlineDragon.dragNodes) === null || _a === void 0 ? void 0 : _a.length) {
      return this.outlineDragon.dragNodes;
    }
    return this.viewportDragon.dragNodes;
  };
  Operation.prototype.getClosestNode = function () {
    return this.viewportDragon.closestNode || this.outlineDragon.closestNode;
  };
  Operation.prototype.getClosestDirection = function () {
    return this.viewportDragon.closestDirection || this.outlineDragon.closestDirection;
  };
  Operation.prototype.setTouchNode = function (node) {
    this.outlineDragon.setTouchNode(node);
    this.viewportDragon.setTouchNode(node);
  };
  Operation.prototype.dragWith = function (point, touchNode) {
    var viewport = this.workspace.viewport;
    var outline = this.workspace.outline;
    if (outline.isPointInViewport(point, false)) {
      this.outlineDragon.calculate({
        point: point,
        touchNode: touchNode || this.tree,
      });
      this.viewportDragon.calculate({
        touchNode: touchNode || this.tree,
        closestNode: this.outlineDragon.closestNode,
        closestDirection: this.outlineDragon.closestDirection,
      });
    } else if (viewport.isPointInViewport(point, false)) {
      this.viewportDragon.calculate({
        point: point,
        touchNode: touchNode || this.tree,
      });
      this.outlineDragon.calculate({
        touchNode: touchNode || this.tree,
        closestNode: this.viewportDragon.closestNode,
        closestDirection: this.viewportDragon.closestDirection,
      });
    } else {
      this.setTouchNode(null);
    }
  };
  Operation.prototype.dragClean = function () {
    this.outlineDragon.clear();
    this.viewportDragon.clear();
  };
  Operation.prototype.getTouchNode = function () {
    return this.outlineDragon.touchNode || this.viewportDragon.touchNode;
  };
  Operation.prototype.setDropNode = function (node) {
    this.outlineDragon.setDropNode(node);
    this.viewportDragon.setDropNode(node);
  };
  Operation.prototype.getDropNode = function () {
    return this.outlineDragon.dropNode || this.viewportDragon.dropNode;
  };
  Operation.prototype.removeNodes = function (nodes) {
    var _a;
    for (var i = nodes.length - 1; i >= 0; i--) {
      var node = nodes[i];
      if (
        node !== this.tree &&
        ((_a = node === null || node === void 0 ? void 0 : node.designerProps) === null ||
        _a === void 0
          ? void 0
          : _a.deletable) !== false
      ) {
        var previousIndex = node.index - 1;
        var afterIndex = node.index + 1;
        var parent_1 = node.parent;
        node.remove();
        var previous = previousIndex > -1 && parent_1.children[previousIndex];
        var after = afterIndex < parent_1.children.length && parent_1.children[afterIndex];
        this.selection.select(previous ? previous : after ? after : node.parent);
        this.hover.clear();
      }
    }
  };
  Operation.prototype.sortNodes = function (nodes) {
    return nodes.sort(function (before, after) {
      if (before.depth !== after.depth) return 0;
      return before.index - after.index >= 0 ? 1 : -1;
    });
  };
  Operation.prototype.cloneNodes = function (nodes) {
    var _this = this;
    var groups = {};
    var lastGroupNode = {};
    var filterNestedNode = this.sortNodes(nodes).filter(function (node) {
      return !nodes.some(function (parent) {
        return node.isMyParents(parent);
      });
    });
    each(filterNestedNode, function (node) {
      var _a, _b, _c, _d, _e, _f, _g, _h;
      if (
        ((_a = node === null || node === void 0 ? void 0 : node.designerProps) === null ||
        _a === void 0
          ? void 0
          : _a.cloneable) === false
      )
        return;
      groups[
        (_b = node === null || node === void 0 ? void 0 : node.parent) === null || _b === void 0
          ? void 0
          : _b.id
      ] =
        groups[
          (_c = node === null || node === void 0 ? void 0 : node.parent) === null || _c === void 0
            ? void 0
            : _c.id
        ] || [];
      groups[
        (_d = node === null || node === void 0 ? void 0 : node.parent) === null || _d === void 0
          ? void 0
          : _d.id
      ].push(node);
      if (
        lastGroupNode[
          (_e = node === null || node === void 0 ? void 0 : node.parent) === null || _e === void 0
            ? void 0
            : _e.id
        ]
      ) {
        if (
          node.index >
          lastGroupNode[
            (_f = node === null || node === void 0 ? void 0 : node.parent) === null || _f === void 0
              ? void 0
              : _f.id
          ].index
        ) {
          lastGroupNode[
            (_g = node === null || node === void 0 ? void 0 : node.parent) === null || _g === void 0
              ? void 0
              : _g.id
          ] = node;
        }
      } else {
        lastGroupNode[
          (_h = node === null || node === void 0 ? void 0 : node.parent) === null || _h === void 0
            ? void 0
            : _h.id
        ] = node;
      }
    });
    var parents = new Map();
    each(groups, function (nodes, parentId) {
      var lastNode = lastGroupNode[parentId];
      var insertPoint = lastNode;
      each(nodes, function (node) {
        var cloned = node.clone();
        if (!cloned) return;
        if (_this.selection.has(node) && insertPoint.parent.allowAppend([cloned])) {
          insertPoint.insertAfter(cloned);
          insertPoint = insertPoint.next;
        } else if (_this.selection.length === 1) {
          var targetNode = _this.tree.findById(_this.selection.first);
          var cloneNodes = parents.get(targetNode);
          if (!cloneNodes) {
            cloneNodes = [];
            parents.set(targetNode, cloneNodes);
          }
          if (targetNode && targetNode.allowAppend([cloned])) {
            cloneNodes.push(cloned);
          }
        }
      });
    });
    parents.forEach(function (nodes, target) {
      if (!nodes.length) return;
      target.append.apply(target, __spread(nodes));
    });
  };
  Operation.prototype.makeObservable = function () {
    define(this, {
      focusNode: observable.ref,
      hover: observable.ref,
      removeNodes: action,
      cloneNodes: action,
    });
  };
  Operation.prototype.snapshot = function (type) {
    var _this = this;
    cancelIdle(this.requests.snapshot);
    if (!this.workspace || !this.workspace.history || this.workspace.history.locking) return;
    this.requests.snapshot = requestIdle(function () {
      _this.workspace.history.push(type);
    });
  };
  Operation.prototype.from = function (operation) {
    if (!operation) return;
    if (operation.tree) {
      this.tree.from(operation.tree);
    }
    if (operation.selected) {
      this.selection.batchSelect(operation.selected);
    }
  };
  Operation.prototype.serialize = function () {
    return {
      tree: this.tree.serialize(),
      selected: this.selection.selected,
    };
  };
  return Operation;
})();
export { Operation };
