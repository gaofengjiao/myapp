import { Operation } from './Operation';
import { IDesignerProps, IControlNodeMetaType } from '../types';
export interface ITreeNode {
  componentName?: string;
  operation?: Operation;
  hidden?: boolean;
  isSourceNode?: boolean;
  id?: string;
  props?: Record<string | number | symbol, any>;
  children?: ITreeNode[];
}
export interface INodeFinder {
  (node: TreeNode): boolean;
}
export declare class TreeNode {
  parent: TreeNode;
  root: TreeNode;
  operation: Operation;
  id: string;
  depth: number;
  hidden: boolean;
  componentName: string;
  props: ITreeNode['props'];
  children: TreeNode[];
  isSelfSourceNode: boolean;
  originDesignerProps: IDesignerProps;
  constructor(node?: ITreeNode, parent?: TreeNode);
  makeObservable(): void;
  set designerProps(props: IDesignerProps);
  get designerProps(): IDesignerProps;
  get previous(): TreeNode;
  get next(): TreeNode;
  get siblings(): TreeNode[];
  get index(): number;
  get childrens(): TreeNode[];
  get isRoot(): boolean;
  get isInOperation(): boolean;
  get lastChild(): TreeNode;
  get firstChild(): TreeNode;
  getPrevious(step?: number): TreeNode;
  getAfter(step?: number): TreeNode;
  getSibling(index?: number): TreeNode;
  isMyAncestor(node: TreeNode): boolean;
  isMyParent(node: TreeNode): boolean;
  isMyParents(node: TreeNode): boolean;
  isMyChild(node: TreeNode): boolean;
  isMyChildren(node: TreeNode): boolean;
  get isSourceNode(): boolean;
  takeSnapshot(type?: string): void;
  triggerMutation<T>(event: any, callback?: () => T, defaults?: T): T;
  find(finder: INodeFinder): TreeNode;
  findAll(finder: INodeFinder): TreeNode[];
  distanceTo(node: TreeNode): number;
  crossSiblings(node: TreeNode): TreeNode[];
  matchNodeMeta(meta: IControlNodeMetaType): boolean;
  allowSibling(nodes: TreeNode[]): boolean;
  allowAppend(nodes: TreeNode[]): boolean;
  findById(id: string): TreeNode;
  getParents(node?: TreeNode): TreeNode[];
  getParentByDepth(depth?: number): any;
  contains(...nodes: TreeNode[]): boolean;
  eachChildren(callback?: (node: TreeNode) => void | boolean): void;
  resetNodesParent(nodes: TreeNode[], parent: TreeNode): TreeNode[];
  setProps(props?: any): void;
  /**
   * @deprecated
   * please use `setProps`
   */
  setNodeProps(...nodes: TreeNode[]): void;
  setComponentName(name: string): void;
  prepend(...nodes: TreeNode[]): TreeNode[];
  /**
   * @deprecated
   * please use `prepend`
   */
  prependNode(...nodes: TreeNode[]): TreeNode[];
  append(...nodes: TreeNode[]): TreeNode[];
  /**
   * @deprecated
   * please use `append`
   */
  appendNode(...nodes: TreeNode[]): TreeNode[];
  wrap(wrapper: TreeNode): TreeNode;
  /**
   * @deprecated
   * please use `wrap`
   */
  wrapNode(wrapper: TreeNode): TreeNode;
  insertAfter(...nodes: TreeNode[]): TreeNode[];
  insertBefore(...nodes: TreeNode[]): TreeNode[];
  insertChildren(start: number, ...nodes: TreeNode[]): TreeNode[];
  setChildren(...nodes: TreeNode[]): TreeNode[];
  /**
   * @deprecated
   * please use `setChildren`
   */
  setNodeChildren(...nodes: TreeNode[]): TreeNode[];
  remove(): void;
  clone(parent?: TreeNode): TreeNode;
  from(node?: ITreeNode): void;
  serialize(): ITreeNode;
  create(node: ITreeNode, parent?: TreeNode): TreeNode;
}
