import { IDesignerControllerProps, IDesignerControllerPropsMap, IDesignerLocales } from './types';
declare const DESIGNER_GlobalRegistry: {
  setComponentDesignerProps: (componentName: string, props: IDesignerControllerProps) => void;
  getComponentDesignerProps: (componentName: string) => IDesignerControllerProps;
  registerDesignerProps: (map: IDesignerControllerPropsMap) => void;
  registerDesignerIcons: (map: Record<string, any>) => void;
  getDesignerIcon: (name: string) => any;
  setDesignerLanguage(lang: string): void;
  getDesignerLanguage(): string;
  getDesignerMessage(token: string): any;
  registerDesignerLocales(...packages: IDesignerLocales['messages'][]): void;
};
export declare type IDesignerRegistry = typeof DESIGNER_GlobalRegistry;
export declare const GlobalRegistry: IDesignerRegistry;
export {};
