var __assign =
  (this && this.__assign) ||
  function () {
    __assign =
      Object.assign ||
      function (t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
      };
    return __assign.apply(this, arguments);
  };
import { each, isFn, isPlainObj } from '@/pages/design/visualSource/@designable/shared';
import { Path } from '@/pages/design/visualSource/@formily/path';
import { define, observable } from '@/pages/design/visualSource/@formily/reactive';
var getBrowserlanguage = function () {
  var _a;
  /* istanbul ignore next */
  if (!window.navigator) {
    return 'en';
  }
  return (
    window.navigator['browserlanguage'] ||
    ((_a = window.navigator) === null || _a === void 0 ? void 0 : _a.language) ||
    'en'
  );
};
var getISOCode = function (language) {
  var isoCode = DESIGNER_LOCALES.language;
  var lang = cleanSpace(language);
  if (DESIGNER_LOCALES.messages[lang]) {
    return lang;
  }
  each(DESIGNER_LOCALES.messages, function (_, key) {
    if (key.indexOf(lang) > -1 || String(lang).indexOf(key) > -1) {
      isoCode = key;
      return false;
    }
  });
  return isoCode;
};
var DESIGNER_PROPS_MAP = {
  Root: {
    droppable: true,
    cloneable: false,
    deletable: false,
  },
};
var DESIGNER_ICONS_MAP = {};
var DESIGNER_LOCALES = define(
  {
    messages: {},
    language: getBrowserlanguage(),
  },
  {
    language: observable.ref,
  },
);
var cleanSpace = function (str) {
  return String(str).replace(/\s+/g, '_').toLocaleLowerCase();
};
var mergeLocales = function (target, source) {
  if (isPlainObj(target) && isPlainObj(source)) {
    each(source, function (value, key) {
      var token = cleanSpace(key);
      var messages = mergeLocales(target[key] || target[token], value);
      target[token] = messages;
      target[key] = messages;
    });
    return target;
  } else if (isPlainObj(source)) {
    var result_1 = Array.isArray(source) ? [] : {};
    each(source, function (value, key) {
      var messages = mergeLocales(undefined, value);
      result_1[cleanSpace(key)] = messages;
      result_1[key] = messages;
    });
    return result_1;
  }
  return source;
};
var DESIGNER_GlobalRegistry = {
  setComponentDesignerProps: function (componentName, props) {
    var originProps = GlobalRegistry.getComponentDesignerProps(componentName);
    DESIGNER_PROPS_MAP[componentName] = function (node) {
      if (isFn(originProps)) {
        if (isFn(props)) {
          return __assign(__assign({}, originProps(node)), props(node));
        } else {
          return __assign(__assign({}, originProps(node)), props);
        }
      } else if (isFn(props)) {
        return __assign(__assign({}, originProps), props(node));
      } else {
        return __assign(__assign({}, originProps), props);
      }
    };
  },
  getComponentDesignerProps: function (componentName) {
    return DESIGNER_PROPS_MAP[componentName] || {};
  },
  registerDesignerProps: function (map) {
    each(map, function (props, componentName) {
      GlobalRegistry.setComponentDesignerProps(componentName, props);
    });
  },
  registerDesignerIcons: function (map) {
    Object.assign(DESIGNER_ICONS_MAP, map);
  },
  getDesignerIcon: function (name) {
    return DESIGNER_ICONS_MAP[name];
  },
  setDesignerLanguage: function (lang) {
    DESIGNER_LOCALES.language = lang;
  },
  getDesignerLanguage: function () {
    return getISOCode(DESIGNER_LOCALES.language);
  },
  getDesignerMessage: function (token) {
    var lang = getISOCode(DESIGNER_LOCALES.language);
    var locale = DESIGNER_LOCALES.messages[lang];
    if (!locale) {
      for (var key in DESIGNER_LOCALES.messages) {
        var message = Path.getIn(DESIGNER_LOCALES.messages[key], cleanSpace(token));
        if (message) return message;
      }
      return;
    }
    return Path.getIn(locale, cleanSpace(token));
  },
  registerDesignerLocales: function () {
    var packages = [];
    for (var _i = 0; _i < arguments.length; _i++) {
      packages[_i] = arguments[_i];
    }
    packages.forEach(function (locales) {
      mergeLocales(DESIGNER_LOCALES.messages, locales);
    });
  },
};
export var GlobalRegistry = window['__DESIGNER_GlobalRegistry__'] || DESIGNER_GlobalRegistry;
