import { KeyCode, Shortcut } from '../models';
export var SelectPrevNode = new Shortcut({
  codes: [
    [KeyCode.Up],
    [KeyCode.PageUp],
    [KeyCode.ArrowUp],
    [KeyCode.Left],
    [KeyCode.LeftWindowKey],
    [KeyCode.ArrowLeft],
  ],
  handler: function (context) {
    var operation = context === null || context === void 0 ? void 0 : context.workspace.operation;
    if (operation) {
      var tree = operation.tree;
      var selection = operation.selection;
      var lastNode = tree.findById(selection.last);
      if (lastNode) {
        var previousNode = lastNode.previous;
        if (previousNode) {
          selection.select(previousNode);
        } else if (lastNode.parent) {
          selection.select(lastNode.parent);
        } else {
          selection.select(lastNode.lastChild);
        }
      }
    }
  },
});
export var SelectNextNode = new Shortcut({
  codes: [
    [KeyCode.Down],
    [KeyCode.PageDown],
    [KeyCode.ArrowDown],
    [KeyCode.Right],
    [KeyCode.RightWindowKey],
    [KeyCode.ArrowRight],
  ],
  handler: function (context) {
    var operation = context === null || context === void 0 ? void 0 : context.workspace.operation;
    if (operation) {
      var tree = operation.tree;
      var selection = operation.selection;
      var lastNode = tree.findById(selection.last);
      if (lastNode) {
        var nextNode = lastNode.next;
        if (nextNode) {
          selection.select(nextNode);
        } else if (lastNode.parent) {
          selection.select(lastNode.parent);
        } else {
          selection.select(lastNode.firstChild);
        }
      }
    }
  },
});
