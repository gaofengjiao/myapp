import { IEventProps, Event } from '@/pages/design/visualSource/@designable/shared';
import { ISchema } from '@/pages/design/visualSource/@formily/json-schema';
import {
  Engine,
  ITreeNode,
  ScreenType,
  Shortcut,
  Viewport,
  Workbench,
  Workspace,
  TreeNode,
} from './models';
export declare type IEngineProps<T = Event> = IEventProps<T> & {
  sourceIdAttrName?: string;
  nodeIdAttrName?: string;
  outlineNodeIdAttrName?: string;
  nodeHelpersIdAttrName?: string;
  defaultComponentTree?: ITreeNode[];
  defaultScreenType?: ScreenType;
  shortcuts?: Shortcut[];
};
export declare type IEngineContext = {
  workspace: Workspace;
  workbench: Workbench;
  engine: Engine;
  viewport: Viewport;
};
export declare type IControlType = boolean | ((node: TreeNode) => boolean);
export declare type IControlNodeMetaType = {
  componentName: string;
  id?: string;
  maxInstances?: number;
  minInstances?: number;
};
export declare type IControlNodeType =
  | string
  | IControlNodeMetaType
  | ((node: TreeNode) => IControlNodeMetaType);
export interface IDesignerProps {
  package?: string;
  registry?: string;
  version?: string;
  path?: string;
  title?: string;
  description?: string;
  icon?: string;
  group?: string;
  droppable?: boolean;
  draggable?: boolean;
  deletable?: boolean;
  cloneable?: boolean;
  resizable?: boolean;
  inlineLayout?: boolean;
  inlineChildrenLayout?: boolean;
  selfRenderChildren?: boolean;
  propsSchema?: ISchema;
  defaultProps?: any;
  effects?: (engine: Engine) => void;
  getDragNodes?: (node: TreeNode) => TreeNode | TreeNode[];
  getComponentProps?: (node: TreeNode) => any;
  allowAppend?: (target: TreeNode, sources?: TreeNode[]) => boolean;
  allowSiblings?: (target: TreeNode, sources?: TreeNode[]) => boolean;
  [key: string]: any;
}
export declare type IDesignerPropsMap = Record<string, IDesignerProps>;
export declare type IDesignerControllerProps =
  | IDesignerProps
  | ((node: TreeNode) => IDesignerProps);
export declare type IDesignerControllerPropsMap = Record<string, IDesignerControllerProps>;
export interface IDesignerLocales {
  messages: {
    [ISOCode: string]: {
      [key: string]: any;
    };
  };
  language: string;
}
export declare type WorkbenchTypes =
  | 'DESIGNABLE'
  | 'PREVIEW'
  | 'JSONTREE'
  | 'MARKUP'
  | (string & {});
