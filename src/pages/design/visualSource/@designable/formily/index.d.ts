import type { ISchema } from '@/pages/design/visualSource/@formily/json-schema';
import type { ITreeNode, TreeNode } from '@/pages/design/visualSource/@designable/core';

export interface ITransformerOptions {
  designableFieldName?: string;
  designableFormName?: string;
}
export interface IFormilySchema {
  schema?: ISchema;
  form?: Record<string, any>;
}
export declare const transformToSchema: (
  node: TreeNode,
  options?: ITransformerOptions,
) => IFormilySchema;
export declare const transformToTreeNode: (
  formily?: IFormilySchema,
  options?: ITransformerOptions,
) => ITreeNode;
