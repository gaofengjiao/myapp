var __assign =
  (this && this.__assign) ||
  function () {
    __assign =
      Object.assign ||
      function (t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
      };
    return __assign.apply(this, arguments);
  };
import { Schema } from '@/pages/design/visualSource/@formily/json-schema';
import { clone, uid } from '@/pages/design/visualSource/@designable/shared';

var createOptions = function (options) {
  return __assign(
    { designableFieldName: 'DesignableField', designableFormName: 'DesignableForm' },
    options,
  );
};
export var transformToSchema = function (node, options) {
  var realOptions = createOptions(options);
  var root = node.find(function (child) {
    // console.log(child,'child')
    // console.log(realOptions,'realOptions')
    return child.componentName === realOptions.designableFormName;
  });
  var schema = {
    type: 'object',
    properties: {},
  };
  if (!root) return { schema: schema };
  var createSchema = function (node, schema) {
    if (schema === void 0) {
      schema = {};
    }
    if (node !== root) {
      Object.assign(schema, clone(node.props));
    }
    schema['_designableId'] = node.id;
    if (schema.type === 'array') {
      if (node.children[0]) {
        if (node.children[0].componentName === realOptions.designableFieldName) {
          schema.items = createSchema(node.children[0]);
          schema['x-index'] = 0;
        }
      }
      node.children.slice(1).forEach(function (child, index) {
        if (child.componentName !== realOptions.designableFieldName) return;
        var key = child.props.name || child.id;
        schema.properties = schema.properties || {};
        schema.properties[key] = createSchema(child);
        schema.properties[key]['x-index'] = index;
      });
    } else {
      node.children.forEach(function (child, index) {
        if (child.componentName !== realOptions.designableFieldName) return;
        var key = child.props.name || child.id;
        schema.properties = schema.properties || {};
        schema.properties[key] = createSchema(child);
        schema.properties[key]['x-index'] = index;
      });
    }
    return schema;
  };
  return { form: clone(root.props), schema: createSchema(root, schema) };
};
export var transformToTreeNode = function (formily, options) {
  if (formily === void 0) {
    formily = {};
  }
  var realOptions = createOptions(options);
  var root = {
    componentName: realOptions.designableFormName,
    props: formily.form,
    children: [],
  };
  var schema = new Schema(formily.schema);
  var cleanProps = function (props) {
    if (props['name'] === props['_designableId']) {
      delete props.name;
    }
    delete props['version'];
    delete props['_isJSONSchemaObject'];
    return props;
  };
  var appendTreeNode = function (parent, schema) {
    if (!schema) return;
    var current = {
      id: schema['_designableId'] || uid(),
      componentName: realOptions.designableFieldName,
      props: cleanProps(schema.toJSON(false)),
      children: [],
    };
    parent.children.push(current);
    if (schema.items && !Array.isArray(schema.items)) {
      appendTreeNode(current, schema.items);
    }
    schema.mapProperties(function (schema) {
      schema['_designableId'] = schema['_designableId'] || uid();
      appendTreeNode(current, schema);
    });
  };
  schema.mapProperties(function (schema) {
    schema['_designableId'] = schema['_designableId'] || uid();
    appendTreeNode(root, schema);
  });
  return root;
};
