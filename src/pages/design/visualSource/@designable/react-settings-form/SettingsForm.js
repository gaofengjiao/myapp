import React, { useMemo } from 'react';
import { createForm } from '@/pages/design/visualSource/@formily/core';
import { Form } from '@/pages/design/visualSource/@formily/antd';
import { observer } from '@/pages/design/visualSource/@formily/react';
import { requestIdle, cancelIdle } from '@/pages/design/visualSource/@designable/shared';
import {
  usePrefix,
  useSelected,
  useOperation,
  useCurrentNode,
  IconWidget,
} from '@/pages/design/visualSource/@designable/react';
import { SchemaField } from './SchemaField';
import { SettingsFormContext } from './shared/context';
import { useLocales, useSnapshot } from './effects';
import { NodePath } from './components/NodePath';
import { Empty } from 'antd';
import cls from 'classnames';
import './styles.less';

var GlobalState = {
  idleRequest: null,
};
export var SettingsForm = observer(
  function (props) {
    var _a, _b;
    var operation = useOperation();
    var node = useCurrentNode();
    var selected = useSelected();
    // console.log(operation,'operation')
    // console.log(node,'node SettingsForm')
    // console.log(selected,'selected SettingsForm')
    var prefix = usePrefix('settings-form');
    // if(props.onChange) {
    //     props.onChange(node)
    // }
    var schema =
      (_a = node === null || node === void 0 ? void 0 : node.designerProps) === null ||
      _a === void 0
        ? void 0
        : _a.propsSchema;
    var form = useMemo(
      function () {
        return createForm({
          values: node === null || node === void 0 ? void 0 : node.props,
          effects: function () {
            useLocales(schema === null || schema === void 0 ? void 0 : schema['$namespace']);
            useSnapshot(operation);
          },
        });
      },
      [node, schema, operation],
    );

    // console.log(schema,'schema')
    // console.log(props,'props')
    var isEmpty = !(
      node &&
      ((_b = node.designerProps) === null || _b === void 0 ? void 0 : _b.propsSchema) &&
      selected.length === 1
    );
    var render = function () {
      if (!isEmpty) {
        return React.createElement(
          'div',
          { className: cls(prefix, props.className), style: props.style, key: node.id },
          React.createElement(
            SettingsFormContext.Provider,
            { value: props },
            React.createElement(
              Form,
              {
                form: form,
                colon: false,
                labelWidth: 120,
                labelAlign: 'left',
                wrapperAlign: 'right',
                feedbackLayout: 'none',
                tooltipLayout: 'text',
              },
              React.createElement(SchemaField, {
                schema: schema,
                components: props.components,
                scope: props.scope,
              }),
            ),
          ),
        );
      }
      return React.createElement(
        'div',
        { className: prefix + '-empty' },
        React.createElement(Empty, null),
      );
    };
    return React.createElement(
      IconWidget.Provider,
      { tooltip: true },
      React.createElement(
        'div',
        { className: prefix + '-wrapper' },
        !isEmpty && React.createElement(NodePath, null),
        React.createElement('div', { className: prefix + '-content' }, render()),
      ),
    );
  },
  {
    scheduler: function (update) {
      cancelIdle(GlobalState.idleRequest);
      GlobalState.idleRequest = requestIdle(update);
    },
  },
);
