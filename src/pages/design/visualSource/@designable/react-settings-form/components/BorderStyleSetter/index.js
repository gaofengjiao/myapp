import React, { Fragment, useMemo } from 'react';
import { usePrefix } from '@/pages/design/visualSource/@designable/react';
import { camelCase } from '@/pages/design/visualSource/@formily/shared';
import { Select } from '@/pages/design/visualSource/@formily/antd';
import { observable } from '@/pages/design/visualSource/@formily/reactive';
import { useField, Field, observer } from '@/pages/design/visualSource/@formily/react';
import { FoldItem } from '../FoldItem';
import { ColorInput } from '../ColorInput';
import { SizeInput } from '../SizeInput';
import { PositionInput } from '../PositionInput';
import cls from 'classnames';
import './styles.less';
var Positions = ['center', 'top', 'right', 'bottom', 'left'];
var BorderStyleOptions = [
  {
    label: 'None',
    value: 'none',
  },
  {
    label: React.createElement('span', { className: 'border-style-solid-line' }),
    value: 'solid',
  },
  {
    label: React.createElement('span', { className: 'border-style-dashed-line' }),
    value: 'dashed',
  },
  {
    label: React.createElement('span', { className: 'border-style-dotted-line' }),
    value: 'dotted',
  },
];
export var BorderStyleSetter = observer(function (_a) {
  var className = _a.className,
    style = _a.style;
  var currentPosition = useMemo(function () {
    return observable({
      value: 'center',
    });
  }, []);
  var field = useField();
  var prefix = usePrefix('border-style-setter');
  var createReaction = function (position) {
    return function (field) {
      field.display = currentPosition.value === position ? 'visible' : 'hidden';
      if (position !== 'center') {
        var borderStyle = field.query('.borderStyle').value();
        var borderWidth = field.query('.borderWidth').value();
        var borderColor = field.query('.borderColor').value();
        if (borderStyle || borderWidth || borderColor) {
          field.value = undefined;
        }
      }
    };
  };
  return React.createElement(
    FoldItem,
    { label: field.title },
    React.createElement(
      FoldItem.Extra,
      null,
      React.createElement(
        'div',
        { className: cls(prefix, className), style: style },
        React.createElement(
          'div',
          { className: prefix + '-position' },
          React.createElement(PositionInput, {
            value: currentPosition.value,
            onChange: function (value) {
              currentPosition.value = value;
            },
          }),
        ),
        React.createElement(
          'div',
          { className: prefix + '-input' },
          Positions.map(function (position, key) {
            return React.createElement(
              Fragment,
              { key: key },
              React.createElement(Field, {
                name: camelCase('border' + (position === 'center' ? '' : '-' + position) + 'Style'),
                basePath: field.address.parent(),
                dataSource: BorderStyleOptions,
                reactions: createReaction(position),
                component: [Select, { placeholder: 'Please Select' }],
              }),
              React.createElement(Field, {
                name: camelCase('border' + (position === 'center' ? '' : '-' + position) + 'Width'),
                basePath: field.address.parent(),
                reactions: createReaction(position),
                component: [SizeInput, { exclude: ['auto'] }],
              }),
              React.createElement(Field, {
                name: camelCase('border' + (position === 'center' ? '' : '-' + position) + 'Color'),
                basePath: field.address.parent(),
                reactions: createReaction(position),
                component: [ColorInput],
              }),
            );
          }),
        ),
      ),
    ),
  );
});
