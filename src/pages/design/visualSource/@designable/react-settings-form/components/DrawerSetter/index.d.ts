import React from 'react';
import './styles.less';
export interface IDrawerSetterProps {
  text: React.ReactNode;
}
export declare const DrawerSetter: React.FC<IDrawerSetterProps>;
