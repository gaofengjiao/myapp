import React from 'react';
import { IFormItemProps } from '@/pages/design/visualSource/@formily/antd';
import './styles.less';
export declare const FoldItem: React.FC<IFormItemProps> & {
  Base?: React.FC;
  Extra?: React.FC;
};
