var __assign =
  (this && this.__assign) ||
  function () {
    __assign =
      Object.assign ||
      function (t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
      };
    return __assign.apply(this, arguments);
  };
var __rest =
  (this && this.__rest) ||
  function (s, e) {
    var t = {};
    for (var p in s)
      if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === 'function')
      for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
        if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
          t[p[i]] = s[p[i]];
      }
    return t;
  };
import React, { Fragment, useRef, useMemo } from 'react';
import { FormItem } from '@/pages/design/visualSource/@formily/antd';
import { useField, observer } from '@/pages/design/visualSource/@formily/react';
import { observable } from '@/pages/design/visualSource/@formily/reactive';
import { IconWidget, usePrefix } from '@/pages/design/visualSource/@designable/react';
import cls from 'classnames';
import './styles.less';
var ExpandedMap = new Map();
export var FoldItem = observer(function (_a) {
  var className = _a.className,
    style = _a.style,
    children = _a.children,
    props = __rest(_a, ['className', 'style', 'children']);
  var prefix = usePrefix('fold-item');
  var field = useField();
  var expand = useMemo(function () {
    return observable.ref(ExpandedMap.get(field.address.toString()));
  }, []);
  var slots = useRef({ base: null, extra: null });
  React.Children.forEach(children, function (node) {
    var _a, _b;
    if (React.isValidElement(node)) {
      if (
        ((_a = node === null || node === void 0 ? void 0 : node['type']) === null || _a === void 0
          ? void 0
          : _a['displayName']) === 'FoldItem.Base'
      ) {
        slots.current.base = node['props'].children;
      }
      if (
        ((_b = node === null || node === void 0 ? void 0 : node['type']) === null || _b === void 0
          ? void 0
          : _b['displayName']) === 'FoldItem.Extra'
      ) {
        slots.current.extra = node['props'].children;
      }
    }
  });
  return React.createElement(
    'div',
    { className: cls(prefix, className) },
    React.createElement(
      'div',
      {
        className: prefix + '-base',
        onClick: function () {
          expand.value = !expand.value;
          ExpandedMap.set(field.address.toString(), expand.value);
        },
      },
      React.createElement(
        FormItem.BaseItem,
        __assign({}, props, {
          label: React.createElement(
            'span',
            {
              className: cls(prefix + '-title', {
                expand: expand.value,
              }),
            },
            slots.current.extra && React.createElement(IconWidget, { infer: 'Expand', size: 10 }),
            props.label,
          ),
        }),
        React.createElement(
          'div',
          {
            style: { width: '100%' },
            onClick: function (e) {
              e.stopPropagation();
            },
          },
          slots.current.base,
        ),
      ),
    ),
    expand.value &&
      slots.current.extra &&
      React.createElement('div', { className: prefix + '-extra' }, slots.current.extra),
  );
});
var Base = function () {
  return React.createElement(Fragment, null);
};
Base.displayName = 'FoldItem.Base';
var Extra = function () {
  return React.createElement(Fragment, null);
};
Extra.displayName = 'FoldItem.Extra';
FoldItem.Base = Base;
FoldItem.Extra = Extra;
