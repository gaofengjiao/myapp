import React from 'react';
import { Breadcrumb } from 'antd';
import {
  useCurrentNode,
  TextWidget,
  IconWidget,
  useSelection,
  usePrefix,
  useHover,
} from '@/pages/design/visualSource/@designable/react';
import { observer } from '@/pages/design/visualSource/@formily/react';
import './styles.less';
export var NodePath = observer(function () {
  var selected = useCurrentNode();
  var selection = useSelection();
  var hover = useHover();
  var prefix = usePrefix('node-path');
  if (!selected) return React.createElement(React.Fragment, null);
  var nodes = selected.getParents().slice(0, 2).reverse().concat(selected);
  return React.createElement(
    Breadcrumb,
    { className: prefix },
    nodes.map(function (node, key) {
      return React.createElement(
        Breadcrumb.Item,
        { key: key },
        key === 0 &&
          React.createElement(IconWidget, { infer: 'Position', style: { marginRight: 3 } }),
        React.createElement(
          'a',
          {
            href: '',
            onMouseEnter: function () {
              hover.setHover(node);
            },
            onClick: function (e) {
              e.stopPropagation();
              e.preventDefault();
              selection.select(node);
            },
          },
          React.createElement(TextWidget, null, node.designerProps.title),
        ),
      );
    }),
  );
});
