var __assign =
  (this && this.__assign) ||
  function () {
    __assign =
      Object.assign ||
      function (t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
      };
    return __assign.apply(this, arguments);
  };
import React from 'react';
import { isVoidField, onFieldReact } from '@/pages/design/visualSource/@formily/core';
import { GlobalRegistry } from '@/pages/design/visualSource/@designable/core';
import { isPlainObj, isStr } from '@/pages/design/visualSource/@designable/shared';
import { IconWidget } from '@/pages/design/visualSource/@designable/react';
var takeLocales = function (message) {
  if (isStr(message))
    return {
      title: message,
    };
  if (isPlainObj(message)) return message;
  return {};
};
var takeIcon = function (message) {
  if (!isStr(message)) return;
  var matched = message.match(/@([^:\s]+)(?:\s*\:\s*([\s\S]+))?/);
  if (matched) return [matched[1], matched[2]];
  return;
};
export var useLocales = function (namespace) {
  onFieldReact('*', function (field) {
    var _a, _b;
    var path = field.path.toString().replace(/\.[\d+]/g, '');
    var token = field.path.segments[field.path.segments.length - 1];
    var commonMessage = GlobalRegistry.getDesignerMessage('settings.' + path);
    var namespaceMessage = GlobalRegistry.getDesignerMessage('settings.' + namespace + '.' + token);
    var locales = namespaceMessage ? takeLocales(namespaceMessage) : takeLocales(commonMessage);
    if (locales.title) {
      field.title = locales.title;
    }
    if (locales.description) {
      field.description = locales.description;
    }
    if (locales.tooltip) {
      field.decorator[1] = field.decorator[1] || [];
      field.decorator[1].tooltip = locales.tooltip;
    }
    if (locales.placeholder) {
      field.component[1] = field.component[1] || [];
      field.component[1].placeholder = locales.placeholder;
    }
    if (!isVoidField(field)) {
      if ((_a = locales.dataSource) === null || _a === void 0 ? void 0 : _a.length) {
        if ((_b = field.dataSource) === null || _b === void 0 ? void 0 : _b.length) {
          field.dataSource = field.dataSource.map(function (item, index) {
            var _a, _b;
            var label = locales.dataSource[index] || locales.dataSource[item.value] || item.label;
            var icon = takeIcon(label);
            return __assign(__assign({}, item), {
              value:
                (_a = item === null || item === void 0 ? void 0 : item.value) !== null &&
                _a !== void 0
                  ? _a
                  : null,
              label: icon
                ? React.createElement(IconWidget, { infer: icon[0], tooltip: icon[1] })
                : (_b = label === null || label === void 0 ? void 0 : label.label) !== null &&
                  _b !== void 0
                ? _b
                : label,
            });
          });
        } else {
          field.dataSource = locales.dataSource.slice();
        }
      }
    }
  });
};
