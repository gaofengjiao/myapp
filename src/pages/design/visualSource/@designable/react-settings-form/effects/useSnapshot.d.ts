import { Operation } from '@/pages/design/visualSource/@designable/core';
export declare const useSnapshot: (operation: Operation) => void;
