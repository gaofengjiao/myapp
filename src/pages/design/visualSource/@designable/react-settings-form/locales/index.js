import { GlobalRegistry } from '@/pages/design/visualSource/@designable/core';
import zhCN from './zh-CN';
import enUS from './en-US';
GlobalRegistry.registerDesignerLocales(zhCN, enUS);
