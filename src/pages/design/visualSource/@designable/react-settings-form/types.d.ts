import React from 'react';
export interface ISettingFormProps {
  onChange?: (node: any) => void;
  className?: string;
  style?: React.CSSProperties;
  uploadAction?: string;
  components?: Record<string, React.FC<any>>;
  scope?: any;
}
