import React, { useEffect, useRef } from 'react';
import cls from 'classnames';
import { DesignerContext } from '../context';
import { GhostWidget } from '../widgets';
import { useDesigner } from '../hooks';
export var Designer = function (props) {
  var _a;
  var engine = useDesigner();
  var ref = useRef();
  useEffect(function () {
    if (props.engine) {
      if (props.engine && ref.current) {
        if (props.engine !== ref.current) {
          ref.current.unmount();
        }
      }
      props.engine.mount();
      ref.current = props.engine;
    }
    return function () {
      if (props.engine) {
        props.engine.unmount();
      }
    };
  }, []);
  if (engine) throw new Error('There can only be one Designable Engine Context in the React Tree');
  return React.createElement(
    'div',
    {
      className: cls(
        ((_a = {}),
        (_a[props.prefixCls + 'app'] = true),
        (_a['' + props.prefixCls + props.theme] = props.theme),
        _a),
      ),
    },
    React.createElement(
      DesignerContext.Provider,
      {
        value: {
          engine: props.engine,
          prefixCls: props.prefixCls,
          theme: props.theme,
        },
      },
      props.children,
      React.createElement(GhostWidget, null),
    ),
  );
};
Designer.defaultProps = {
  prefixCls: 'dn-',
  theme: 'light',
};
