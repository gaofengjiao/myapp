import React from 'react';
export interface IViewportProps extends Omit<React.HTMLAttributes<HTMLDivElement>, 'placeholder'> {
  placeholder?: React.ReactNode;
}
export declare const Viewport: React.FC<IViewportProps>;
