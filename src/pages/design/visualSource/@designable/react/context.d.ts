/// <reference types="react" />
import { TreeNode } from '@/pages/design/visualSource/@designable/core';
import { IDesignerContext, IWorkspaceContext } from './types';
export declare const DesignerContext: import('react').Context<IDesignerContext>;
export declare const TreeNodeContext: import('react').Context<TreeNode>;
export declare const WorkspaceContext: import('react').Context<IWorkspaceContext>;
