import { createContext } from 'react';
export var DesignerContext = createContext(null);
export var TreeNodeContext = createContext(null);
export var WorkspaceContext = createContext(null);
