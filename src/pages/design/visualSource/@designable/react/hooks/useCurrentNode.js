import { useSelected } from './useSelected';
import { useTree } from './useTree';
export var useCurrentNode = function () {
  var _a;
  var selected = useSelected();
  var tree = useTree();
  return (_a = tree === null || tree === void 0 ? void 0 : tree.findById) === null || _a === void 0
    ? void 0
    : _a.call(tree, selected[0]);
};
