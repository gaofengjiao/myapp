import { Engine } from '@/pages/design/visualSource/@designable/core';
export interface IEffects {
  (engine: Engine): void;
}
export declare const useDesigner: (effects?: IEffects) => Engine;
