import { useContext, useEffect } from 'react';
import { DesignerContext } from '../context';
import { isFn } from '@/pages/design/visualSource/@designable/shared';
export var useDesigner = function (effects) {
  var _a;
  var designer =
    window['__DESINGER_ENGINE__'] ||
    ((_a = useContext(DesignerContext)) === null || _a === void 0 ? void 0 : _a.engine);
  useEffect(function () {
    if (isFn(effects)) {
      return effects(designer);
    }
  }, []);
  return designer;
};
