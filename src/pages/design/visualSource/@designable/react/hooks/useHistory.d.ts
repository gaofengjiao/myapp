export declare const useHistory: (
  workspaceId?: string,
) => import('@/pages/design/visualSource/@designable/core').History<
  import('@/pages/design/visualSource/@designable/core').Workspace
>;
