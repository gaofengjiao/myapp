export declare const useHover: (
  workspaceId?: string,
) => import('@/visualSource/@designable/core/lib/models/Hover').Hover;
