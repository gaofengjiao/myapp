import { useDesigner } from './useDesigner';
import { useTreeNode } from './useTreeNode';
export var useNodeIdProps = function () {
  var _a;
  var node = useTreeNode();
  var designer = useDesigner();
  return (_a = {}), (_a[designer.props.nodeIdAttrName] = node.id), _a;
};
