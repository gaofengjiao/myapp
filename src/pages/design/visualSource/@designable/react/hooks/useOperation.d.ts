export declare const useOperation: (
  workspaceId?: string,
) => import('@/pages/design/visualSource/@designable/core').Operation;
