export declare const useOutline: (
  workspaceId?: string,
) => import('@/pages/design/visualSource/@designable/core').Viewport;
