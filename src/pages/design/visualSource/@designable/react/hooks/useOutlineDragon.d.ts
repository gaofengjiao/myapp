export declare const useOutlineDragon: (
  workspaceId?: string,
) => import('@/pages/design/visualSource/@designable/core').Dragon;
