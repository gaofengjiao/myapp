import { useContext } from 'react';
import { DesignerContext } from '../context';
export var usePrefix = function (after) {
  var _a;
  if (after === void 0) {
    after = '';
  }
  return (
    ((_a = useContext(DesignerContext)) === null || _a === void 0 ? void 0 : _a.prefixCls) + after
  );
};
