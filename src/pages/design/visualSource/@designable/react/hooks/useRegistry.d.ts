import { IDesignerRegistry } from '@/pages/design/visualSource/@designable/core';
export declare const useRegistry: () => IDesignerRegistry;
