import { GlobalRegistry } from '@/pages/design/visualSource/@designable/core';
export var useRegistry = function () {
  return window['__DESIGNER_REGISTRY__'] || GlobalRegistry;
};
