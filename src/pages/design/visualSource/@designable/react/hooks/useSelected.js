import { useSelection } from './useSelection';
export var useSelected = function () {
  var selection = useSelection();
  return (selection === null || selection === void 0 ? void 0 : selection.selected) || [];
};
