export declare const useSelection: (
  workspaceId?: string,
) => import('@/pages/design/visualSource/@designable/core').Selection;
