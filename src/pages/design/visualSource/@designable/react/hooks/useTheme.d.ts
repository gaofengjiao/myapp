import { IDesignerContext } from '../types';
export declare const useTheme: () => IDesignerContext['theme'];
