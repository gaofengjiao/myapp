import { useContext } from 'react';
import { DesignerContext } from '../context';
export var useTheme = function () {
  var _a;
  return (
    window['__DESINGER_THEME__'] ||
    ((_a = useContext(DesignerContext)) === null || _a === void 0 ? void 0 : _a.theme)
  );
};
