export declare const useTree: (
  workspaceId?: string,
) => import('@/pages/design/visualSource/@designable/core').TreeNode;
