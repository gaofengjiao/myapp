import { TreeNode } from '@/pages/design/visualSource/@designable/core';
export declare const useValidNodeOffsetRect: (node: TreeNode) => DOMRect;
