export declare const useViewport: (
  workspaceId?: string,
) => import('@/pages/design/visualSource/@designable/core').Viewport;
