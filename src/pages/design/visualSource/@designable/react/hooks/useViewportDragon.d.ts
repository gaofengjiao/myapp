export declare const useDragon: (
  workspaceId?: string,
) => import('@/pages/design/visualSource/@designable/core').Dragon;
