import { Workspace } from '@/pages/design/visualSource/@designable/core';
export declare const useWorkspace: (id?: string) => Workspace;
