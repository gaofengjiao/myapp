/// <reference types="react" />
export declare const DragSourceAnimation: {
  light: JSX.Element;
  dark: JSX.Element;
};
export declare const BatchDragAnimation: {
  light: JSX.Element;
  dark: JSX.Element;
};
