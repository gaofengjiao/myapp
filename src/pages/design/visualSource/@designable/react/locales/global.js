export default {
  'zh-CN': {
    save: '保存111',
    submit: '提交222',
    cancel: '取消333',
    reset: '重置444',
    publish: '发布5555',
  },
  'en-US': {
    save: 'Save',
    submit: 'Submit',
    cancel: 'Cancel',
    reset: 'Reset',
    publish: 'Publish',
  },
};
