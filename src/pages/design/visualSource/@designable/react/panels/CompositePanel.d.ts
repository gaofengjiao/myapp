import React from 'react';
export interface ICompositePanelItemProps {
  title?: React.ReactNode;
  icon?: React.ReactNode;
  href?: string;
  extra?: React.ReactNode;
}
export declare const CompositePanel: React.FC & {
  Item?: React.FC<ICompositePanelItemProps>;
};
