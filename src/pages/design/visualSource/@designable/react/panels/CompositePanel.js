var __read =
  (this && this.__read) ||
  function (o, n) {
    var m = typeof Symbol === 'function' && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o),
      r,
      ar = [],
      e;
    try {
      while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    } catch (error) {
      e = { error: error };
    } finally {
      try {
        if (r && !r.done && (m = i['return'])) m.call(i);
      } finally {
        if (e) throw e.error;
      }
    }
    return ar;
  };
import React, { useState } from 'react';
import cls from 'classnames';
import { IconWidget, TextWidget } from '../widgets';
import { usePrefix } from '../hooks';
var parseItems = function (children) {
  var items = [];
  React.Children.forEach(children, function (child) {
    if (child['type'] === CompositePanel.Item) {
      items.push(child['props']);
    }
  });
  return items;
};
export var CompositePanel = function (props) {
  var prefix = usePrefix('composite-panel');
  var _a = __read(useState(0), 2),
    activeKey = _a[0],
    setActiveKey = _a[1];
  var _b = __read(useState(false), 2),
    pinning = _b[0],
    setPinning = _b[1];
  var _c = __read(useState(true), 2),
    visible = _c[0],
    setVisible = _c[1];
  var items = parseItems(props.children);
  var currentItem = items === null || items === void 0 ? void 0 : items[activeKey];
  var content = currentItem === null || currentItem === void 0 ? void 0 : currentItem.children;
  var renderContent = function () {
    if (!content || !visible) return;
    return React.createElement(
      'div',
      { className: cls(prefix + '-tabs-content', { pinning: pinning }) },
      // React.createElement("div", { className: prefix + '-tabs-header' },
      //     React.createElement("div", { className: prefix + '-tabs-header-title' },
      //         React.createElement(TextWidget, null, currentItem.title)),
      //     React.createElement("div", { className: prefix + '-tabs-header-actions' },
      // React.createElement("div", { className: prefix + '-tabs-header-extra' }, currentItem.extra),
      // !pinning && (React.createElement(IconWidget, { infer: "PushPinOutlined", className: prefix + '-tabs-header-pin', onClick: function () {
      //         setPinning(!pinning);
      //     } })),
      // pinning && (React.createElement(IconWidget, { infer: "PushPinFilled", className: prefix + '-tabs-header-pin-filled', onClick: function () {
      //         setPinning(!pinning);
      //     } })),
      // React.createElement(IconWidget, { infer: "Close", className: prefix + '-tabs-header-close', onClick: function () {
      //         setVisible(false);
      //     } })
      // ),
      // ),
      React.createElement('div', { className: prefix + '-tabs-body' }, content),
    );
  };
  return React.createElement(
    'div',
    { className: prefix },
    // React.createElement("div", { className: prefix + '-tabs' }, items.map(function (item, index) {
    //     var takeTab = function () {
    //         if (item.href) {
    //             return React.createElement("a", { href: item.href }, item.icon);
    //         }
    //         return React.createElement(IconWidget, { infer: item.icon });
    //     };
    //     return (React.createElement("div", { className: cls(prefix + '-tabs-pane', {
    //             active: activeKey === index,
    //         }), key: index, onClick: function () {
    //             setVisible(true);
    //             if (index === activeKey) {
    //                 setVisible(!visible);
    //             }
    //             else {
    //                 setVisible(true);
    //             }
    //             setActiveKey(index);
    //         } }));
    // })),
    renderContent(),
  );
};
CompositePanel.Item = function () {
  return React.createElement(React.Fragment, null);
};
