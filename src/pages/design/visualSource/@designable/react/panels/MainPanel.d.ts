import React from 'react';
export interface IMainPanelProps {
  style?: React.CSSProperties;
  className?: string;
  logo?: React.ReactNode;
  actions?: React.ReactNode;
}
export declare const MainPanel: React.FC<IMainPanelProps>;
