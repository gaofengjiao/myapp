var __assign =
  (this && this.__assign) ||
  function () {
    __assign =
      Object.assign ||
      function (t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
      };
    return __assign.apply(this, arguments);
  };
var __rest =
  (this && this.__rest) ||
  function (s, e) {
    var t = {};
    for (var p in s)
      if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === 'function')
      for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
        if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
          t[p[i]] = s[p[i]];
      }
    return t;
  };
import React from 'react';
import { usePrefix } from '../hooks';
import cls from 'classnames';
export var MainPanel = function (_a) {
  var logo = _a.logo,
    actions = _a.actions,
    props = __rest(_a, ['logo', 'actions']);
  var prefix = usePrefix('main-panel');
  if (logo || actions) {
    return React.createElement(
      'div',
      __assign({}, props, { className: cls(prefix + '-container', 'root', props.className) }),
      React.createElement(
        'div',
        { className: prefix + '-header' },
        React.createElement('div', { className: prefix + '-header-logo' }, logo),
        React.createElement('div', { className: prefix + '-header-actions' }, actions),
      ),
      React.createElement('div', { className: prefix }, props.children),
    );
  }
  return React.createElement(
    'div',
    __assign({}, props, { className: cls(prefix, 'root') }),
    props.children,
  );
};
