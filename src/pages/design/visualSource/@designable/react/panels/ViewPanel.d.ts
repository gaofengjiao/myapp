import React from 'react';
import { TreeNode, ITreeNode, WorkbenchTypes } from '@/pages/design/visualSource/@designable/core';
export interface IViewPanelProps {
  type: WorkbenchTypes;
  children: (tree: TreeNode, onChange: (tree: ITreeNode) => void) => React.ReactElement;
  scrollable?: boolean;
}
export declare const ViewPanel: React.FC<IViewPanelProps>;
