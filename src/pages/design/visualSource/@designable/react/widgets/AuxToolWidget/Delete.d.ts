import React from 'react';
import { TreeNode } from '@/pages/design/visualSource/@designable/core';
export interface IDeleteProps {
  node: TreeNode;
  style?: React.CSSProperties;
}
export declare const Delete: React.FC<IDeleteProps>;
