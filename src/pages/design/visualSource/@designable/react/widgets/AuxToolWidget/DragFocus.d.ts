import React from 'react';
import { TreeNode } from '@/pages/design/visualSource/@designable/core';
export interface IDragFocusProps {
  node: TreeNode;
  style?: React.CSSProperties;
}
export declare const DragFocus: React.FC<IDragFocusProps>;
