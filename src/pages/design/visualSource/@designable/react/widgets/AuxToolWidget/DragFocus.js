import React from 'react';
import { observer } from '@/pages/design/visualSource/@formily/reactive-react';
import { IconWidget } from '../IconWidget';
import { useOperation, usePrefix } from '../../hooks';
import { Button } from 'antd';
export var DragFocus = observer(function (_a) {
  var node = _a.node,
    style = _a.style;
  var operation = useOperation();
  var prefix = usePrefix('aux-focus');
  if (node === node.root) return null;
  return React.createElement(
    Button,
    {
      className: prefix,
      style: style,
      type: 'primary',
      onClick: function () {
        operation.switchFocusNode(node);
      },
    },
    React.createElement(IconWidget, { infer: operation.focusNode === node ? 'Move' : 'Focus' }),
  );
});
DragFocus.displayName = 'DragFocus';
