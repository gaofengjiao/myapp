var __assign =
  (this && this.__assign) ||
  function () {
    __assign =
      Object.assign ||
      function (t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
      };
    return __assign.apply(this, arguments);
  };
var __read =
  (this && this.__read) ||
  function (o, n) {
    var m = typeof Symbol === 'function' && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o),
      r,
      ar = [],
      e;
    try {
      while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    } catch (error) {
      e = { error: error };
    } finally {
      try {
        if (r && !r.done && (m = i['return'])) m.call(i);
      } finally {
        if (e) throw e.error;
      }
    }
    return ar;
  };
import React, { useRef, useState, useLayoutEffect } from 'react';
import { reaction } from '@/pages/design/visualSource/@formily/reactive';
import cls from 'classnames';
import { useDesigner, usePrefix, useViewport } from '../../hooks';
import { Selector } from './Selector';
import { Copy } from './Copy';
import { Delete } from './Delete';
import { DragFocus } from './DragFocus';
var HELPER_DEBOUNCE_TIMEOUT = 100;
export var Helpers = function (_a) {
  var _b, _c;
  var _d, _e, _f, _g;
  var node = _a.node,
    nodeRect = _a.nodeRect;
  var prefix = usePrefix('aux-helpers');
  var designer = useDesigner();
  var viewport = useViewport();
  var unmountRef = useRef(false);
  var ref = useRef();
  var _h = __read(useState('top-right'), 2),
    position = _h[0],
    setPositin = _h[1];
  useLayoutEffect(
    function () {
      var request = null;
      var getYInViewport = function (nodeRect, helpersRect) {
        if (nodeRect.top - viewport.scrollY > helpersRect.height) {
          return 'top';
        } else if (viewport.isScrollTop && nodeRect.height + helpersRect.height > viewport.height) {
          return 'inner-top';
        } else if (
          viewport.isScrollBottom &&
          nodeRect.height + helpersRect.height > viewport.height
        ) {
          return 'inner-bottom';
        }
        return 'bottom';
      };
      var getXInViewport = function (nodeRect, helpersRect) {
        var widthDelta = helpersRect.width - nodeRect.width;
        if (widthDelta >= 0) {
          if (nodeRect.x < widthDelta) {
            return 'left';
          } else if (nodeRect.right + widthDelta > viewport.width) {
            return 'right';
          } else {
            return 'center';
          }
        }
        return 'right';
      };
      var update = function () {
        var _a;
        var helpersRect =
          (_a = ref.current) === null || _a === void 0 ? void 0 : _a.getBoundingClientRect();
        if (!helpersRect || !nodeRect) return;
        if (unmountRef.current) return;
        setPositin(
          getYInViewport(nodeRect, helpersRect) + '-' + getXInViewport(nodeRect, helpersRect),
        );
      };
      update();
      return reaction(
        function () {
          return [
            viewport.width,
            viewport.height,
            viewport.scrollX,
            viewport.scrollY,
            viewport.isScrollBottom,
            viewport.isScrollTop,
          ];
        },
        function () {
          clearTimeout(request);
          request = setTimeout(update, HELPER_DEBOUNCE_TIMEOUT);
        },
      );
    },
    [viewport, nodeRect],
  );
  if (!nodeRect || !node) return null;
  var helpersId =
    ((_b = {}),
    (_b[(_d = designer.props) === null || _d === void 0 ? void 0 : _d.nodeHelpersIdAttrName] =
      node.id),
    _b);
  return React.createElement(
    'div',
    __assign({}, helpersId, {
      className: cls(prefix, ((_c = {}), (_c[position] = true), _c)),
      ref: ref,
    }),
    React.createElement(
      'div',
      { className: cls(prefix + '-content') },
      React.createElement(Selector, { node: node }),
      ((_e = node === null || node === void 0 ? void 0 : node.designerProps) === null ||
      _e === void 0
        ? void 0
        : _e.cloneable) === false
        ? null
        : React.createElement(Copy, { node: node }),
      ((_f = node === null || node === void 0 ? void 0 : node.designerProps) === null ||
      _f === void 0
        ? void 0
        : _f.draggable) === false
        ? null
        : React.createElement(DragFocus, { node: node }),
      ((_g = node === null || node === void 0 ? void 0 : node.designerProps) === null ||
      _g === void 0
        ? void 0
        : _g.deletable) === false
        ? null
        : React.createElement(Delete, { node: node }),
    ),
  );
};
Helpers.displayName = 'Helpers';
