import React from 'react';
import { useDragon, usePrefix } from '../../hooks';
import { ClosestDirection } from '@/pages/design/visualSource/@designable/core';
import { observer } from '@/pages/design/visualSource/@formily/reactive-react';
export var Insertion = observer(function () {
  var viewportDragon = useDragon();
  var prefix = usePrefix('aux-insertion');
  var createInsertionStyle = function () {
    var _a, _b;
    var closestDirection = viewportDragon.closestDirection;
    var closestRect = viewportDragon.closestOffsetRect;
    var closestNode = viewportDragon.closestNode;
    var baseStyle = {
      position: 'absolute',
      transform: 'perspective(1px) translate3d(0,0,0)',
      top: 0,
      left: 0,
    };
    if (!closestRect) return baseStyle;
    if (
      closestDirection === ClosestDirection.Before ||
      closestDirection === ClosestDirection.ForbidBefore
    ) {
      baseStyle.width = 2;
      baseStyle.height = closestRect.height;
      baseStyle.transform =
        'perspective(1px) translate3d(' + closestRect.x + 'px,' + closestRect.y + 'px,0)';
    } else if (
      closestDirection === ClosestDirection.After ||
      closestDirection === ClosestDirection.ForbidAfter
    ) {
      baseStyle.width = 2;
      baseStyle.height = closestRect.height;
      baseStyle.transform =
        'perspective(1px) translate3d(' +
        (closestRect.x + closestRect.width - 2) +
        'px,' +
        closestRect.y +
        'px,0)';
    } else if (
      closestDirection === ClosestDirection.InnerAfter ||
      closestDirection === ClosestDirection.Under ||
      closestDirection === ClosestDirection.ForbidInnerAfter ||
      closestDirection === ClosestDirection.ForbidUnder
    ) {
      if (
        ((_a =
          closestNode === null || closestNode === void 0 ? void 0 : closestNode.designerProps) ===
          null || _a === void 0
          ? void 0
          : _a.inlineLayout) === true
      ) {
        baseStyle.width = 2;
        baseStyle.height = closestRect.height;
        baseStyle.transform =
          'perspective(1px) translate3d(' +
          (closestRect.x + closestRect.width - 2) +
          'px,' +
          closestRect.y +
          'px,0)';
      } else {
        baseStyle.width = closestRect.width;
        baseStyle.height = 2;
        baseStyle.transform =
          'perspective(1px) translate3d(' +
          closestRect.x +
          'px,' +
          (closestRect.y + closestRect.height - 2) +
          'px,0)';
      }
    } else if (
      closestDirection === ClosestDirection.InnerBefore ||
      closestDirection === ClosestDirection.Upper ||
      closestDirection === ClosestDirection.ForbidInnerBefore ||
      closestDirection === ClosestDirection.ForbidUpper
    ) {
      if (
        ((_b =
          closestNode === null || closestNode === void 0 ? void 0 : closestNode.designerProps) ===
          null || _b === void 0
          ? void 0
          : _b.inlineLayout) === true
      ) {
        baseStyle.width = 2;
        baseStyle.height = closestRect.height;
        baseStyle.transform =
          'perspective(1px) translate3d(' + closestRect.x + 'px,' + closestRect.y + 'px,0)';
      } else {
        baseStyle.width = closestRect.width;
        baseStyle.height = 2;
        baseStyle.transform =
          'perspective(1px) translate3d(' + closestRect.x + 'px,' + closestRect.y + 'px,0)';
      }
    }
    if (closestDirection.includes('FORBID')) {
      baseStyle.backgroundColor = 'red';
    }
    return baseStyle;
  };
  return React.createElement('div', { className: prefix, style: createInsertionStyle() });
});
Insertion.displayName = 'Insertion';
