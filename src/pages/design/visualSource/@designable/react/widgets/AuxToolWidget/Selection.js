var __assign =
  (this && this.__assign) ||
  function () {
    __assign =
      Object.assign ||
      function (t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
      };
    return __assign.apply(this, arguments);
  };
import React, { Fragment } from 'react';
import { Helpers } from './Helpers';
import {
  useSelection,
  useValidNodeOffsetRect,
  useTree,
  useCursor,
  useDragon,
  usePrefix,
} from '../../hooks';
import { observer } from '@/pages/design/visualSource/@formily/reactive-react';
export var SelectionBox = function (props) {
  var prefix = usePrefix('aux-selection-box');
  var nodeRect = useValidNodeOffsetRect(props.node);
  var createSelectionStyle = function () {
    var baseStyle = {
      position: 'absolute',
      top: 0,
      left: 0,
      pointerEvents: 'none',
      boxSizing: 'border-box',
    };
    if (nodeRect) {
      baseStyle.transform =
        'perspective(1px) translate3d(' + nodeRect.x + 'px,' + nodeRect.y + 'px,0)';
      baseStyle.height = nodeRect.height;
      baseStyle.width = nodeRect.width;
    }
    return baseStyle;
  };
  if (!nodeRect) return null;
  if (!nodeRect.width || !nodeRect.height) return null;
  return React.createElement(
    'div',
    { className: prefix, style: createSelectionStyle() },
    props.showHelpers &&
      React.createElement(Helpers, __assign({}, props, { node: props.node, nodeRect: nodeRect })),
  );
};
export var Selection = observer(function () {
  var selection = useSelection();
  var tree = useTree();
  var cursor = useCursor();
  var viewportDragon = useDragon();
  if (cursor.status !== 'NORMAL' && viewportDragon.touchNode) return null;
  return React.createElement(
    Fragment,
    null,
    selection.selected.map(function (id) {
      var node = tree.findById(id);
      if (!node) return;
      if (node.hidden) return;
      return React.createElement(SelectionBox, {
        key: id,
        node: node,
        showHelpers: selection.selected.length === 1,
      });
    }),
  );
});
Selection.displayName = 'Selection';
