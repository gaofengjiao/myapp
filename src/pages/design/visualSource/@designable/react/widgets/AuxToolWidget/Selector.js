var __read =
  (this && this.__read) ||
  function (o, n) {
    var m = typeof Symbol === 'function' && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o),
      r,
      ar = [],
      e;
    try {
      while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    } catch (error) {
      e = { error: error };
    } finally {
      try {
        if (r && !r.done && (m = i['return'])) m.call(i);
      } finally {
        if (e) throw e.error;
      }
    }
    return ar;
  };
import React, { useEffect, useState, useRef } from 'react';
import { useHover, useSelection, usePrefix } from '../../hooks';
import { IconWidget } from '../IconWidget';
import { TextWidget } from '../TextWidget';
import { Button } from 'antd';
import { observer } from '@/pages/design/visualSource/@formily/reactive-react';
var useMouseHover = function (ref, enter, leave) {
  useEffect(function () {
    var timer = null;
    var unmounted = false;
    var onMouseOver = function (e) {
      var target = e.target;
      clearTimeout(timer);
      timer = setTimeout(function () {
        var _a;
        if (unmounted) return;
        if (
          (_a = ref === null || ref === void 0 ? void 0 : ref.current) === null || _a === void 0
            ? void 0
            : _a.contains(target)
        ) {
          enter && enter();
        } else {
          leave && leave();
        }
      }, 100);
    };
    document.addEventListener('mouseover', onMouseOver);
    return function () {
      unmounted = true;
      document.removeEventListener('mouseover', onMouseOver);
    };
  }, []);
};
export var Selector = observer(function (_a) {
  var _b;
  var node = _a.node;
  var hover = useHover();
  var _c = __read(useState(false), 2),
    expand = _c[0],
    setExpand = _c[1];
  var ref = useRef(null);
  var selection = useSelection();
  var prefix = usePrefix('aux-selector');
  var renderIcon = function (node) {
    var _a;
    var icon = node.designerProps.icon;
    if (icon) {
      return React.createElement(IconWidget, { infer: icon });
    }
    if (node === node.root) {
      return React.createElement(IconWidget, { infer: 'Page' });
    } else if ((_a = node.designerProps) === null || _a === void 0 ? void 0 : _a.droppable) {
      return React.createElement(IconWidget, { infer: 'Container' });
    }
    return React.createElement(IconWidget, { infer: 'Component' });
  };
  var renderMenu = function () {
    var parents = node.getParents();
    return React.createElement(
      'div',
      {
        className: prefix + '-menu',
        style: {
          position: 'absolute',
          top: '100%',
          left: 0,
        },
      },
      parents.slice(0, 4).map(function (parent) {
        var _a;
        return React.createElement(
          Button,
          {
            key: parent.id,
            type: 'primary',
            onClick: function () {
              selection.select(parent.id);
            },
            onMouseEnter: function () {
              hover.setHover(parent);
            },
          },
          renderIcon(parent),
          React.createElement(
            'span',
            { style: { transform: 'scale(0.85)', marginLeft: 2 } },
            React.createElement(
              TextWidget,
              null,
              ((_a = parent === null || parent === void 0 ? void 0 : parent.designerProps) ===
                null || _a === void 0
                ? void 0
                : _a.title) ||
                (parent === null || parent === void 0 ? void 0 : parent.componentName),
            ),
          ),
        );
      }),
    );
  };
  useMouseHover(
    ref,
    function () {
      setExpand(true);
    },
    function () {
      setExpand(false);
    },
  );
  return React.createElement(
    'div',
    { ref: ref, className: prefix },
    React.createElement(
      Button,
      {
        className: prefix + '-title',
        type: 'primary',
        onMouseEnter: function () {
          hover.setHover(node);
        },
      },
      renderIcon(node),
      React.createElement(
        'span',
        null,
        React.createElement(
          TextWidget,
          null,
          ((_b = node.designerProps) === null || _b === void 0 ? void 0 : _b.title) ||
            (node === null || node === void 0 ? void 0 : node.componentName),
        ),
      ),
    ),
    expand && renderMenu(),
  );
});
Selector.displayName = 'Selector';
