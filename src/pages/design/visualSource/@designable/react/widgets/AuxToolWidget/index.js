import React, { useEffect, useRef } from 'react';
import { CursorStatus, CursorType } from '@/pages/design/visualSource/@designable/core';
import {
  useViewport,
  useCursor,
  useDragon,
  useDesigner,
  usePrefix,
  useOperation,
} from '../../hooks';
import { Insertion } from './Insertion';
import { Selection } from './Selection';
import { FreeSelection } from './FreeSelection';
import { Cover } from './Cover';
import { DashedBox } from './DashedBox';
import './styles.less';
var setCursorState = function (contentWindow, state) {
  var _a, _b, _c, _d;
  var currentRoot =
    (_b =
      (_a = document === null || document === void 0 ? void 0 : document.getElementsByTagName) ===
        null || _a === void 0
        ? void 0
        : _a.call(document, 'html')) === null || _b === void 0
      ? void 0
      : _b[0];
  var root =
    (_d =
      (_c =
        contentWindow === null || contentWindow === void 0 ? void 0 : contentWindow.document) ===
        null || _c === void 0
        ? void 0
        : _c.getElementsByTagName('html')) === null || _d === void 0
      ? void 0
      : _d[0];
  if (root) {
    root.style.cursor = state;
  }
  if (currentRoot) {
    currentRoot.style.cursor = state;
  }
};
export var AuxToolWidget = function () {
  var engine = useDesigner();
  var viewport = useViewport();
  var operation = useOperation();
  var cursor = useCursor();
  var prefix = usePrefix('auxtool');
  var viewportDragon = useDragon();
  var ref = useRef();
  useEffect(
    function () {
      return engine.subscribeWith('viewport:scroll', function () {
        if (viewport.isIframe && ref.current) {
          ref.current.style.transform =
            'perspective(1px) translate3d(' +
            -viewport.scrollX +
            'px,' +
            -viewport.scrollY +
            'px,0)';
        }
      });
    },
    [engine, viewport],
  );
  useEffect(
    function () {
      return engine.subscribeWith(['drag:move', 'drag:stop'], function () {
        if (cursor.status !== CursorStatus.Dragging) {
          setCursorState(viewport.contentWindow, 'default');
        } else {
          if (cursor.type === CursorType.Move) {
            if (operation.getDragNodes().length) {
              // todo: update cusor will trigger document layout rerender https://bugs.chromium.org/p/chromium/issues/detail?id=664066
              // if (viewportDragon.closestDirection === ClosestDirection.Inner) {
              //   setCursorState(viewport.contentWindow, 'copy')
              // } else {
              setCursorState(viewport.contentWindow, 'move');
              //}
            }
          } else {
            if (cursor.type === CursorType.ResizeWidth) {
              setCursorState(viewport.contentWindow, 'ew-resize');
            } else if (cursor.type === CursorType.ResizeHeight) {
              setCursorState(viewport.contentWindow, 'ns-resize');
            } else if (cursor.type === CursorType.Resize) {
              setCursorState(viewport.contentWindow, 'nwse-resize');
            } else {
              setCursorState(viewport.contentWindow, 'default');
            }
          }
        }
      });
    },
    [engine, cursor, viewportDragon, viewport, operation],
  );
  if (!viewport) return null;
  return React.createElement(
    'div',
    { ref: ref, className: prefix },
    React.createElement(Insertion, null),
    React.createElement(DashedBox, null),
    React.createElement(Selection, null),
    React.createElement(Cover, null),
    React.createElement(FreeSelection, null),
  );
};
AuxToolWidget.displayName = 'AuxToolWidget';
