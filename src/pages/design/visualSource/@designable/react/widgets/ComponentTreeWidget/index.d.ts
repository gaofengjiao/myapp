import React from 'react';
import { TreeNode } from '@/pages/design/visualSource/@designable/core';
import './styles.less';
export interface IComponents {
  [key: string]: React.JSXElementConstructor<any>;
}
export interface IComponentTreeWidgetProps {
  style?: React.CSSProperties;
  className?: string;
  components: IComponents;
  onChange: (node: any, selected: any, location: string) => void;
}
export interface ITreeNodeWidgetProps {
  node: TreeNode;
  children?: React.ReactChild;
}
export declare const TreeNodeWidget: React.FC<ITreeNodeWidgetProps>;
export declare const ComponentTreeWidget: React.FC<IComponentTreeWidgetProps>;
