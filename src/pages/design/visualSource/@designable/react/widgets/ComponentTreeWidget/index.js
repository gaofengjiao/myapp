var __assign =
  (this && this.__assign) ||
  function () {
    __assign =
      Object.assign ||
      function (t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
      };
    return __assign.apply(this, arguments);
  };
var __read =
  (this && this.__read) ||
  function (o, n) {
    var m = typeof Symbol === 'function' && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o),
      r,
      ar = [],
      e;
    try {
      while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    } catch (error) {
      e = { error: error };
    } finally {
      try {
        if (r && !r.done && (m = i['return'])) m.call(i);
      } finally {
        if (e) throw e.error;
      }
    }
    return ar;
  };
var __spread =
  (this && this.__spread) ||
  function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
  };
import React, { Fragment, useEffect, useContext, createContext, useImperativeHandle } from 'react';
import { useTree, usePrefix, useDesigner, useRegistry, useSelected } from '../../hooks';
import { TreeNodeContext } from '../../context';
import { observer } from '@/pages/design/visualSource/@formily/reactive-react';
import cls from 'classnames';
import './styles.less';
var ComponentsContext = createContext({});
export var TreeNodeWidget = observer(function (props) {
  var _a, _b;
  var designer = useDesigner(
    (_b = (_a = props.node) === null || _a === void 0 ? void 0 : _a.designerProps) === null ||
      _b === void 0
      ? void 0
      : _b.effects,
  );
  var components = useContext(ComponentsContext);
  const selected = useSelected();
  var node = props.node;
  var renderChildren = function () {
    var _a, _b;
    if (
      (_a = node === null || node === void 0 ? void 0 : node.designerProps) === null ||
      _a === void 0
        ? void 0
        : _a.selfRenderChildren
    )
      return [];
    return (_b = node === null || node === void 0 ? void 0 : node.children) === null ||
      _b === void 0
      ? void 0
      : _b.map(function (child) {
          return React.createElement(TreeNodeWidget, { key: child.id, node: child });
        });
  };
  var renderProps = function (extendsProps) {
    var _a;
    if (extendsProps === void 0) {
      extendsProps = {};
    }
    if (
      (_a = node === null || node === void 0 ? void 0 : node.designerProps) === null ||
      _a === void 0
        ? void 0
        : _a.getComponentProps
    ) {
      return __assign(__assign({}, extendsProps), node.designerProps.getComponentProps(node));
    }
    return __assign(__assign({}, extendsProps), node.props);
  };
  var renderComponent = function () {
    var _a, _b;
    var componentName = node.componentName;
    var Component = components[componentName];
    var dataId = {};
    if (Component) {
      if (designer) {
        dataId[
          (_a = designer === null || designer === void 0 ? void 0 : designer.props) === null ||
          _a === void 0
            ? void 0
            : _a.nodeIdAttrName
        ] = node.id;
      }
      return React.createElement.apply(
        React,
        __spread([Component, renderProps(dataId)], renderChildren()),
      );
    } else {
      if (
        (_b = node === null || node === void 0 ? void 0 : node.children) === null || _b === void 0
          ? void 0
          : _b.length
      ) {
        return React.createElement(Fragment, null, renderChildren());
      }
    }
  };
  if (!node) return null;
  if (node.hidden) return null;
  return React.createElement(TreeNodeContext.Provider, { value: node }, renderComponent());
});
export var ComponentTreeWidget = observer(function (props) {
  var _a;
  var tree = useTree();
  var prefix = usePrefix('component-tree');
  var designer = useDesigner();
  var registry = useRegistry();
  const selected = useSelected();
  var dataId = {};

  useEffect(() => {
    if (selected) {
      console.log(selected, 'selected');
      if (props.onChange) {
        props.onChange(tree, selected);
      }
    }
  }, [selected]);

  useEffect(function () {
    if (designer) {
      Object.entries(props.components).forEach(function (_a) {
        var _b = __read(_a, 2),
          componentName = _b[0],
          component = _b[1];
        if (component['designerProps']) {
          registry.setComponentDesignerProps(componentName, component['designerProps']);
        }
      });
    }
  }, []);

  if (designer) {
    dataId[
      (_a = designer === null || designer === void 0 ? void 0 : designer.props) === null ||
      _a === void 0
        ? void 0
        : _a.nodeIdAttrName
    ] = tree.id;
  }
  return React.createElement(
    'div',
    __assign({ style: props.style, className: cls(prefix, props.className) }, dataId),
    React.createElement(
      ComponentsContext.Provider,
      { value: props.components },
      React.createElement(TreeNodeWidget, { node: tree, onChange: props.onChange }),
    ),
  );
});
ComponentTreeWidget.displayName = 'ComponentTreeWidget';
