import React from 'react';
import { TreeNode } from '@/pages/design/visualSource/@designable/core';
import './styles.less';
export declare type SourceMapper = (node: TreeNode) => React.ReactChild;
export interface IDragSourceWidgetProps {
  name: string;
  title: React.ReactNode;
  className?: string;
  defaultExpand?: boolean;
  children?: SourceMapper | React.ReactElement;
}
export declare const DragSourceWidget: React.FC<IDragSourceWidgetProps>;
