var __read =
  (this && this.__read) ||
  function (o, n) {
    var m = typeof Symbol === 'function' && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o),
      r,
      ar = [],
      e;
    try {
      while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    } catch (error) {
      e = { error: error };
    } finally {
      try {
        if (r && !r.done && (m = i['return'])) m.call(i);
      } finally {
        if (e) throw e.error;
      }
    }
    return ar;
  };
import React, { useState } from 'react';
import { isFn } from '@/pages/design/visualSource/@designable/shared';
import { observer } from '@/pages/design/visualSource/@formily/reactive-react';
import cls from 'classnames';
import { useDesigner, usePrefix, useWorkspace } from '../../hooks';
import { IconWidget } from '../IconWidget';
import { TextWidget } from '../TextWidget';
import './styles.less';
export var DragSourceWidget = observer(function (props) {
  var _a;
  var prefix = usePrefix('drag-source');
  var designer = useDesigner();
  var workspace = useWorkspace();
  var _b = __read(useState(props.defaultExpand), 2),
    expand = _b[0],
    setExpand = _b[1];
  var renderNode = function (node) {
    var _a, _b, _c;
    return React.createElement(
      'div',
      { className: prefix + '-item', key: node.id, 'data-designer-source-id': node.id },
      ((_a = node === null || node === void 0 ? void 0 : node.designerProps) === null ||
      _a === void 0
        ? void 0
        : _a.icon) &&
        React.createElement(IconWidget, {
          infer:
            (_b = node === null || node === void 0 ? void 0 : node.designerProps) === null ||
            _b === void 0
              ? void 0
              : _b.icon,
          size: 12,
          style: { marginRight: 3 },
        }),
      React.createElement(
        TextWidget,
        null,
        (_c = node === null || node === void 0 ? void 0 : node.designerProps) === null ||
          _c === void 0
          ? void 0
          : _c.title,
      ),
    );
  };
  var source =
    ((_a = workspace === null || workspace === void 0 ? void 0 : workspace.source) === null ||
    _a === void 0
      ? void 0
      : _a.size) > 0
      ? workspace.source
      : designer.source;
  return React.createElement(
    'div',
    {
      className: cls(prefix, props.className, {
        expand: expand,
      }),
    },
    React.createElement(
      'div',
      {
        className: prefix + '-header',
        onClick: function (e) {
          e.stopPropagation();
          e.preventDefault();
          setExpand(!expand);
        },
      },
      React.createElement(
        'div',
        { className: prefix + '-header-expand' },
        React.createElement(IconWidget, { infer: 'Expand' }),
      ),
      React.createElement(
        'div',
        { className: prefix + '-header-content' },
        React.createElement(TextWidget, null, props.title || 'sources.' + props.name),
      ),
    ),
    React.createElement(
      'div',
      { className: prefix + '-content-wrapper' },
      React.createElement(
        'div',
        { className: prefix + '-content' },
        source.mapSourcesByGroup(props.name, isFn(props.children) ? props.children : renderNode),
      ),
    ),
  );
});
DragSourceWidget.defaultProps = {
  defaultExpand: true,
};
