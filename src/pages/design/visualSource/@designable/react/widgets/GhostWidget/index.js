import React from 'react';
import { useCursor, usePrefix, useDesigner } from '../../hooks';
import { CursorStatus } from '@/pages/design/visualSource/@designable/core';
import { observer } from '@/pages/design/visualSource/@formily/reactive-react';
import { TextWidget } from '../TextWidget';
import './styles.less';
export var GhostWidget = observer(function () {
  var _a, _b;
  var designer = useDesigner();
  var cursor = useCursor();
  var prefix = usePrefix('ghost');
  var draggingNodes = designer.findDraggingNodes();
  var firstNode = draggingNodes[0];
  var renderNodes = function () {
    var _a;
    return React.createElement(
      'span',
      {
        style: {
          whiteSpace: 'nowrap',
        },
      },
      React.createElement(
        TextWidget,
        null,
        ((_a = firstNode === null || firstNode === void 0 ? void 0 : firstNode.designerProps) ===
          null || _a === void 0
          ? void 0
          : _a.title) ||
          (firstNode === null || firstNode === void 0 ? void 0 : firstNode.componentName) ||
          'NoTitleComponent',
      ),
      draggingNodes.length > 1 ? '...' : '',
    );
  };
  if (!firstNode) return null;
  return cursor.status === CursorStatus.Dragging
    ? React.createElement(
        'div',
        {
          className: prefix,
          style: {
            transform:
              'perspective(1px) translate3d(' +
              (((_a = cursor.position) === null || _a === void 0 ? void 0 : _a.topClientX) - 18) +
              'px,' +
              (((_b = cursor.position) === null || _b === void 0 ? void 0 : _b.topClientY) - 12) +
              'px,0) scale(0.8)',
          },
        },
        renderNodes(),
      )
    : null;
});
GhostWidget.displayName = 'GhostWidget';
