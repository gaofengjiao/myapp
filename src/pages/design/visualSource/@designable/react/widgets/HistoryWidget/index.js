import React from 'react';
import format from 'dateformat';
import { observer } from '@/pages/design/visualSource/@formily/reactive-react';
import { usePrefix, useWorkspace } from '../../hooks';
import { TextWidget } from '../TextWidget';
import cls from 'classnames';
import './styles.less';
export var HistoryWidget = observer(function () {
  var currentWorkspace = useWorkspace();
  var prefix = usePrefix('history');
  if (!currentWorkspace) return null;
  return React.createElement(
    'div',
    { className: prefix },
    currentWorkspace.history.list().map(function (item, index) {
      var type = item.type || 'default_state';
      var token = type.replace(/\:/g, '_');
      return React.createElement(
        'div',
        {
          className: cls(prefix + '-item', {
            active: currentWorkspace.history.current === index,
          }),
          key: item.timestamp,
          onClick: function () {
            currentWorkspace.history.goTo(index);
          },
        },
        React.createElement(
          'span',
          { className: prefix + '-item-title' },
          React.createElement(TextWidget, { token: 'operations.' + token }),
        ),
        React.createElement(
          'span',
          { className: prefix + '-item-timestamp' },
          ' ',
          format(item.timestamp, 'yy/mm/dd HH:MM:ss'),
        ),
      );
    }),
  );
});
