import React from 'react';
import './styles.less';
export interface IconProviderProps {
  tooltip?: boolean;
}
export interface IIconWidgetProps extends React.HTMLAttributes<HTMLElement> {
  tooltip?: React.ReactNode;
  infer: React.ReactNode;
  size?: number | string;
}
export declare const IconWidget: React.FC<IIconWidgetProps> & {
  Provider?: React.FC<IconProviderProps>;
};
