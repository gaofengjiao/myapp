var __assign =
  (this && this.__assign) ||
  function () {
    __assign =
      Object.assign ||
      function (t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
      };
    return __assign.apply(this, arguments);
  };
import React, { createContext, useContext } from 'react';
import { GlobalRegistry } from '@/pages/design/visualSource/@designable/core';
import { isStr, isFn, isPlainObj } from '@/pages/design/visualSource/@designable/shared';
import { observer } from '@/pages/design/visualSource/@formily/reactive-react';
import { Tooltip } from 'antd';
import { usePrefix, useRegistry, useTheme } from '../../hooks';
import cls from 'classnames';
import * as icons from '../../icons';
import './styles.less';
GlobalRegistry.registerDesignerIcons(icons);
var IconContext = createContext(null);
export var IconWidget = observer(function (props) {
  var _a, _b, _c;
  var theme = useTheme();
  var context = useContext(IconContext);
  var registry = useRegistry();
  var prefix = usePrefix('icon');
  var size = props.size || '1em';
  var height = ((_a = props.style) === null || _a === void 0 ? void 0 : _a.height) || size;
  var width = ((_b = props.style) === null || _b === void 0 ? void 0 : _b.width) || size;
  var takeIcon = function (infer) {
    if (isStr(infer)) {
      var finded = registry.getDesignerIcon(infer);
      if (finded) {
        return takeIcon(finded);
      }
      return React.createElement('img', { src: infer, height: height, width: width });
    } else if (isFn(infer)) {
      return React.createElement(infer, {
        height: height,
        width: width,
        fill: 'currentColor',
      });
    } else if (React.isValidElement(infer)) {
      if (infer.type === 'svg') {
        return React.cloneElement(infer, {
          height: height,
          width: width,
          fill: 'currentColor',
          viewBox: infer.props.viewBox || '0 0 1024 1024',
          focusable: 'false',
          'aria-hidden': 'true',
        });
      } else if (infer.type === 'path') {
        return React.createElement(
          'svg',
          {
            viewBox: '0 0 1024 1024',
            height: height,
            width: width,
            fill: 'currentColor',
            focusable: 'false',
            'aria-hidden': 'true',
          },
          infer,
        );
      }
      return infer;
    } else if (isPlainObj(infer)) {
      if (infer[theme]) {
        return takeIcon(infer[theme]);
      }
    }
  };
  var renderTooltips = function (children) {
    if (!context) return children;
    if (!isStr(props.infer) && context.tooltip) return children;
    var tooltip = props.tooltip || registry.getDesignerMessage('icons.' + props.infer);
    if (tooltip) {
      return React.createElement(Tooltip, { title: tooltip }, children);
    }
    return children;
  };
  return renderTooltips(
    React.createElement(
      'span',
      __assign({}, props, {
        className: cls(prefix, props.className),
        style: __assign(__assign({}, props.style), {
          cursor: props.onClick
            ? 'pointer'
            : (_c = props.style) === null || _c === void 0
            ? void 0
            : _c.cursor,
        }),
      }),
      takeIcon(props.infer),
    ),
  );
});
IconWidget.Provider = function (props) {
  return React.createElement(IconContext.Provider, { value: props }, props.children);
};
