import React from 'react';
export interface ITextWidgetProps {
  token?: string;
}
export declare const TextWidget: React.FC<ITextWidgetProps>;
