import React, { Fragment } from 'react';
import { isStr } from '@/pages/design/visualSource/@designable/shared';
import { GlobalRegistry } from '@/pages/design/visualSource/@designable/core';
import { observer } from '@/pages/design/visualSource/@formily/reactive-react';
export var TextWidget = observer(function (props) {
  var token = props.token ? props.token : isStr(props.children) ? props.children : null;
  if (token) {
    var message = GlobalRegistry.getDesignerMessage(token);
    if (message) return message;
  }
  return React.createElement(Fragment, null, props.children);
});
