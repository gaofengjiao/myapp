export interface IPoint {
  x: number;
  y: number;
}
export declare class Point implements IPoint {
  x: number;
  y: number;
  constructor(x: number, y: number);
}
export interface IRect {
  x: number;
  y: number;
  width: number;
  height: number;
}
export declare enum RectQuadrant {
  Inner1 = 'I1',
  Inner2 = 'I2',
  Inner3 = 'I3',
  Inner4 = 'I4',
  Outer1 = 'O1',
  Outer2 = 'O2',
  Outer3 = 'O3',
  Outer4 = 'O4',
}
export interface IPointToRectRelative {
  quadrant: RectQuadrant;
  distance: number;
}
export declare function isPointInRect(point: IPoint, rect: IRect, sensitive?: boolean): boolean;
export declare function getRectPoints(source: IRect): Point[];
export declare function isRectInRect(target: IRect, source: IRect): boolean;
export declare function isCrossRectInRect(target: IRect, source: IRect): boolean;
/**
 * 计算点在矩形的哪个象限
 * @param point
 * @param rect
 */
export declare function calcQuadrantOfPonitToRect(point: IPoint, rect: IRect): RectQuadrant;
export declare function calcDistanceOfPointToRect(point: IPoint, rect: IRect): number;
export declare function calcDistancePointToEdge(point: IPoint, rect: IRect): number;
export declare function isNearAfter(point: IPoint, rect: IRect, inline?: boolean): boolean;
/**
 * 计算点鱼矩形的相对位置信息
 * @param point
 * @param rect
 */
export declare function calcRelativeOfPointToRect(point: IPoint, rect: IRect): IPointToRectRelative;
export declare function calcBoundingRect(rects: IRect[]): DOMRect;
export declare function calcRectByStartEndPoint(
  startPoint: IPoint,
  endPoint: IPoint,
  scrollX?: number,
  scrollY?: number,
): DOMRect;
