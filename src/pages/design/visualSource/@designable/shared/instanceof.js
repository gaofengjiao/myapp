import { isStr, isFn } from './types';
export var instOf = function (value, cls) {
  if (isFn(cls)) return value instanceof cls;
  if (isStr(cls)) return window[cls] ? value instanceof window[cls] : false;
  return false;
};
