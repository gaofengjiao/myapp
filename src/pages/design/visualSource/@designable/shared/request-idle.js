import 'requestidlecallback';
export var requestIdle = function (callback, options) {
  return window['requestIdleCallback'](callback, options);
};
export var cancelIdle = function (id) {
  window['cancelIdleCallback'](id);
};
