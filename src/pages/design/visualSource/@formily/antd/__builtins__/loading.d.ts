export declare const loading: (
  title: React.ReactNode,
  processor: () => Promise<any>,
) => Promise<any>;
