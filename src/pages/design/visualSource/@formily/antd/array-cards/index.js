var __assign =
  (this && this.__assign) ||
  function () {
    __assign =
      Object.assign ||
      function (t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
      };
    return __assign.apply(this, arguments);
  };
import React from 'react';
import { Card, Empty } from 'antd';
import {
  useField,
  observer,
  useFieldSchema,
  RecursionField,
} from '@/pages/design/visualSource/@formily/react';
import cls from 'classnames';
import { usePrefixCls } from '../__builtins__';
import { ArrayBase } from '../array-base';
var isAdditionComponent = function (schema) {
  var _a;
  return (
    ((_a = schema['x-component']) === null || _a === void 0 ? void 0 : _a.indexOf('Addition')) > -1
  );
};
var isIndexComponent = function (schema) {
  var _a;
  return (
    ((_a = schema['x-component']) === null || _a === void 0 ? void 0 : _a.indexOf('Index')) > -1
  );
};
var isRemoveComponent = function (schema) {
  var _a;
  return (
    ((_a = schema['x-component']) === null || _a === void 0 ? void 0 : _a.indexOf('Remove')) > -1
  );
};
var isMoveUpComponent = function (schema) {
  var _a;
  return (
    ((_a = schema['x-component']) === null || _a === void 0 ? void 0 : _a.indexOf('MoveUp')) > -1
  );
};
var isMoveDownComponent = function (schema) {
  var _a;
  return (
    ((_a = schema['x-component']) === null || _a === void 0 ? void 0 : _a.indexOf('MoveDown')) > -1
  );
};
var isOperationComponent = function (schema) {
  return (
    isAdditionComponent(schema) ||
    isRemoveComponent(schema) ||
    isMoveDownComponent(schema) ||
    isMoveUpComponent(schema)
  );
};
export var ArrayCards = observer(function (props) {
  var field = useField();
  var schema = useFieldSchema();
  var dataSource = Array.isArray(field.value) ? field.value : [];
  var prefixCls = usePrefixCls('formily-array-cards', props);
  if (!schema) throw new Error('can not found schema object');
  var renderItems = function () {
    return dataSource === null || dataSource === void 0
      ? void 0
      : dataSource.map(function (item, index) {
          var items = Array.isArray(schema.items)
            ? schema.items[index] || schema.items[0]
            : schema.items;
          var title = React.createElement(
            'span',
            null,
            React.createElement(RecursionField, {
              schema: items,
              name: index,
              filterProperties: function (schema) {
                if (!isIndexComponent(schema)) return false;
                return true;
              },
              onlyRenderProperties: true,
            }),
            props.title || field.title,
          );
          var extra = React.createElement(
            'span',
            null,
            React.createElement(RecursionField, {
              schema: items,
              name: index,
              filterProperties: function (schema) {
                if (!isOperationComponent(schema)) return false;
                return true;
              },
              onlyRenderProperties: true,
            }),
            props.extra,
          );
          var content = React.createElement(RecursionField, {
            schema: items,
            name: index,
            filterProperties: function (schema) {
              if (isIndexComponent(schema)) return false;
              if (isOperationComponent(schema)) return false;
              return true;
            },
          });
          return React.createElement(
            ArrayBase.Item,
            { key: index, index: index },
            React.createElement(
              Card,
              __assign({}, props, {
                onChange: function () {},
                className: cls(prefixCls + '-item', props.className),
                title: title,
                extra: extra,
              }),
              content,
            ),
          );
        });
  };
  var renderAddition = function () {
    return schema.reduceProperties(function (addition, schema, key) {
      if (isAdditionComponent(schema)) {
        return React.createElement(RecursionField, { schema: schema, name: key });
      }
      return addition;
    }, null);
  };
  var renderEmpty = function () {
    if (dataSource === null || dataSource === void 0 ? void 0 : dataSource.length) return;
    return React.createElement(
      Card,
      __assign({}, props, {
        onChange: function () {},
        className: cls(prefixCls + '-item', props.className),
        title: props.title || field.title,
      }),
      React.createElement(Empty, null),
    );
  };
  return React.createElement(ArrayBase, null, renderEmpty(), renderItems(), renderAddition());
});
ArrayCards.displayName = 'ArrayCards';
ArrayBase.mixin(ArrayCards);
export default ArrayCards;
//# sourceMappingURL=index.js.map
