import React from 'react';
export declare const Cascader: React.ForwardRefExoticComponent<
  Partial<import('antd').CascaderProps> & React.RefAttributes<unknown>
>;
export default Cascader;
