var __assign =
  (this && this.__assign) ||
  function () {
    __assign =
      Object.assign ||
      function (t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
      };
    return __assign.apply(this, arguments);
  };
import { connect, mapProps, mapReadPretty } from '@/pages/design/visualSource/@formily/react';
import { DatePicker as AntdDatePicker } from 'antd';
import { PreviewText } from '../preview-text';
import { formatMomentValue, momentable } from '../__builtins__';
var mapDateFormat = function () {
  var getDefaultFormat = function (props) {
    if (props['picker'] === 'month') {
      return 'YYYY-MM';
    } else if (props['picker'] === 'quarter') {
      return 'YYYY-\\QQ';
    } else if (props['picker'] === 'year') {
      return 'YYYY';
    } else if (props['picker'] === 'week') {
      return 'gggg-wo';
    }
    return props['showTime'] ? 'YYYY-MM-DD HH:mm:ss' : 'YYYY-MM-DD';
  };
  return function (props) {
    var format = props['format'] || getDefaultFormat(props);
    var onChange = props.onChange;
    return __assign(__assign({}, props), {
      format: format,
      value: momentable(props.value, format === 'gggg-wo' ? 'gggg-ww' : format),
      onChange: function (value) {
        if (onChange) {
          onChange(formatMomentValue(value, format));
        }
      },
    });
  };
};
export var DatePicker = connect(
  AntdDatePicker,
  mapProps(mapDateFormat()),
  mapReadPretty(PreviewText.DatePicker),
);
DatePicker.RangePicker = connect(
  AntdDatePicker.RangePicker,
  mapProps(mapDateFormat()),
  mapReadPretty(PreviewText.DateRangePicker),
);
export default DatePicker;
//# sourceMappingURL=index.js.map
