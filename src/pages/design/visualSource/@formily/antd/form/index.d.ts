import React from 'react';
import { Form as FormType, IFormFeedback } from '@/pages/design/visualSource/@formily/core';
import { IProviderProps, JSXComponent } from '@/pages/design/visualSource/@formily/react';
import { IFormLayoutProps } from '../form-layout';
export interface FormProps extends IProviderProps, IFormLayoutProps {
  form: FormType;
  component?: JSXComponent;
  onAutoSubmit?: (values: any) => any;
  onAutoSubmitFailed?: (feedbacks: IFormFeedback[]) => void;
  previewTextPlaceholder?: React.ReactNode;
}
export declare const Form: React.FC<FormProps>;
export default Form;
