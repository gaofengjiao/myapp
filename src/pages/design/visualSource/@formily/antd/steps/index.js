var __assign =
  (this && this.__assign) ||
  function () {
    __assign =
      Object.assign ||
      function (t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
      };
    return __assign.apply(this, arguments);
  };
import React from 'react';
import { connect, mapProps, mapReadPretty } from '@/pages/design/visualSource/@formily/react';
import { Steps as AntdSteps } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';
export var DesignableStep = connect(
  AntdSteps,
  mapProps(function (props, field) {
    return __assign(__assign({}, props));
  }),
);
export default DesignableStep;
//# sourceMappingURL=index.js.map
