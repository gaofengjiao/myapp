/// <reference types="react" />
export declare const Switch: import('react').ForwardRefExoticComponent<
  Pick<
    Partial<import('antd').SwitchProps & import('react').RefAttributes<HTMLElement>>,
    | 'disabled'
    | 'loading'
    | 'style'
    | 'title'
    | 'size'
    | 'key'
    | 'defaultChecked'
    | 'className'
    | 'tabIndex'
    | 'onChange'
    | 'onClick'
    | 'prefixCls'
    | 'checked'
    | 'autoFocus'
    | 'checkedChildren'
    | 'unCheckedChildren'
  > &
    import('react').RefAttributes<unknown>
>;
export default Switch;
