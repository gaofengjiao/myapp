/// <reference types="react" />
export declare const Transfer: import('react').ForwardRefExoticComponent<
  Partial<import('antd').TransferProps<import('antd/lib/transfer').TransferItem>> &
    import('react').RefAttributes<unknown>
>;
export default Transfer;
