import { FormPath, FormPathPattern } from '@/pages/design/visualSource/@formily/shared';
import { ValidatorTriggerType } from '@/pages/design/visualSource/@formily/validator';
import { Form } from './Form';
import {
  JSXComponent,
  IFieldFeedback,
  FeedbackMessage,
  IFieldCaches,
  IFieldRequests,
  FieldDisplayTypes,
  FieldPatternTypes,
  FieldValidator,
  FieldDecorator,
  FieldComponent,
  FieldDataSource,
  ISearchFeedback,
  IFieldProps,
  IFieldResetOptions,
  IFieldState,
  IModelSetter,
  IModelGetter,
} from '../types';
import { Query } from './Query';
export declare class Field<
  Decorator extends JSXComponent = any,
  Component extends JSXComponent = any,
  TextType = any,
  ValueType = any,
> {
  displayName: string;
  title: TextType;
  description: TextType;
  selfDisplay: FieldDisplayTypes;
  selfPattern: FieldPatternTypes;
  loading: boolean;
  validating: boolean;
  modified: boolean;
  active: boolean;
  visited: boolean;
  inputValue: ValueType;
  inputValues: any[];
  initialized: boolean;
  dataSource: FieldDataSource;
  mounted: boolean;
  unmounted: boolean;
  validator: FieldValidator;
  decoratorType: Decorator;
  decoratorProps: Record<string, any>;
  componentType: Component;
  componentProps: Record<string, any>;
  feedbacks: IFieldFeedback[];
  address: FormPath;
  path: FormPath;
  form: Form;
  props: IFieldProps<Decorator, Component, TextType, ValueType>;
  protected caches: IFieldCaches;
  protected requests: IFieldRequests;
  protected disposers: (() => void)[];
  constructor(
    address: FormPathPattern,
    props: IFieldProps<Decorator, Component, TextType, ValueType>,
    form: Form,
    designable: boolean,
  );
  protected makeIndexes(address: FormPathPattern): void;
  protected initialize(
    props: IFieldProps<Decorator, Component, TextType, ValueType>,
    form: Form,
  ): void;
  protected makeObservable(designable: boolean): void;
  protected makeReactive(designable: boolean): void;
  get parent(): import('../types').GeneralField;
  get component(): FieldComponent<Component>;
  set component(value: FieldComponent<Component>);
  get decorator(): FieldDecorator<Decorator>;
  set decorator(value: FieldDecorator<Decorator>);
  get errors(): FeedbackMessage;
  get warnings(): FeedbackMessage;
  get successes(): FeedbackMessage;
  get valid(): boolean;
  get invalid(): boolean;
  get value(): ValueType;
  get initialValue(): ValueType;
  get display(): FieldDisplayTypes;
  get pattern(): FieldPatternTypes;
  get required(): boolean;
  get hidden(): boolean;
  get visible(): boolean;
  set hidden(hidden: boolean);
  set visible(visible: boolean);
  get disabled(): boolean;
  get readOnly(): boolean;
  get readPretty(): boolean;
  get editable(): boolean;
  get validateStatus(): 'error' | 'success' | 'warning' | 'validating';
  set readOnly(readOnly: boolean);
  set editable(editable: boolean);
  set disabled(disabled: boolean);
  set readPretty(readPretty: boolean);
  set pattern(pattern: FieldPatternTypes);
  set display(display: FieldDisplayTypes);
  set required(required: boolean);
  set value(value: ValueType);
  set initialValue(initialValue: ValueType);
  set errors(messages: FeedbackMessage);
  set warnings(messages: FeedbackMessage);
  set successes(messages: FeedbackMessage);
  setTitle: (title?: TextType) => void;
  setDescription: (description?: TextType) => void;
  setDataSource: (dataSource?: FieldDataSource) => void;
  setFeedback: (feedback?: IFieldFeedback) => void;
  setErrors: (messages?: FeedbackMessage) => void;
  setWarnings: (messages?: FeedbackMessage) => void;
  setSuccesses: (messages?: FeedbackMessage) => void;
  setValidator: (validator?: FieldValidator) => void;
  setRequired: (required?: boolean) => void;
  setValue: (value?: ValueType) => void;
  setInitialValue: (initialValue?: ValueType) => void;
  setDisplay: (type?: FieldDisplayTypes) => void;
  setPattern: (type?: FieldPatternTypes) => void;
  setLoading: (loading?: boolean) => void;
  setValidating: (validating?: boolean) => void;
  setComponent: <C extends unknown, ComponentProps extends object = {}>(
    component?: C,
    props?: ComponentProps,
  ) => void;
  setComponentProps: <ComponentProps extends object = {}>(props?: ComponentProps) => void;
  setDecorator: <D extends unknown, ComponentProps extends object = {}>(
    component?: D,
    props?: ComponentProps,
  ) => void;
  setDecoratorProps: <ComponentProps extends object = {}>(props?: ComponentProps) => void;
  setState: IModelSetter<IFieldState>;
  getState: IModelGetter<IFieldState>;
  onInit: (designable: boolean) => void;
  onMount: () => void;
  onUnmount: () => void;
  onInput: (...args: any[]) => Promise<void>;
  onFocus: (...args: any[]) => Promise<void>;
  onBlur: (...args: any[]) => Promise<void>;
  validate: (triggerType?: ValidatorTriggerType) => Promise<{}>;
  reset: (options?: IFieldResetOptions) => Promise<{}>;
  query: (pattern: FormPathPattern) => Query;
  queryFeedbacks: (search?: ISearchFeedback) => IFieldFeedback[];
  dispose: () => void;
  destroy: () => void;
  match: (pattern: FormPathPattern) => boolean;
}
