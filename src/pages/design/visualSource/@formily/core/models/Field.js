var __awaiter =
  (this && this.__awaiter) ||
  function (thisArg, _arguments, P, generator) {
    function adopt(value) {
      return value instanceof P
        ? value
        : new P(function (resolve) {
            resolve(value);
          });
    }
    return new (P || (P = Promise))(function (resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
var __generator =
  (this && this.__generator) ||
  function (thisArg, body) {
    var _ = {
        label: 0,
        sent: function () {
          if (t[0] & 1) throw t[1];
          return t[1];
        },
        trys: [],
        ops: [],
      },
      f,
      y,
      t,
      g;
    return (
      (g = { next: verb(0), throw: verb(1), return: verb(2) }),
      typeof Symbol === 'function' &&
        (g[Symbol.iterator] = function () {
          return this;
        }),
      g
    );
    function verb(n) {
      return function (v) {
        return step([n, v]);
      };
    }
    function step(op) {
      if (f) throw new TypeError('Generator is already executing.');
      while (_)
        try {
          if (
            ((f = 1),
            y &&
              (t =
                op[0] & 2
                  ? y['return']
                  : op[0]
                  ? y['throw'] || ((t = y['return']) && t.call(y), 0)
                  : y.next) &&
              !(t = t.call(y, op[1])).done)
          )
            return t;
          if (((y = 0), t)) op = [op[0] & 2, t.value];
          switch (op[0]) {
            case 0:
            case 1:
              t = op;
              break;
            case 4:
              _.label++;
              return { value: op[1], done: false };
            case 5:
              _.label++;
              y = op[1];
              op = [0];
              continue;
            case 7:
              op = _.ops.pop();
              _.trys.pop();
              continue;
            default:
              if (
                !((t = _.trys), (t = t.length > 0 && t[t.length - 1])) &&
                (op[0] === 6 || op[0] === 2)
              ) {
                _ = 0;
                continue;
              }
              if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) {
                _.label = op[1];
                break;
              }
              if (op[0] === 6 && _.label < t[1]) {
                _.label = t[1];
                t = op;
                break;
              }
              if (t && _.label < t[2]) {
                _.label = t[2];
                _.ops.push(op);
                break;
              }
              if (t[2]) _.ops.pop();
              _.trys.pop();
              continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [6, e];
          y = 0;
        } finally {
          f = t = 0;
        }
      if (op[0] & 5) throw op[1];
      return { value: op[0] ? op[1] : void 0, done: true };
    }
  };
import {
  FormPath,
  isValid,
  isArr,
  isBool,
  each,
  isFn,
  isEmpty,
  toArr,
} from '@/pages/design/visualSource/@formily/shared';
import { parseValidatorDescriptions } from '@/pages/design/visualSource/@formily/validator';
import {
  define,
  observable,
  reaction,
  batch,
  toJS,
  autorun,
  action,
} from '@/pages/design/visualSource/@formily/reactive';
import { LifeCycleTypes } from '../types';
import {
  buildNodeIndexes,
  validateToFeedbacks,
  initFieldUpdate,
  updateFeedback,
  queryFeedbacks,
  queryFeedbackMessages,
  getValuesFromEvent,
  modelStateSetter,
  modelStateGetter,
  isHTMLInputEvent,
  initFieldValue,
} from '../shared/internals';
import { isArrayField, isObjectField } from '../shared/checkers';
import { Query } from './Query';
var RESPONSE_REQUEST_DURATION = 100;
var Field = /** @class */ (function () {
  function Field(address, props, form, designable) {
    var _this = this;
    this.displayName = 'Field';
    this.caches = {};
    this.requests = {};
    this.disposers = [];
    this.setTitle = function (title) {
      _this.title = title;
    };
    this.setDescription = function (description) {
      _this.description = description;
    };
    this.setDataSource = function (dataSource) {
      _this.dataSource = dataSource;
    };
    this.setFeedback = function (feedback) {
      updateFeedback(_this, feedback);
    };
    this.setErrors = function (messages) {
      _this.errors = messages;
    };
    this.setWarnings = function (messages) {
      _this.warnings = messages;
    };
    this.setSuccesses = function (messages) {
      _this.successes = messages;
    };
    this.setValidator = function (validator) {
      _this.validator = validator;
    };
    this.setRequired = function (required) {
      _this.required = required;
    };
    this.setValue = function (value) {
      _this.value = value;
    };
    this.setInitialValue = function (initialValue) {
      _this.initialValue = initialValue;
    };
    this.setDisplay = function (type) {
      _this.display = type;
    };
    this.setPattern = function (type) {
      _this.pattern = type;
    };
    this.setLoading = function (loading) {
      clearTimeout(_this.requests.loading);
      if (loading) {
        _this.requests.loading = setTimeout(function () {
          batch(function () {
            _this.loading = loading;
            _this.form.notify(LifeCycleTypes.ON_FIELD_LOADING, _this);
          });
        }, RESPONSE_REQUEST_DURATION);
      } else if (_this.loading !== loading) {
        _this.loading = loading;
      }
    };
    this.setValidating = function (validating) {
      clearTimeout(_this.requests.validating);
      if (validating) {
        _this.requests.validating = setTimeout(function () {
          batch(function () {
            _this.validating = validating;
            _this.form.notify(LifeCycleTypes.ON_FIELD_VALIDATING, _this);
          });
        }, RESPONSE_REQUEST_DURATION);
      } else if (_this.validating !== validating) {
        _this.validating = validating;
      }
    };
    this.setComponent = function (component, props) {
      if (component) {
        _this.componentType = component;
      }
      if (props) {
        _this.componentProps = _this.componentProps || {};
        Object.assign(_this.componentProps, props);
      }
    };
    this.setComponentProps = function (props) {
      if (props) {
        _this.componentProps = _this.componentProps || {};
        Object.assign(_this.componentProps, props);
      }
    };
    this.setDecorator = function (component, props) {
      if (component) {
        _this.decoratorType = component;
      }
      if (props) {
        _this.decoratorProps = _this.decoratorProps || {};
        Object.assign(_this.decoratorProps, props);
      }
    };
    this.setDecoratorProps = function (props) {
      if (props) {
        _this.decoratorProps = _this.decoratorProps || {};
        Object.assign(_this.decoratorProps, props);
      }
    };
    this.setState = modelStateSetter(this);
    this.getState = modelStateGetter(this);
    this.onInit = function (designable) {
      _this.initialized = true;
      batch.scope(function () {
        initFieldValue(_this, designable);
      });
      batch.scope(function () {
        initFieldUpdate(_this);
      });
      _this.form.notify(LifeCycleTypes.ON_FIELD_INIT, _this);
    };
    this.onMount = function () {
      _this.mounted = true;
      _this.unmounted = false;
      _this.form.notify(LifeCycleTypes.ON_FIELD_MOUNT, _this);
    };
    this.onUnmount = function () {
      _this.mounted = false;
      _this.unmounted = true;
      _this.form.notify(LifeCycleTypes.ON_FIELD_UNMOUNT, _this);
    };
    this.onInput = function () {
      var args = [];
      for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
      }
      return __awaiter(_this, void 0, void 0, function () {
        var values, value;
        var _a;
        return __generator(this, function (_b) {
          switch (_b.label) {
            case 0:
              if ((_a = args[0]) === null || _a === void 0 ? void 0 : _a.target) {
                if (!isHTMLInputEvent(args[0])) return [2 /*return*/];
              }
              values = getValuesFromEvent(args);
              value = values[0];
              this.caches.inputting = true;
              this.inputValue = value;
              this.inputValues = values;
              this.value = value;
              this.modified = true;
              this.form.modified = true;
              this.form.notify(LifeCycleTypes.ON_FIELD_INPUT_VALUE_CHANGE, this);
              this.form.notify(LifeCycleTypes.ON_FORM_INPUT_CHANGE, this.form);
              return [4 /*yield*/, this.validate('onInput')];
            case 1:
              _b.sent();
              this.caches.inputting = false;
              return [2 /*return*/];
          }
        });
      });
    };
    this.onFocus = function () {
      var args = [];
      for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
      }
      return __awaiter(_this, void 0, void 0, function () {
        var _a;
        return __generator(this, function (_b) {
          switch (_b.label) {
            case 0:
              if ((_a = args[0]) === null || _a === void 0 ? void 0 : _a.target) {
                if (!isHTMLInputEvent(args[0], false)) return [2 /*return*/];
              }
              this.active = true;
              this.visited = true;
              return [4 /*yield*/, this.validate('onFocus')];
            case 1:
              _b.sent();
              return [2 /*return*/];
          }
        });
      });
    };
    this.onBlur = function () {
      var args = [];
      for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
      }
      return __awaiter(_this, void 0, void 0, function () {
        var _a;
        return __generator(this, function (_b) {
          switch (_b.label) {
            case 0:
              if ((_a = args[0]) === null || _a === void 0 ? void 0 : _a.target) {
                if (!isHTMLInputEvent(args[0], false)) return [2 /*return*/];
              }
              this.active = false;
              return [4 /*yield*/, this.validate('onBlur')];
            case 1:
              _b.sent();
              return [2 /*return*/];
          }
        });
      });
    };
    this.validate = function (triggerType) {
      return __awaiter(_this, void 0, void 0, function () {
        var start, end, allTriggerTypes, results_1, i, payload, results;
        var _this = this;
        return __generator(this, function (_a) {
          switch (_a.label) {
            case 0:
              start = function () {
                _this.setValidating(true);
                _this.form.notify(LifeCycleTypes.ON_FIELD_VALIDATE_START, _this);
              };
              end = function () {
                _this.setValidating(false);
                if (_this.valid) {
                  _this.form.notify(LifeCycleTypes.ON_FIELD_VALIDATE_SUCCESS, _this);
                } else {
                  _this.form.notify(LifeCycleTypes.ON_FIELD_VALIDATE_FAILED, _this);
                }
                _this.form.notify(LifeCycleTypes.ON_FIELD_VALIDATE_END, _this);
              };
              start();
              if (!!triggerType) return [3 /*break*/, 5];
              allTriggerTypes = parseValidatorDescriptions(this.validator).map(function (desc) {
                return desc.triggerType;
              });
              results_1 = {};
              i = 0;
              _a.label = 1;
            case 1:
              if (!(i < allTriggerTypes.length)) return [3 /*break*/, 4];
              return [4 /*yield*/, validateToFeedbacks(this, allTriggerTypes[i])];
            case 2:
              payload = _a.sent();
              each(payload, function (result, key) {
                results_1[key] = results_1[key] || [];
                results_1[key] = results_1[key].concat(result);
              });
              _a.label = 3;
            case 3:
              i++;
              return [3 /*break*/, 1];
            case 4:
              end();
              return [2 /*return*/, results_1];
            case 5:
              return [4 /*yield*/, validateToFeedbacks(this, triggerType)];
            case 6:
              results = _a.sent();
              end();
              return [2 /*return*/, results];
          }
        });
      });
    };
    this.reset = function (options) {
      return __awaiter(_this, void 0, void 0, function () {
        return __generator(this, function (_a) {
          switch (_a.label) {
            case 0:
              this.modified = false;
              this.visited = false;
              this.feedbacks = [];
              this.inputValue = undefined;
              this.inputValues = [];
              if (options === null || options === void 0 ? void 0 : options.forceClear) {
                if (isArrayField(this)) {
                  this.value = [];
                } else if (isObjectField(this)) {
                  this.value = {};
                } else {
                  this.value = undefined;
                }
              } else if (isValid(this.value)) {
                this.value = toJS(this.initialValue);
              }
              this.form.notify(LifeCycleTypes.ON_FIELD_RESET, this);
              if (!(options === null || options === void 0 ? void 0 : options.validate))
                return [3 /*break*/, 2];
              return [4 /*yield*/, this.validate()];
            case 1:
              return [2 /*return*/, _a.sent()];
            case 2:
              return [2 /*return*/];
          }
        });
      });
    };
    this.query = function (pattern) {
      return new Query({
        pattern: pattern,
        base: _this.address,
        form: _this.form,
      });
    };
    this.queryFeedbacks = function (search) {
      return queryFeedbacks(_this, search);
    };
    this.dispose = function () {
      _this.disposers.forEach(function (dispose) {
        dispose();
      });
      _this.form.removeEffects(_this);
    };
    this.destroy = function () {
      _this.dispose();
      delete _this.form.fields[_this.address.toString()];
    };
    this.match = function (pattern) {
      return FormPath.parse(pattern).matchAliasGroup(_this.address, _this.path);
    };
    this.initialize(props, form);
    this.makeIndexes(address);
    this.makeObservable(designable);
    this.makeReactive(designable);
    this.onInit(designable);
  }
  Field.prototype.makeIndexes = function (address) {
    buildNodeIndexes(this, address);
  };
  Field.prototype.initialize = function (props, form) {
    this.form = form;
    this.props = props;
    this.initialized = false;
    this.loading = false;
    this.validating = false;
    this.modified = false;
    this.active = false;
    this.visited = false;
    this.mounted = false;
    this.unmounted = false;
    this.inputValues = [];
    this.inputValue = null;
    this.feedbacks = [];
    this.title = props.title;
    this.description = props.description;
    this.display = this.props.display;
    this.pattern = this.props.pattern;
    this.editable = this.props.editable;
    this.disabled = this.props.disabled;
    this.readOnly = this.props.readOnly;
    this.readPretty = this.props.readPretty;
    this.visible = this.props.visible;
    this.hidden = this.props.hidden;
    this.dataSource = this.props.dataSource;
    this.validator = this.props.validator;
    this.required = this.props.required;
    this.decorator = toArr(this.props.decorator);
    this.component = toArr(this.props.component);
  };
  Field.prototype.makeObservable = function (designable) {
    if (designable) return;
    define(this, {
      title: observable.ref,
      description: observable.ref,
      dataSource: observable.ref,
      selfDisplay: observable.ref,
      selfPattern: observable.ref,
      loading: observable.ref,
      validating: observable.ref,
      modified: observable.ref,
      active: observable.ref,
      visited: observable.ref,
      initialized: observable.ref,
      mounted: observable.ref,
      unmounted: observable.ref,
      inputValue: observable.ref,
      inputValues: observable.ref,
      decoratorType: observable.ref,
      componentType: observable.ref,
      decoratorProps: observable,
      componentProps: observable,
      validator: observable.shallow,
      feedbacks: observable.shallow,
      component: observable.computed,
      decorator: observable.computed,
      errors: observable.computed,
      warnings: observable.computed,
      successes: observable.computed,
      valid: observable.computed,
      invalid: observable.computed,
      validateStatus: observable.computed,
      value: observable.computed,
      initialValue: observable.computed,
      display: observable.computed,
      pattern: observable.computed,
      required: observable.computed,
      hidden: observable.computed,
      visible: observable.computed,
      disabled: observable.computed,
      readOnly: observable.computed,
      readPretty: observable.computed,
      editable: observable.computed,
      setDisplay: action,
      setTitle: action,
      setDescription: action,
      setDataSource: action,
      setValue: action,
      setPattern: action,
      setInitialValue: action,
      setLoading: action,
      setValidating: action,
      setFeedback: action,
      setErrors: action,
      setWarnings: action,
      setSuccesses: action,
      setValidator: action,
      setRequired: action,
      setComponent: action,
      setComponentProps: action,
      setDecorator: action,
      setDecoratorProps: action,
      validate: action,
      reset: action,
      onInit: batch,
      onInput: batch,
      onMount: batch,
      onUnmount: batch,
      onFocus: batch,
      onBlur: batch,
    });
  };
  Field.prototype.makeReactive = function (designable) {
    var _this = this;
    if (designable) return;
    this.disposers.push(
      reaction(
        function () {
          return _this.value;
        },
        function (value) {
          _this.form.notify(LifeCycleTypes.ON_FIELD_VALUE_CHANGE, _this);
          if (isValid(value) && _this.modified && !_this.caches.inputting) {
            _this.validate();
          }
        },
      ),
      reaction(
        function () {
          return _this.initialValue;
        },
        function () {
          _this.form.notify(LifeCycleTypes.ON_FIELD_INITIAL_VALUE_CHANGE, _this);
        },
      ),
      reaction(
        function () {
          return _this.display;
        },
        function (display) {
          if (display === 'visible') {
            if (isEmpty(_this.value)) {
              _this.setValue(_this.caches.value);
              _this.caches.value = undefined;
            }
          } else {
            _this.caches.value = toJS(_this.value);
            if (display === 'none') {
              _this.form.deleteValuesIn(_this.path);
            }
          }
          if (display === 'none' || display === 'hidden') {
            _this.setFeedback({
              type: 'error',
              messages: [],
            });
          }
        },
      ),
      reaction(
        function () {
          return _this.pattern;
        },
        function (pattern) {
          if (pattern !== 'editable') {
            _this.setFeedback({
              type: 'error',
              messages: [],
            });
          }
        },
      ),
    );
    var reactions = toArr(this.props.reactions);
    this.form.addEffects(this, function () {
      reactions.forEach(function (reaction) {
        if (isFn(reaction)) {
          _this.disposers.push(
            autorun(function () {
              return reaction(_this);
            }),
          );
        }
      });
    });
  };
  Object.defineProperty(Field.prototype, 'parent', {
    get: function () {
      var parent = this.address.parent();
      var identifier = parent.toString();
      if (!identifier) return;
      while (!this.form.fields[identifier]) {
        parent = parent.parent();
        identifier = parent.toString();
        if (!identifier) return;
      }
      return this.form.fields[identifier];
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(Field.prototype, 'component', {
    get: function () {
      return [this.componentType, this.componentProps];
    },
    set: function (value) {
      var component = toArr(value);
      this.componentType = component[0];
      this.componentProps = component[1] || {};
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(Field.prototype, 'decorator', {
    get: function () {
      return [this.decoratorType, this.decoratorProps];
    },
    set: function (value) {
      var decorator = toArr(value);
      this.decoratorType = decorator[0];
      this.decoratorProps = decorator[1] || {};
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(Field.prototype, 'errors', {
    get: function () {
      return queryFeedbackMessages(this, {
        type: 'error',
      });
    },
    set: function (messages) {
      this.setFeedback({
        type: 'error',
        code: 'EffectError',
        messages: messages,
      });
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(Field.prototype, 'warnings', {
    get: function () {
      return queryFeedbackMessages(this, {
        type: 'warning',
      });
    },
    set: function (messages) {
      this.setFeedback({
        type: 'warning',
        code: 'EffectWarning',
        messages: messages,
      });
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(Field.prototype, 'successes', {
    get: function () {
      return queryFeedbackMessages(this, {
        type: 'success',
      });
    },
    set: function (messages) {
      this.setFeedback({
        type: 'success',
        code: 'EffectSuccess',
        messages: messages,
      });
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(Field.prototype, 'valid', {
    get: function () {
      return !this.errors.length;
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(Field.prototype, 'invalid', {
    get: function () {
      return !this.valid;
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(Field.prototype, 'value', {
    get: function () {
      return this.form.getValuesIn(this.path);
    },
    set: function (value) {
      this.form.setValuesIn(this.path, value);
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(Field.prototype, 'initialValue', {
    get: function () {
      return this.form.getInitialValuesIn(this.path);
    },
    set: function (initialValue) {
      this.form.setInitialValuesIn(this.path, initialValue);
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(Field.prototype, 'display', {
    get: function () {
      var _a;
      var parentDisplay = (_a = this.parent) === null || _a === void 0 ? void 0 : _a.display;
      if (parentDisplay && parentDisplay !== 'visible') {
        if (this.selfDisplay && this.selfDisplay !== 'visible') return this.selfDisplay;
        return parentDisplay;
      }
      if (this.selfDisplay) return this.selfDisplay;
      return parentDisplay || this.form.display || 'visible';
    },
    set: function (display) {
      this.selfDisplay = display;
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(Field.prototype, 'pattern', {
    get: function () {
      var _a;
      var parentPattern = (_a = this.parent) === null || _a === void 0 ? void 0 : _a.pattern;
      if (this.selfPattern) return this.selfPattern;
      return parentPattern || this.form.pattern || 'editable';
    },
    set: function (pattern) {
      this.selfPattern = pattern;
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(Field.prototype, 'required', {
    get: function () {
      return parseValidatorDescriptions(this.validator).some(function (desc) {
        return desc.required;
      });
    },
    set: function (required) {
      if (!isBool(required)) return;
      var hasRequired = parseValidatorDescriptions(this.validator).some(function (desc) {
        return 'required' in desc;
      });
      if (hasRequired) {
        if (isArr(this.validator)) {
          this.validator = this.validator.map(function (desc) {
            if (Object.prototype.hasOwnProperty.call(desc, 'required')) {
              desc.required = required;
              return desc;
            }
            return desc;
          });
        } else {
          this.validator['required'] = required;
        }
      } else {
        if (isArr(this.validator)) {
          this.validator.unshift({
            required: required,
          });
        } else if (typeof this.validator === 'object') {
          this.validator['required'] = required;
        } else if (this.validator) {
          this.validator = [
            {
              required: required,
            },
            this.validator,
          ];
        } else if (required) {
          this.validator = [
            {
              required: required,
            },
          ];
        }
      }
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(Field.prototype, 'hidden', {
    get: function () {
      return this.display === 'hidden';
    },
    set: function (hidden) {
      if (!isValid(hidden)) return;
      if (hidden) {
        this.display = 'hidden';
      } else {
        this.display = 'visible';
      }
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(Field.prototype, 'visible', {
    get: function () {
      return this.display === 'visible';
    },
    set: function (visible) {
      if (!isValid(visible)) return;
      if (visible) {
        this.display = 'visible';
      } else {
        this.display = 'none';
      }
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(Field.prototype, 'disabled', {
    get: function () {
      return this.pattern === 'disabled';
    },
    set: function (disabled) {
      if (!isValid(disabled)) return;
      if (disabled) {
        this.pattern = 'disabled';
      } else {
        this.pattern = 'editable';
      }
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(Field.prototype, 'readOnly', {
    get: function () {
      return this.pattern === 'readOnly';
    },
    set: function (readOnly) {
      if (!isValid(readOnly)) return;
      if (readOnly) {
        this.pattern = 'readOnly';
      } else {
        this.pattern = 'editable';
      }
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(Field.prototype, 'readPretty', {
    get: function () {
      return this.pattern === 'readPretty';
    },
    set: function (readPretty) {
      if (!isValid(readPretty)) return;
      if (readPretty) {
        this.pattern = 'readPretty';
      } else {
        this.pattern = 'editable';
      }
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(Field.prototype, 'editable', {
    get: function () {
      return this.pattern === 'editable';
    },
    set: function (editable) {
      if (!isValid(editable)) return;
      if (editable) {
        this.pattern = 'editable';
      } else {
        this.pattern = 'readPretty';
      }
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(Field.prototype, 'validateStatus', {
    get: function () {
      if (this.validating) return 'validating';
      if (this.invalid) return 'error';
      if (this.warnings.length) return 'warning';
      if (this.successes.length) return 'success';
    },
    enumerable: false,
    configurable: true,
  });
  return Field;
})();
export { Field };
//# sourceMappingURL=Field.js.map
