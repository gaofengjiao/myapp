var __assign =
  (this && this.__assign) ||
  function () {
    __assign =
      Object.assign ||
      function (t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
      };
    return __assign.apply(this, arguments);
  };
var __awaiter =
  (this && this.__awaiter) ||
  function (thisArg, _arguments, P, generator) {
    function adopt(value) {
      return value instanceof P
        ? value
        : new P(function (resolve) {
            resolve(value);
          });
    }
    return new (P || (P = Promise))(function (resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
var __generator =
  (this && this.__generator) ||
  function (thisArg, body) {
    var _ = {
        label: 0,
        sent: function () {
          if (t[0] & 1) throw t[1];
          return t[1];
        },
        trys: [],
        ops: [],
      },
      f,
      y,
      t,
      g;
    return (
      (g = { next: verb(0), throw: verb(1), return: verb(2) }),
      typeof Symbol === 'function' &&
        (g[Symbol.iterator] = function () {
          return this;
        }),
      g
    );
    function verb(n) {
      return function (v) {
        return step([n, v]);
      };
    }
    function step(op) {
      if (f) throw new TypeError('Generator is already executing.');
      while (_)
        try {
          if (
            ((f = 1),
            y &&
              (t =
                op[0] & 2
                  ? y['return']
                  : op[0]
                  ? y['throw'] || ((t = y['return']) && t.call(y), 0)
                  : y.next) &&
              !(t = t.call(y, op[1])).done)
          )
            return t;
          if (((y = 0), t)) op = [op[0] & 2, t.value];
          switch (op[0]) {
            case 0:
            case 1:
              t = op;
              break;
            case 4:
              _.label++;
              return { value: op[1], done: false };
            case 5:
              _.label++;
              y = op[1];
              op = [0];
              continue;
            case 7:
              op = _.ops.pop();
              _.trys.pop();
              continue;
            default:
              if (
                !((t = _.trys), (t = t.length > 0 && t[t.length - 1])) &&
                (op[0] === 6 || op[0] === 2)
              ) {
                _ = 0;
                continue;
              }
              if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) {
                _.label = op[1];
                break;
              }
              if (op[0] === 6 && _.label < t[1]) {
                _.label = t[1];
                t = op;
                break;
              }
              if (t && _.label < t[2]) {
                _.label = t[2];
                _.ops.push(op);
                break;
              }
              if (t[2]) _.ops.pop();
              _.trys.pop();
              continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [6, e];
          y = 0;
        } finally {
          f = t = 0;
        }
      if (op[0] & 5) throw op[1];
      return { value: op[0] ? op[1] : void 0, done: true };
    }
  };
import {
  define,
  observable,
  batch,
  action,
  toJS,
  isObservable,
  observe,
} from '@/pages/design/visualSource/@formily/reactive';
import {
  FormPath,
  isFn,
  isValid,
  uid,
  globalThisPolyfill,
  merge,
  clone,
  isPlainObj,
  isArr,
  isObj,
} from '@/pages/design/visualSource/@formily/shared';
import { Heart } from './Heart';
import { Field } from './Field';
import { LifeCycleTypes } from '../types';
import {
  modelStateGetter,
  modelStateSetter,
  createFieldStateSetter,
  createFieldStateGetter,
  applyValuesPatch,
  triggerFormInitialValuesChange,
  triggerFormValuesChange,
} from '../shared/internals';
import { isVoidField } from '../shared/checkers';
import { runEffects } from '../shared/effectbox';
import { ArrayField } from './ArrayField';
import { ObjectField } from './ObjectField';
import { VoidField } from './VoidField';
import { Query } from './Query';
import { Graph } from './Graph';
var DEV_TOOLS_HOOK = '__FORMILY_DEV_TOOLS_HOOK__';
var RESPONSE_REQUEST_DURATION = 100;
var Form = /** @class */ (function () {
  function Form(props) {
    var _this = this;
    this.displayName = 'Form';
    this.fields = {};
    this.requests = {};
    this.indexes = new Map();
    this.disposers = [];
    /** 创建字段 **/
    this.createField = function (props) {
      var address = FormPath.parse(props.basePath).concat(props.name);
      var identifier = address.toString();
      if (!identifier) return;
      if (!_this.fields[identifier] || _this.props.designable) {
        batch(function () {
          _this.fields[identifier] = new Field(address, props, _this, _this.props.designable);
        });
        _this.notify(LifeCycleTypes.ON_FORM_GRAPH_CHANGE);
      }
      return _this.fields[identifier];
    };
    this.createArrayField = function (props) {
      var address = FormPath.parse(props.basePath).concat(props.name);
      var identifier = address.toString();
      if (!identifier) return;
      if (!_this.fields[identifier] || _this.props.designable) {
        batch(function () {
          _this.fields[identifier] = new ArrayField(
            address,
            __assign(__assign({}, props), { value: isArr(props.value) ? props.value : [] }),
            _this,
            _this.props.designable,
          );
        });
        _this.notify(LifeCycleTypes.ON_FORM_GRAPH_CHANGE);
      }
      return _this.fields[identifier];
    };
    this.createObjectField = function (props) {
      var address = FormPath.parse(props.basePath).concat(props.name);
      var identifier = address.toString();
      if (!identifier) return;
      if (!_this.fields[identifier] || _this.props.designable) {
        batch(function () {
          _this.fields[identifier] = new ObjectField(
            address,
            __assign(__assign({}, props), { value: isObj(props.value) ? props.value : {} }),
            _this,
            _this.props.designable,
          );
        });
        _this.notify(LifeCycleTypes.ON_FORM_GRAPH_CHANGE);
      }
      return _this.fields[identifier];
    };
    this.createVoidField = function (props) {
      var address = FormPath.parse(props.basePath).concat(props.name);
      var identifier = address.toString();
      if (!identifier) return;
      if (!_this.fields[identifier] || _this.props.designable) {
        batch(function () {
          _this.fields[identifier] = new VoidField(address, props, _this, _this.props.designable);
        });
        _this.notify(LifeCycleTypes.ON_FORM_GRAPH_CHANGE);
      }
      return _this.fields[identifier];
    };
    /** 状态操作模型 **/
    this.setValues = function (values, strategy) {
      if (strategy === void 0) {
        strategy = 'merge';
      }
      if (!isPlainObj(values)) return;
      if (strategy === 'merge' || strategy === 'deepMerge') {
        _this.values = merge(_this.values, values, {
          arrayMerge: function (target, source) {
            return source;
          },
        });
      } else if (strategy === 'shallowMerge') {
        _this.values = Object.assign(_this.values, values);
      } else {
        _this.values = values;
      }
    };
    this.setInitialValues = function (initialValues, strategy) {
      if (strategy === void 0) {
        strategy = 'merge';
      }
      if (!isPlainObj(initialValues)) return;
      if (strategy === 'merge' || strategy === 'deepMerge') {
        _this.initialValues = merge(_this.initialValues, initialValues, {
          arrayMerge: function (target, source) {
            return source;
          },
        });
      } else if (strategy === 'shallowMerge') {
        _this.initialValues = Object.assign(_this.initialValues, initialValues);
      } else {
        _this.initialValues = initialValues;
      }
    };
    this.setValuesIn = function (pattern, value) {
      FormPath.setIn(_this.values, pattern, value);
    };
    this.deleteValuesIn = function (pattern) {
      FormPath.deleteIn(_this.values, pattern);
    };
    this.existValuesIn = function (pattern) {
      return FormPath.existIn(_this.values, pattern);
    };
    this.getValuesIn = function (pattern) {
      return FormPath.getIn(_this.values, pattern);
    };
    this.setInitialValuesIn = function (pattern, initialValue) {
      FormPath.setIn(_this.initialValues, pattern, initialValue);
    };
    this.deleteInitialValuesIn = function (pattern) {
      FormPath.deleteIn(_this.initialValues, pattern);
    };
    this.existInitialValuesIn = function (pattern) {
      return FormPath.existIn(_this.initialValues, pattern);
    };
    this.getInitialValuesIn = function (pattern) {
      return FormPath.getIn(_this.initialValues, pattern);
    };
    this.setSubmitting = function (submitting) {
      clearTimeout(_this.requests.submit);
      if (submitting) {
        _this.requests.submit = setTimeout(function () {
          batch(function () {
            _this.submitting = submitting;
            _this.notify(LifeCycleTypes.ON_FORM_SUBMITTING);
          });
        }, RESPONSE_REQUEST_DURATION);
        _this.notify(LifeCycleTypes.ON_FORM_SUBMIT_START);
      } else {
        if (_this.submitting !== submitting) {
          _this.submitting = submitting;
        }
        _this.notify(LifeCycleTypes.ON_FORM_SUBMIT_END);
      }
    };
    this.setValidating = function (validating) {
      clearTimeout(_this.requests.validate);
      if (validating) {
        _this.requests.validate = setTimeout(function () {
          batch(function () {
            _this.validating = validating;
            _this.notify(LifeCycleTypes.ON_FORM_VALIDATING);
          });
        }, RESPONSE_REQUEST_DURATION);
        _this.notify(LifeCycleTypes.ON_FORM_VALIDATE_START);
      } else {
        if (_this.validating !== validating) {
          _this.validating = validating;
        }
        _this.notify(LifeCycleTypes.ON_FORM_VALIDATE_END);
      }
    };
    this.setDisplay = function (display) {
      _this.display = display;
    };
    this.setPattern = function (pattern) {
      _this.pattern = pattern;
    };
    this.addEffects = function (id, effects) {
      if (!_this.heart.hasLifeCycles(id)) {
        _this.heart.addLifeCycles(id, runEffects(_this, effects));
      }
    };
    this.removeEffects = function (id) {
      _this.heart.removeLifeCycles(id);
    };
    this.setEffects = function (effects) {
      _this.heart.setLifeCycles(runEffects(_this, effects));
    };
    this.clearErrors = function (pattern) {
      if (pattern === void 0) {
        pattern = '*';
      }
      _this.query(pattern).forEach(function (field) {
        if (!isVoidField(field)) {
          field.setFeedback({
            type: 'error',
            messages: [],
          });
        }
      });
    };
    this.clearWarnings = function (pattern) {
      if (pattern === void 0) {
        pattern = '*';
      }
      _this.query(pattern).forEach(function (field) {
        if (!isVoidField(field)) {
          field.setFeedback({
            type: 'warning',
            messages: [],
          });
        }
      });
    };
    this.clearSuccesses = function (pattern) {
      if (pattern === void 0) {
        pattern = '*';
      }
      _this.query(pattern).forEach(function (field) {
        if (!isVoidField(field)) {
          field.setFeedback({
            type: 'success',
            messages: [],
          });
        }
      });
    };
    this.query = function (pattern) {
      return new Query({
        pattern: pattern,
        base: '',
        form: _this,
      });
    };
    this.queryFeedbacks = function (search) {
      return _this.query(search.address || search.path || '*').reduce(function (messages, field) {
        if (isVoidField(field)) return messages;
        return messages.concat(
          field
            .queryFeedbacks(search)
            .map(function (feedback) {
              return __assign(__assign({}, feedback), {
                address: field.address.toString(),
                path: field.path.toString(),
              });
            })
            .filter(function (feedback) {
              return feedback.messages.length > 0;
            }),
        );
      }, []);
    };
    this.notify = function (type, payload) {
      _this.heart.publish(type, isValid(payload) ? payload : _this);
    };
    this.subscribe = function (subscriber) {
      return _this.heart.subscribe(subscriber);
    };
    this.unsubscribe = function (id) {
      _this.heart.unsubscribe(id);
    };
    /**事件钩子**/
    this.onInit = function () {
      _this.initialized = true;
      _this.notify(LifeCycleTypes.ON_FORM_INIT);
    };
    this.onMount = function () {
      _this.mounted = true;
      _this.notify(LifeCycleTypes.ON_FORM_MOUNT);
      if (globalThisPolyfill[DEV_TOOLS_HOOK] && !_this.props.designable) {
        globalThisPolyfill[DEV_TOOLS_HOOK].inject(_this.id, _this);
      }
    };
    this.onUnmount = function () {
      _this.notify(LifeCycleTypes.ON_FORM_UNMOUNT);
      _this.query('*').forEach(function (field) {
        return field.destroy();
      });
      _this.disposers.forEach(function (dispose) {
        return dispose();
      });
      _this.unmounted = true;
      _this.indexes.clear();
      _this.heart.clear();
      if (globalThisPolyfill[DEV_TOOLS_HOOK] && !_this.props.designable) {
        globalThisPolyfill[DEV_TOOLS_HOOK].unmount(_this.id);
      }
    };
    this.setState = modelStateSetter(this);
    this.getState = modelStateGetter(this);
    this.setFormState = modelStateSetter(this);
    this.getFormState = modelStateGetter(this);
    this.setFieldState = createFieldStateSetter(this);
    this.getFieldState = createFieldStateGetter(this);
    this.getFormGraph = function () {
      return _this.graph.getGraph();
    };
    this.setFormGraph = function (graph) {
      _this.graph.setGraph(graph);
    };
    this.clearFormGraph = function (pattern) {
      if (pattern === void 0) {
        pattern = '*';
      }
      _this.query(pattern).forEach(function (field) {
        field.destroy();
      });
    };
    this.validate = function (pattern) {
      if (pattern === void 0) {
        pattern = '*';
      }
      return __awaiter(_this, void 0, void 0, function () {
        var tasks;
        return __generator(this, function (_a) {
          switch (_a.label) {
            case 0:
              this.setValidating(true);
              tasks = [];
              this.query(pattern).forEach(function (field) {
                if (!isVoidField(field)) {
                  tasks.push(field.validate());
                }
              });
              return [4 /*yield*/, Promise.all(tasks)];
            case 1:
              _a.sent();
              this.setValidating(false);
              if (this.invalid) {
                this.notify(LifeCycleTypes.ON_FORM_VALIDATE_FAILED);
                throw this.errors;
              }
              this.notify(LifeCycleTypes.ON_FORM_VALIDATE_SUCCESS);
              return [2 /*return*/];
          }
        });
      });
    };
    this.submit = function (onSubmit) {
      return __awaiter(_this, void 0, void 0, function () {
        var e_1, results, e_2;
        return __generator(this, function (_a) {
          switch (_a.label) {
            case 0:
              this.setSubmitting(true);
              _a.label = 1;
            case 1:
              _a.trys.push([1, 3, , 4]);
              this.notify(LifeCycleTypes.ON_FORM_SUBMIT_VALIDATE_START);
              return [4 /*yield*/, this.validate()];
            case 2:
              _a.sent();
              this.notify(LifeCycleTypes.ON_FORM_SUBMIT_VALIDATE_SUCCESS);
              return [3 /*break*/, 4];
            case 3:
              e_1 = _a.sent();
              this.notify(LifeCycleTypes.ON_FORM_SUBMIT_VALIDATE_FAILED);
              return [3 /*break*/, 4];
            case 4:
              this.notify(LifeCycleTypes.ON_FORM_SUBMIT_VALIDATE_END);
              _a.label = 5;
            case 5:
              _a.trys.push([5, 9, , 10]);
              if (this.invalid) {
                throw this.errors;
              }
              if (!isFn(onSubmit)) return [3 /*break*/, 7];
              return [4 /*yield*/, onSubmit(toJS(this.values))];
            case 6:
              results = _a.sent();
              return [3 /*break*/, 8];
            case 7:
              results = toJS(this.values);
              _a.label = 8;
            case 8:
              this.notify(LifeCycleTypes.ON_FORM_SUBMIT_SUCCESS);
              return [3 /*break*/, 10];
            case 9:
              e_2 = _a.sent();
              this.setSubmitting(false);
              this.notify(LifeCycleTypes.ON_FORM_SUBMIT_FAILED);
              this.notify(LifeCycleTypes.ON_FORM_SUBMIT);
              throw e_2;
            case 10:
              this.setSubmitting(false);
              this.notify(LifeCycleTypes.ON_FORM_SUBMIT);
              return [2 /*return*/, results];
          }
        });
      });
    };
    this.reset = function (pattern, options) {
      if (pattern === void 0) {
        pattern = '*';
      }
      return __awaiter(_this, void 0, void 0, function () {
        var tasks;
        return __generator(this, function (_a) {
          switch (_a.label) {
            case 0:
              tasks = [];
              this.query(pattern).forEach(function (field) {
                if (!isVoidField(field)) {
                  tasks.push(field.reset(options));
                }
              });
              this.notify(LifeCycleTypes.ON_FORM_RESET);
              return [4 /*yield*/, Promise.all(tasks)];
            case 1:
              _a.sent();
              return [2 /*return*/];
          }
        });
      });
    };
    this.initialize(props);
    this.makeInitialValues();
    this.makeObservable();
    this.makeReactive();
    this.onInit();
  }
  Form.prototype.initialize = function (props) {
    this.id = uid();
    this.props = __assign({}, props);
    this.initialized = false;
    this.submitting = false;
    this.validating = false;
    this.modified = false;
    this.mounted = false;
    this.unmounted = false;
    this.display = this.props.display || 'visible';
    this.pattern = this.props.pattern || 'editable';
    this.editable = this.props.editable;
    this.disabled = this.props.disabled;
    this.readOnly = this.props.readOnly;
    this.readPretty = this.props.readPretty;
    this.visible = this.props.visible;
    this.hidden = this.props.hidden;
    this.graph = new Graph(this);
    this.heart = new Heart({
      lifecycles: this.lifecycles,
      context: this,
    });
  };
  Form.prototype.makeInitialValues = function () {
    this.values = isObservable(this.props.values)
      ? this.props.values
      : clone(this.props.values) || {};
    this.initialValues = isObservable(this.props.initialValues)
      ? this.props.initialValues
      : clone(this.props.initialValues) || {};
    applyValuesPatch(this, [], this.initialValues);
  };
  Form.prototype.makeObservable = function () {
    define(this, {
      fields: observable.shallow,
      initialized: observable.ref,
      validating: observable.ref,
      submitting: observable.ref,
      modified: observable.ref,
      pattern: observable.ref,
      display: observable.ref,
      mounted: observable.ref,
      unmounted: observable.ref,
      values: observable,
      initialValues: observable,
      valid: observable.computed,
      invalid: observable.computed,
      errors: observable.computed,
      warnings: observable.computed,
      successes: observable.computed,
      hidden: observable.computed,
      visible: observable.computed,
      editable: observable.computed,
      readOnly: observable.computed,
      readPretty: observable.computed,
      disabled: observable.computed,
      setValues: action,
      setValuesIn: action,
      setInitialValues: action,
      setInitialValuesIn: action,
      setPattern: action,
      setDisplay: action,
      setState: action,
      deleteInitialValuesIn: action,
      deleteValuesIn: action,
      setSubmitting: action,
      setValidating: action,
      setFormGraph: action,
      clearFormGraph: action,
      reset: action,
      submit: action,
      validate: action,
      onMount: batch,
      onUnmount: batch,
      onInit: batch,
    });
  };
  Form.prototype.makeReactive = function () {
    var _this = this;
    if (this.props.designable) return;
    this.disposers.push(
      observe(
        this,
        function (change) {
          triggerFormInitialValuesChange(_this, change);
          triggerFormValuesChange(_this, change);
        },
        true,
      ),
    );
  };
  Object.defineProperty(Form.prototype, 'valid', {
    get: function () {
      return !this.invalid;
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(Form.prototype, 'invalid', {
    get: function () {
      return this.errors.length > 0;
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(Form.prototype, 'errors', {
    get: function () {
      return this.queryFeedbacks({
        type: 'error',
      });
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(Form.prototype, 'warnings', {
    get: function () {
      return this.queryFeedbacks({
        type: 'warning',
      });
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(Form.prototype, 'successes', {
    get: function () {
      return this.queryFeedbacks({
        type: 'success',
      });
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(Form.prototype, 'lifecycles', {
    get: function () {
      return runEffects(this, this.props.effects);
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(Form.prototype, 'hidden', {
    get: function () {
      return this.display === 'hidden';
    },
    set: function (hidden) {
      if (!isValid(hidden)) return;
      if (hidden) {
        this.display = 'hidden';
      } else {
        this.display = 'visible';
      }
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(Form.prototype, 'visible', {
    get: function () {
      return this.display === 'visible';
    },
    set: function (visible) {
      if (!isValid(visible)) return;
      if (visible) {
        this.display = 'visible';
      } else {
        this.display = 'none';
      }
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(Form.prototype, 'editable', {
    get: function () {
      return this.pattern === 'editable';
    },
    set: function (editable) {
      if (!isValid(editable)) return;
      if (editable) {
        this.pattern = 'editable';
      } else {
        this.pattern = 'readPretty';
      }
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(Form.prototype, 'readOnly', {
    get: function () {
      return this.pattern === 'readOnly';
    },
    set: function (readOnly) {
      if (!isValid(readOnly)) return;
      if (readOnly) {
        this.pattern = 'readOnly';
      } else {
        this.pattern = 'editable';
      }
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(Form.prototype, 'disabled', {
    get: function () {
      return this.pattern === 'disabled';
    },
    set: function (disabled) {
      if (!isValid(disabled)) return;
      if (disabled) {
        this.pattern = 'disabled';
      } else {
        this.pattern = 'editable';
      }
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(Form.prototype, 'readPretty', {
    get: function () {
      return this.pattern === 'readPretty';
    },
    set: function (readPretty) {
      if (!isValid(readPretty)) return;
      if (readPretty) {
        this.pattern = 'readPretty';
      } else {
        this.pattern = 'editable';
      }
    },
    enumerable: false,
    configurable: true,
  });
  return Form;
})();
export { Form };
//# sourceMappingURL=Form.js.map
