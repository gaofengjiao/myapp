import { FormPath, isFn, each } from '@/pages/design/visualSource/@formily/shared';
var output = function (field, taker) {
  if (!field) return;
  if (isFn(taker)) {
    return taker(field, field.address);
  }
  return field;
};
var Query = /** @class */ (function () {
  function Query(props) {
    var _this = this;
    this.addresses = [];
    this.pattern = FormPath.parse(props.pattern, props.base);
    this.form = props.form;
    if (!this.pattern.isMatchPattern) {
      var identifier = this.pattern.toString();
      var index = this.form.indexes.get(identifier);
      var absoluteField = this.form.fields[identifier];
      var indexField = this.form.fields[index];
      if (absoluteField) {
        this.addresses = [identifier];
      } else if (indexField) {
        this.addresses = [index];
      }
    } else {
      each(this.form.fields, function (field, address) {
        if (field.match(_this.pattern)) {
          _this.addresses.push(address);
        }
      });
    }
  }
  Query.prototype.take = function (taker) {
    return output(this.form.fields[this.addresses[0]], taker);
  };
  Query.prototype.map = function (mapper) {
    var _this = this;
    return this.addresses.map(function (address) {
      return output(_this.form.fields[address], mapper);
    });
  };
  Query.prototype.forEach = function (eacher) {
    var _this = this;
    return this.addresses.forEach(function (address) {
      return output(_this.form.fields[address], eacher);
    });
  };
  Query.prototype.reduce = function (reducer, initial) {
    var _this = this;
    return this.addresses.reduce(function (value, address) {
      return output(_this.form.fields[address], function (field, address) {
        return reducer(value, field, address);
      });
    }, initial);
  };
  Query.prototype.get = function (key) {
    var results = this.take();
    if (results) {
      return results[key];
    }
  };
  Query.prototype.getIn = function (pattern) {
    return FormPath.getIn(this.take(), pattern);
  };
  Query.prototype.value = function () {
    return this.form.getValuesIn(this.pattern);
  };
  Query.prototype.initialValue = function () {
    return this.form.getInitialValuesIn(this.pattern);
  };
  return Query;
})();
export { Query };
//# sourceMappingURL=Query.js.map
