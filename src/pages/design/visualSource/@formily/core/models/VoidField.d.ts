import { FormPath, FormPathPattern } from '@/pages/design/visualSource/@formily/shared';
import {
  FieldDisplayTypes,
  FieldPatternTypes,
  FieldDecorator,
  FieldComponent,
  IVoidFieldProps,
  FormPatternTypes,
  IVoidFieldState,
  IModelSetter,
  IModelGetter,
} from '../types';
import { Form } from './Form';
import { Query } from './Query';
export declare class VoidField<Decorator = any, Component = any, TextType = any> {
  displayName: string;
  title: TextType;
  description: TextType;
  selfDisplay: FieldDisplayTypes;
  selfPattern: FieldPatternTypes;
  initialized: boolean;
  mounted: boolean;
  unmounted: boolean;
  decoratorType: Decorator;
  decoratorProps: Record<string, any>;
  componentType: Component;
  componentProps: Record<string, any>;
  address: FormPath;
  path: FormPath;
  form: Form;
  props: IVoidFieldProps<Decorator, Component>;
  protected disposers: (() => void)[];
  constructor(
    address: FormPathPattern,
    props: IVoidFieldProps<Decorator, Component>,
    form: Form,
    designable: boolean,
  );
  protected makeIndexes(address: FormPathPattern): void;
  protected initialize(props: IVoidFieldProps<Decorator, Component>, form: Form): void;
  protected makeObservable(designable: boolean): void;
  protected makeReactive(designable: boolean): void;
  get parent(): import('../types').GeneralField;
  get component(): FieldComponent<Component>;
  set component(value: FieldComponent<Component>);
  get decorator(): FieldDecorator<Decorator>;
  set decorator(value: FieldDecorator<Decorator>);
  get display(): FieldDisplayTypes;
  get pattern(): FormPatternTypes;
  get editable(): boolean;
  get disabled(): boolean;
  get readOnly(): boolean;
  get readPretty(): boolean;
  get hidden(): boolean;
  get visible(): boolean;
  set hidden(hidden: boolean);
  set visible(visible: boolean);
  set editable(editable: boolean);
  set readOnly(readOnly: boolean);
  set disabled(disabled: boolean);
  set readPretty(readPretty: boolean);
  set pattern(pattern: FieldPatternTypes);
  set display(display: FieldDisplayTypes);
  setTitle: (title?: TextType) => void;
  setDescription: (description?: TextType) => void;
  setDisplay: (type?: FieldDisplayTypes) => void;
  setPattern: (type?: FieldPatternTypes) => void;
  setComponent: <C extends unknown, ComponentProps extends object = {}>(
    component?: C,
    props?: ComponentProps,
  ) => void;
  setComponentProps: <ComponentProps extends object = {}>(props?: ComponentProps) => void;
  setDecorator: <D extends unknown, ComponentProps extends object = {}>(
    component?: D,
    props?: ComponentProps,
  ) => void;
  setDecoratorProps: <ComponentProps extends object = {}>(props?: ComponentProps) => void;
  setState: IModelSetter<IVoidFieldState>;
  getState: IModelGetter<IVoidFieldState>;
  onInit: () => void;
  onMount: () => void;
  onUnmount: () => void;
  query: (pattern: FormPathPattern | RegExp) => Query;
  dispose: () => void;
  destroy: () => void;
  match: (pattern: FormPathPattern) => boolean;
}
