import { FormPath, isFn, isValid, toArr } from '@/pages/design/visualSource/@formily/shared';
import {
  define,
  observable,
  autorun,
  batch,
  action,
} from '@/pages/design/visualSource/@formily/reactive';
import { LifeCycleTypes } from '../types';
import {
  buildNodeIndexes,
  modelStateGetter,
  modelStateSetter,
  initFieldUpdate,
} from '../shared/internals';
import { Query } from './Query';
var VoidField = /** @class */ (function () {
  function VoidField(address, props, form, designable) {
    var _this = this;
    this.displayName = 'VoidField';
    this.disposers = [];
    this.setTitle = function (title) {
      _this.title = title;
    };
    this.setDescription = function (description) {
      _this.description = description;
    };
    this.setDisplay = function (type) {
      _this.display = type;
    };
    this.setPattern = function (type) {
      _this.pattern = type;
    };
    this.setComponent = function (component, props) {
      if (component) {
        _this.componentType = component;
      }
      if (props) {
        _this.componentProps = _this.componentProps || {};
        Object.assign(_this.componentProps, props);
      }
    };
    this.setComponentProps = function (props) {
      if (props) {
        _this.componentProps = _this.componentProps || {};
        Object.assign(_this.componentProps, props);
      }
    };
    this.setDecorator = function (component, props) {
      if (component) {
        _this.decoratorType = component;
      }
      if (props) {
        _this.decoratorProps = _this.decoratorProps || {};
        Object.assign(_this.decoratorProps, props);
      }
    };
    this.setDecoratorProps = function (props) {
      if (props) {
        _this.decoratorProps = _this.decoratorProps || {};
        Object.assign(_this.decoratorProps, props);
      }
    };
    this.setState = modelStateSetter(this);
    this.getState = modelStateGetter(this);
    this.onInit = function () {
      _this.initialized = true;
      batch.scope(function () {
        initFieldUpdate(_this);
      });
      _this.form.notify(LifeCycleTypes.ON_FIELD_INIT, _this);
    };
    this.onMount = function () {
      _this.mounted = true;
      _this.unmounted = false;
      _this.form.notify(LifeCycleTypes.ON_FIELD_MOUNT, _this);
    };
    this.onUnmount = function () {
      _this.mounted = false;
      _this.unmounted = true;
      _this.form.notify(LifeCycleTypes.ON_FIELD_UNMOUNT, _this);
    };
    this.query = function (pattern) {
      return new Query({
        pattern: pattern,
        base: _this.address,
        form: _this.form,
      });
    };
    this.dispose = function () {
      _this.disposers.forEach(function (dispose) {
        dispose();
      });
      _this.form.removeEffects(_this);
    };
    this.destroy = function () {
      _this.dispose();
      delete _this.form.fields[_this.address.toString()];
    };
    this.match = function (pattern) {
      return FormPath.parse(pattern).matchAliasGroup(_this.address, _this.path);
    };
    this.initialize(props, form);
    this.makeIndexes(address);
    this.makeObservable(designable);
    this.makeReactive(designable);
    this.onInit();
  }
  VoidField.prototype.makeIndexes = function (address) {
    buildNodeIndexes(this, address);
  };
  VoidField.prototype.initialize = function (props, form) {
    this.form = form;
    this.props = props;
    this.mounted = false;
    this.unmounted = false;
    this.initialized = false;
    this.title = props.title;
    this.description = props.description;
    this.pattern = this.props.pattern;
    this.display = props.display;
    this.hidden = this.props.hidden;
    this.editable = this.props.editable;
    this.disabled = this.props.disabled;
    this.readOnly = this.props.readOnly;
    this.readPretty = this.props.readPretty;
    this.visible = this.props.visible;
    this.decorator = toArr(this.props.decorator);
    this.component = toArr(this.props.component);
  };
  VoidField.prototype.makeObservable = function (designable) {
    if (designable) return;
    define(this, {
      title: observable.ref,
      description: observable.ref,
      selfDisplay: observable.ref,
      selfPattern: observable.ref,
      initialized: observable.ref,
      mounted: observable.ref,
      unmounted: observable.ref,
      decoratorType: observable.ref,
      componentType: observable.ref,
      decoratorProps: observable,
      componentProps: observable,
      display: observable.computed,
      pattern: observable.computed,
      hidden: observable.computed,
      visible: observable.computed,
      disabled: observable.computed,
      readOnly: observable.computed,
      readPretty: observable.computed,
      editable: observable.computed,
      component: observable.computed,
      decorator: observable.computed,
      setTitle: action,
      setDescription: action,
      setDisplay: action,
      setPattern: action,
      setComponent: action,
      setComponentProps: action,
      setDecorator: action,
      setDecoratorProps: action,
      onInit: batch,
      onMount: batch,
      onUnmount: batch,
    });
  };
  VoidField.prototype.makeReactive = function (designable) {
    var _this = this;
    if (designable) return;
    this.form.addEffects(this, function () {
      toArr(_this.props.reactions).forEach(function (reaction) {
        if (isFn(reaction)) {
          _this.disposers.push(
            autorun(function () {
              return reaction(_this);
            }),
          );
        }
      });
    });
  };
  Object.defineProperty(VoidField.prototype, 'parent', {
    get: function () {
      var parent = this.address.parent();
      var identifier = parent.toString();
      while (!this.form.fields[identifier]) {
        parent = parent.parent();
        identifier = parent.toString();
        if (!identifier) return;
      }
      return this.form.fields[identifier];
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(VoidField.prototype, 'component', {
    get: function () {
      return [this.componentType, this.componentProps];
    },
    set: function (value) {
      var component = toArr(value);
      this.componentType = component[0];
      this.componentProps = component[1] || {};
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(VoidField.prototype, 'decorator', {
    get: function () {
      return [this.decoratorType, this.decoratorProps];
    },
    set: function (value) {
      var decorator = toArr(value);
      this.decoratorType = decorator[0];
      this.decoratorProps = decorator[1] || {};
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(VoidField.prototype, 'display', {
    get: function () {
      var _a;
      var parentDisplay = (_a = this.parent) === null || _a === void 0 ? void 0 : _a.display;
      if (parentDisplay && parentDisplay !== 'visible') {
        if (this.selfDisplay && this.selfDisplay !== 'visible') return this.selfDisplay;
        return parentDisplay;
      }
      if (isValid(this.selfDisplay)) return this.selfDisplay;
      return parentDisplay || this.form.display || 'visible';
    },
    set: function (display) {
      this.selfDisplay = display;
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(VoidField.prototype, 'pattern', {
    get: function () {
      var _a;
      var parentPattern = (_a = this.parent) === null || _a === void 0 ? void 0 : _a.pattern;
      if (isValid(this.selfPattern)) return this.selfPattern;
      return parentPattern || this.form.pattern || 'editable';
    },
    set: function (pattern) {
      this.selfPattern = pattern;
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(VoidField.prototype, 'editable', {
    get: function () {
      return this.pattern === 'editable';
    },
    set: function (editable) {
      if (!isValid(editable)) return;
      if (editable) {
        this.pattern = 'editable';
      } else {
        this.pattern = 'readPretty';
      }
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(VoidField.prototype, 'disabled', {
    get: function () {
      return this.pattern === 'disabled';
    },
    set: function (disabled) {
      if (!isValid(disabled)) return;
      if (disabled) {
        this.pattern = 'disabled';
      } else {
        this.pattern = 'editable';
      }
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(VoidField.prototype, 'readOnly', {
    get: function () {
      return this.pattern === 'readOnly';
    },
    set: function (readOnly) {
      if (!isValid(readOnly)) return;
      if (readOnly) {
        this.pattern = 'readOnly';
      } else {
        this.pattern = 'editable';
      }
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(VoidField.prototype, 'readPretty', {
    get: function () {
      return this.pattern === 'readPretty';
    },
    set: function (readPretty) {
      if (!isValid(readPretty)) return;
      if (readPretty) {
        this.pattern = 'readPretty';
      } else {
        this.pattern = 'editable';
      }
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(VoidField.prototype, 'hidden', {
    get: function () {
      return this.display === 'hidden';
    },
    set: function (hidden) {
      if (!isValid(hidden)) return;
      if (hidden) {
        this.display = 'hidden';
      } else {
        this.display = 'visible';
      }
    },
    enumerable: false,
    configurable: true,
  });
  Object.defineProperty(VoidField.prototype, 'visible', {
    get: function () {
      return this.display === 'visible';
    },
    set: function (visible) {
      if (!isValid(visible)) return;
      if (visible) {
        this.display = 'visible';
      } else {
        this.display = 'none';
      }
    },
    enumerable: false,
    configurable: true,
  });
  return VoidField;
})();
export { VoidField };
//# sourceMappingURL=VoidField.js.map
