export declare const ReservedProperties: string[];
export declare const GlobalState: {
  initializing: boolean;
  lifecycles: any[];
  context: any[];
  effectStart: boolean;
  effectEnd: boolean;
};
