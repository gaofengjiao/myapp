export var ReservedProperties = [
  'form',
  'parent',
  'props',
  'caches',
  'requests',
  'disposers',
  'heart',
  'graph',
  'indexes',
  'fields',
  'lifecycles',
  'originValues',
  'componentType',
  'componentProps',
  'decoratorType',
  'decoratorProps',
];
export var GlobalState = {
  initializing: false,
  lifecycles: [],
  context: [],
  effectStart: false,
  effectEnd: false,
};
//# sourceMappingURL=constants.js.map
