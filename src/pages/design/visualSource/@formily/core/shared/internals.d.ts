import { FormPath, FormPathPattern } from '@/pages/design/visualSource/@formily/shared';
import { ValidatorTriggerType } from '@/pages/design/visualSource/@formily/validator';
import { DataChange } from '@/pages/design/visualSource/@formily/reactive';
import { Field, ArrayField, Form, ObjectField } from '../models';
import {
  ISpliceArrayStateProps,
  IExchangeArrayStateProps,
  ISearchFeedback,
  IFieldFeedback,
  INodePatch,
  GeneralField,
  IFormFeedback,
  FieldMatchPattern,
} from '../types';
export declare const isHTMLInputEvent: (event: any, stopPropagation?: boolean) => boolean;
export declare const getValuesFromEvent: (args: any[]) => any[];
export declare const buildFieldPath: (field: GeneralField) => FormPath;
export declare const buildNodeIndexes: (
  field: GeneralField,
  address: FormPathPattern,
) => GeneralField;
export declare const applyFieldPatches: (
  target: Record<string, GeneralField>,
  patches: INodePatch<GeneralField>[],
) => void;
export declare const matchFeedback: (search?: ISearchFeedback, feedback?: IFormFeedback) => boolean;
export declare const queryFeedbacks: (field: Field, search?: ISearchFeedback) => IFieldFeedback[];
export declare const queryFeedbackMessages: (field: Field, search: ISearchFeedback) => any[];
export declare const updateFeedback: (field: Field, feedback?: IFieldFeedback) => void;
export declare const validateToFeedbacks: (
  field: Field,
  triggerType?: ValidatorTriggerType,
) => Promise<import('@/pages/design/visualSource/@formily/validator').IValidateResults>;
export declare const spliceArrayState: (field: ArrayField, props?: ISpliceArrayStateProps) => void;
export declare const exchangeArrayState: (
  field: ArrayField,
  props: IExchangeArrayStateProps,
) => void;
export declare const cleanupArrayChildren: (field: ArrayField, start: number) => void;
export declare const cleanupObjectChildren: (field: ObjectField, keys: string[]) => void;
export declare const initFieldValue: (field: Field, designable: boolean) => void;
export declare const initFieldUpdate: (field: GeneralField) => void;
export declare const subscribeUpdate: (
  form: Form,
  pattern: FormPath,
  callback: (...args: any[]) => void,
) => void;
export declare const setModelState: (model: any, setter: any) => any;
export declare const getModelState: (model: any, getter?: any) => any;
export declare const modelStateSetter: (model: any) => (state?: any) => any;
export declare const modelStateGetter: (model: any) => (getter?: any) => any;
export declare const createFieldStateSetter: (
  form: Form,
) => (pattern: FieldMatchPattern, payload?: any) => void;
export declare const createFieldStateGetter: (
  form: Form,
) => (pattern: FieldMatchPattern, payload?: any) => any;
export declare const applyValuesPatch: (
  form: Form,
  path: Array<string | number>,
  source: any,
) => void;
export declare const triggerFormInitialValuesChange: (form: Form, change: DataChange) => void;
export declare const triggerFormValuesChange: (form: Form, change: DataChange) => void;
