var __assign =
  (this && this.__assign) ||
  function () {
    __assign =
      Object.assign ||
      function (t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
      };
    return __assign.apply(this, arguments);
  };
var __awaiter =
  (this && this.__awaiter) ||
  function (thisArg, _arguments, P, generator) {
    function adopt(value) {
      return value instanceof P
        ? value
        : new P(function (resolve) {
            resolve(value);
          });
    }
    return new (P || (P = Promise))(function (resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
var __generator =
  (this && this.__generator) ||
  function (thisArg, body) {
    var _ = {
        label: 0,
        sent: function () {
          if (t[0] & 1) throw t[1];
          return t[1];
        },
        trys: [],
        ops: [],
      },
      f,
      y,
      t,
      g;
    return (
      (g = { next: verb(0), throw: verb(1), return: verb(2) }),
      typeof Symbol === 'function' &&
        (g[Symbol.iterator] = function () {
          return this;
        }),
      g
    );
    function verb(n) {
      return function (v) {
        return step([n, v]);
      };
    }
    function step(op) {
      if (f) throw new TypeError('Generator is already executing.');
      while (_)
        try {
          if (
            ((f = 1),
            y &&
              (t =
                op[0] & 2
                  ? y['return']
                  : op[0]
                  ? y['throw'] || ((t = y['return']) && t.call(y), 0)
                  : y.next) &&
              !(t = t.call(y, op[1])).done)
          )
            return t;
          if (((y = 0), t)) op = [op[0] & 2, t.value];
          switch (op[0]) {
            case 0:
            case 1:
              t = op;
              break;
            case 4:
              _.label++;
              return { value: op[1], done: false };
            case 5:
              _.label++;
              y = op[1];
              op = [0];
              continue;
            case 7:
              op = _.ops.pop();
              _.trys.pop();
              continue;
            default:
              if (
                !((t = _.trys), (t = t.length > 0 && t[t.length - 1])) &&
                (op[0] === 6 || op[0] === 2)
              ) {
                _ = 0;
                continue;
              }
              if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) {
                _.label = op[1];
                break;
              }
              if (op[0] === 6 && _.label < t[1]) {
                _.label = t[1];
                t = op;
                break;
              }
              if (t && _.label < t[2]) {
                _.label = t[2];
                _.ops.push(op);
                break;
              }
              if (t[2]) _.ops.pop();
              _.trys.pop();
              continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [6, e];
          y = 0;
        } finally {
          f = t = 0;
        }
      if (op[0] & 5) throw op[1];
      return { value: op[0] ? op[1] : void 0, done: true };
    }
  };
var __read =
  (this && this.__read) ||
  function (o, n) {
    var m = typeof Symbol === 'function' && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o),
      r,
      ar = [],
      e;
    try {
      while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    } catch (error) {
      e = { error: error };
    } finally {
      try {
        if (r && !r.done && (m = i['return'])) m.call(i);
      } finally {
        if (e) throw e.error;
      }
    }
    return ar;
  };
import {
  FormPath,
  each,
  pascalCase,
  isFn,
  isValid,
  isEmpty,
  isArr,
  isPlainObj,
  toArr,
  isNumberLike,
  shallowClone,
  isEqual,
} from '@/pages/design/visualSource/@formily/shared';
import { validate } from '@/pages/design/visualSource/@formily/validator';
import { batch, toJS } from '@/pages/design/visualSource/@formily/reactive';
import { LifeCycleTypes } from '../types';
import { isArrayField, isGeneralField, isQuery, isVoidField } from './externals';
import { ReservedProperties, GlobalState } from './constants';
export var isHTMLInputEvent = function (event, stopPropagation) {
  var _a;
  if (stopPropagation === void 0) {
    stopPropagation = true;
  }
  if (event === null || event === void 0 ? void 0 : event.target) {
    if (isValid(event.target.value) || isValid(event.target.checked)) return true;
    if (
      event.target.tagName &&
      event.target.tagName !== 'INPUT' &&
      event.target.tagName !== 'TEXTAREA' &&
      event.target.tagName !== 'SELECT'
    ) {
      return false;
    }
    if (stopPropagation)
      (_a = event.stopPropagation) === null || _a === void 0 ? void 0 : _a.call(event);
    return true;
  }
  return false;
};
export var getValuesFromEvent = function (args) {
  return args.map(function (event) {
    if (event === null || event === void 0 ? void 0 : event.target) {
      if (isValid(event.target.value)) return event.target.value;
      if (isValid(event.target.checked)) return event.target.checked;
      return;
    }
    return event;
  });
};
export var buildFieldPath = function (field) {
  var prevArray = false;
  return field.address.reduce(function (path, key, index) {
    var currentPath = path.concat([key]);
    var currentAddress = field.address.slice(0, index + 1);
    var current = field.form.fields[currentAddress.toString()];
    if (prevArray) {
      prevArray = false;
      return path;
    }
    if (index >= field.address.length - 1) {
      if (isVoidField(field)) {
        return currentPath;
      }
      return currentPath;
    }
    if (isVoidField(current)) {
      var parentAddress = field.address.slice(0, index);
      var parent_1 = field.form.fields[parentAddress.toString()];
      if (isArrayField(parent_1) && isNumberLike(key)) {
        prevArray = true;
        return currentPath;
      }
      return path;
    } else {
      prevArray = false;
    }
    return currentPath;
  }, new FormPath(''));
};
export var buildNodeIndexes = function (field, address) {
  field.address = FormPath.parse(address);
  field.path = buildFieldPath(field);
  field.form.indexes.set(field.path.toString(), field.address.toString());
  return field;
};
export var applyFieldPatches = function (target, patches) {
  patches.forEach(function (_a) {
    var type = _a.type,
      address = _a.address,
      payload = _a.payload;
    if (type === 'remove') {
      target[address].dispose();
      delete target[address];
    } else if (type === 'update') {
      if (payload) {
        target[address] = payload;
      }
      if (address && payload) {
        buildNodeIndexes(payload, address);
      }
    }
  });
};
export var matchFeedback = function (search, feedback) {
  if (!search || !feedback) return false;
  if (search.type && search.type !== feedback.type) return false;
  if (search.code && search.code !== feedback.code) return false;
  if (search.path && feedback.path) {
    if (!FormPath.parse(search.path).match(feedback.path)) return false;
  }
  if (search.address && feedback.address) {
    if (!FormPath.parse(search.address).match(feedback.address)) return false;
  }
  if (search.triggerType && search.triggerType !== feedback.triggerType) return false;
  return true;
};
export var queryFeedbacks = function (field, search) {
  return field.feedbacks.filter(function (feedback) {
    var _a, _b;
    return matchFeedback(
      search,
      __assign(__assign({}, feedback), {
        address: (_a = field.address) === null || _a === void 0 ? void 0 : _a.toString(),
        path: (_b = field.path) === null || _b === void 0 ? void 0 : _b.toString(),
      }),
    );
  });
};
export var queryFeedbackMessages = function (field, search) {
  return queryFeedbacks(field, search).reduce(function (buf, info) {
    return isEmpty(info.messages) ? buf : buf.concat(info.messages);
  }, []);
};
export var updateFeedback = function (field, feedback) {
  if (!feedback) return;
  return batch(function () {
    var _a;
    if (!field.feedbacks.length) {
      if (!((_a = feedback.messages) === null || _a === void 0 ? void 0 : _a.length)) {
        return;
      }
      field.feedbacks = [feedback];
    } else {
      var searched_1 = queryFeedbacks(field, feedback);
      if (searched_1.length) {
        field.feedbacks = field.feedbacks.reduce(function (buf, item) {
          if (searched_1.includes(item)) {
            var messages = toArr(feedback.messages);
            if (messages === null || messages === void 0 ? void 0 : messages.length) {
              item.messages = messages;
              return buf.concat(item);
            } else {
              return buf;
            }
          } else {
            return buf.concat(item);
          }
        }, []);
        return;
      }
      field.feedbacks = field.feedbacks.concat(feedback);
    }
  });
};
export var validateToFeedbacks = function (field, triggerType) {
  return __awaiter(void 0, void 0, void 0, function () {
    var results, takeSkipCondition;
    return __generator(this, function (_a) {
      switch (_a.label) {
        case 0:
          return [
            4 /*yield*/,
            validate(field.value, field.validator, {
              triggerType: triggerType,
              validateFirst: field.props.validateFirst || field.form.props.validateFirst,
              context: this,
            }),
          ];
        case 1:
          results = _a.sent();
          takeSkipCondition = function () {
            if (field.display !== 'visible') return true;
            if (field.pattern !== 'editable') return true;
            return false;
          };
          batch(function () {
            each(results, function (messages, type) {
              field.setFeedback({
                triggerType: triggerType,
                type: type,
                code: pascalCase('validate-' + type),
                messages: takeSkipCondition() ? [] : messages,
              });
            });
          });
          return [2 /*return*/, results];
      }
    });
  });
};
export var spliceArrayState = function (field, props) {
  var _a = __assign({ startIndex: 0, deleteCount: 0, insertCount: 0 }, props),
    startIndex = _a.startIndex,
    deleteCount = _a.deleteCount,
    insertCount = _a.insertCount;
  var address = field.address.toString();
  var fields = field.form.fields;
  var fieldPatches = [];
  var offset = insertCount - deleteCount;
  var isArrayChildren = function (identifier) {
    return identifier.indexOf(address) === 0 && identifier.length > address.length;
  };
  var isAfterNode = function (identifier) {
    var _a;
    var afterStr = identifier.slice(address.length);
    var number = (_a = afterStr.match(/^\.(\d+)/)) === null || _a === void 0 ? void 0 : _a[1];
    if (number === undefined) return false;
    var index = Number(number);
    return index > startIndex + deleteCount - 1;
  };
  var isInsertNode = function (identifier) {
    var _a;
    var afterStr = identifier.slice(address.length);
    var number = (_a = afterStr.match(/^\.(\d+)/)) === null || _a === void 0 ? void 0 : _a[1];
    if (number === undefined) return false;
    var index = Number(number);
    return index >= startIndex && index < startIndex + insertCount;
  };
  var isDeleteNode = function (identifier) {
    var _a;
    var preStr = identifier.slice(0, address.length);
    var afterStr = identifier.slice(address.length);
    var number = (_a = afterStr.match(/^\.(\d+)/)) === null || _a === void 0 ? void 0 : _a[1];
    if (number === undefined) return false;
    var index = Number(number);
    return (
      index >= startIndex &&
      !fields['' + preStr + afterStr.replace(/^\.\d+/, '.' + (index + deleteCount))]
    );
  };
  var moveIndex = function (identifier) {
    var _a;
    if (offset === 0) return identifier;
    var preStr = identifier.slice(0, address.length);
    var afterStr = identifier.slice(address.length);
    var number = (_a = afterStr.match(/^\.(\d+)/)) === null || _a === void 0 ? void 0 : _a[1];
    if (number === undefined) return identifier;
    var index = Number(number) + offset;
    return '' + preStr + afterStr.replace(/^\.\d+/, '.' + index);
  };
  batch(function () {
    each(fields, function (field, identifier) {
      if (isArrayChildren(identifier)) {
        if (isAfterNode(identifier)) {
          var newIdentifier = moveIndex(identifier);
          fieldPatches.push({
            type: 'update',
            address: newIdentifier,
            payload: field,
          });
        }
        if (isInsertNode(identifier) || isDeleteNode(identifier)) {
          fieldPatches.push({ type: 'remove', address: identifier });
        }
      }
    });
    applyFieldPatches(fields, fieldPatches);
  });
  field.form.notify(LifeCycleTypes.ON_FORM_GRAPH_CHANGE);
};
export var exchangeArrayState = function (field, props) {
  var _a = __assign({ fromIndex: 0, toIndex: 0 }, props),
    fromIndex = _a.fromIndex,
    toIndex = _a.toIndex;
  var address = field.address.toString();
  var fields = field.form.fields;
  var fieldPatches = [];
  var isArrayChildren = function (identifier) {
    return identifier.indexOf(address) === 0 && identifier.length > address.length;
  };
  var isFromOrToNode = function (identifier) {
    var _a;
    var afterStr = identifier.slice(address.length);
    var number = (_a = afterStr.match(/^\.(\d+)/)) === null || _a === void 0 ? void 0 : _a[1];
    if (number === undefined) return false;
    var index = Number(number);
    return index === toIndex || index === fromIndex;
  };
  var moveIndex = function (identifier) {
    var preStr = identifier.slice(0, address.length);
    var afterStr = identifier.slice(address.length);
    var number = afterStr.match(/^\.(\d+)/)[1];
    var current = Number(number);
    var index = current;
    if (index === fromIndex) {
      index = toIndex;
    } else {
      index = fromIndex;
    }
    return '' + preStr + afterStr.replace(/^\.\d+/, '.' + index);
  };
  batch(function () {
    each(fields, function (field, identifier) {
      if (isArrayChildren(identifier)) {
        if (isFromOrToNode(identifier)) {
          var newIdentifier = moveIndex(identifier);
          fieldPatches.push({
            type: 'update',
            address: newIdentifier,
            payload: field,
          });
          if (!fields[newIdentifier]) {
            fieldPatches.push({
              type: 'remove',
              address: identifier,
            });
          }
        }
      }
    });
    applyFieldPatches(fields, fieldPatches);
  });
  field.form.notify(LifeCycleTypes.ON_FORM_GRAPH_CHANGE);
};
export var cleanupArrayChildren = function (field, start) {
  var address = field.address.toString();
  var fields = field.form.fields;
  var isArrayChildren = function (identifier) {
    return identifier.indexOf(address) === 0 && identifier.length > address.length;
  };
  var isNeedCleanup = function (identifier) {
    var _a;
    var afterStr = identifier.slice(address.length);
    var number = (_a = afterStr.match(/^\.(\d+)/)) === null || _a === void 0 ? void 0 : _a[1];
    if (number === undefined) return false;
    var index = Number(number);
    return index >= start;
  };
  batch(function () {
    each(fields, function (field, identifier) {
      if (isArrayChildren(identifier) && isNeedCleanup(identifier)) {
        field.destroy();
      }
    });
  });
};
export var cleanupObjectChildren = function (field, keys) {
  if (keys.length === 0) return;
  var address = field.address.toString();
  var fields = field.form.fields;
  var isObjectChildren = function (identifier) {
    return identifier.indexOf(address) === 0 && identifier.length > address.length;
  };
  var isNeedCleanup = function (identifier) {
    var _a;
    var afterStr = identifier.slice(address.length);
    var key = (_a = afterStr.match(/^\.([^.]+)/)) === null || _a === void 0 ? void 0 : _a[1];
    if (key === undefined) return false;
    return keys.includes(key);
  };
  batch(function () {
    each(fields, function (field, identifier) {
      if (isObjectChildren(identifier) && isNeedCleanup(identifier)) {
        field.destroy();
      }
    });
  });
};
export var initFieldValue = function (field, designable) {
  GlobalState.initializing = true;
  if (designable) {
    if (isValid(field.props.initialValue)) {
      field.initialValue = shallowClone(field.props.initialValue);
    }
    if (isValid(field.props.value)) {
      field.value = field.props.value;
    }
  } else {
    var fromValue = function (value) {
      return [isValid(value), isEmpty(value, true), value];
    };
    var _a = __read(fromValue(field.value), 3),
      isEmptySelfValue = _a[1],
      selfValue = _a[2];
    var _b = __read(fromValue(field.initialValue), 3),
      isEmptySelfInitialValue = _b[1],
      selfInitialValue = _b[2];
    var _c = __read(fromValue(field.props.value), 3),
      isValidPropsValue = _c[0],
      isEmptyPropsValue = _c[1],
      propsValue = _c[2];
    var _d = __read(fromValue(field.props.initialValue), 3),
      isValidPropsInitialValue = _d[0],
      isEmptyPropsInitialValue = _d[1],
      propsInitialValue = _d[2];
    if (isEmptySelfInitialValue) {
      if (isEmptyPropsInitialValue) {
        if (!isEqual(selfInitialValue, propsInitialValue)) {
          field.initialValue = shallowClone(propsInitialValue);
        }
      } else if (isValidPropsInitialValue) {
        field.initialValue = shallowClone(propsInitialValue);
      }
    }
    if (isEmptySelfValue) {
      if (!isEmptyPropsValue) {
        field.value = shallowClone(propsValue);
      } else {
        if (!isEmptyPropsInitialValue) {
          field.value = shallowClone(propsInitialValue);
        } else if (isValidPropsValue) {
          if (!isEqual(selfValue, propsValue)) {
            field.value = shallowClone(propsValue);
          }
        } else if (isValidPropsInitialValue) {
          if (!isEqual(selfValue, propsInitialValue)) {
            field.value = shallowClone(propsInitialValue);
          }
        }
      }
    }
  }
  GlobalState.initializing = false;
};
export var initFieldUpdate = function (field) {
  var form = field.form;
  var updates = FormPath.ensureIn(form, 'requests.updates', []);
  var indexes = FormPath.ensureIn(form, 'requests.updateIndexes', {});
  for (var index = 0; index < updates.length; index++) {
    var _a = updates[index],
      pattern = _a.pattern,
      callbacks = _a.callbacks;
    var removed = false;
    if (field.match(pattern)) {
      callbacks.forEach(function (callback) {
        field.setState(callback);
      });
      if (!pattern.isWildMatchPattern && !pattern.isMatchPattern) {
        updates.splice(index--, 1);
        removed = true;
      }
    }
    if (!removed) {
      indexes[pattern.toString()] = index;
    } else {
      delete indexes[pattern.toString()];
    }
  }
};
export var subscribeUpdate = function (form, pattern, callback) {
  var updates = FormPath.ensureIn(form, 'requests.updates', []);
  var indexes = FormPath.ensureIn(form, 'requests.updateIndexes', {});
  var id = pattern.toString();
  var current = indexes[id];
  if (isValid(current)) {
    if (
      updates[current] &&
      !updates[current].callbacks.some(function (fn) {
        return fn.toString() === callback.toString() ? fn === callback : false;
      })
    ) {
      updates[current].callbacks.push(callback);
    }
  } else {
    indexes[id] = updates.length;
    updates.push({
      pattern: pattern,
      callbacks: [callback],
    });
  }
};
export var setModelState = function (model, setter) {
  if (!model) return;
  var isSkipProperty = function (key) {
    var _a;
    if (key === 'address' || key === 'path') return true;
    if (key === 'valid' || key === 'invalid') return true;
    if (key === 'componentType' || key === 'componentProps') return true;
    if (key === 'decoratorType' || key === 'decoratorProps') return true;
    if (key === 'validateStatus') return true;
    if (key === 'errors' || key === 'warnings' || key === 'successes') {
      if (model.displayName === 'Form') return true;
      if ((_a = setter.feedbacks) === null || _a === void 0 ? void 0 : _a.length) {
        return true;
      }
    }
    if (
      (key === 'display' || key === 'visible' || key === 'hidden') &&
      'selfDisplay' in setter &&
      !isValid(setter.selfDisplay)
    ) {
      return true;
    }
    if (
      (key === 'pattern' ||
        key === 'editable' ||
        key === 'disabled' ||
        key === 'readOnly' ||
        key === 'readPretty') &&
      'selfPattern' in setter &&
      !isValid(setter.selfPattern)
    ) {
      return true;
    }
    return false;
  };
  if (isFn(setter)) {
    setter(model);
  } else {
    Object.keys(setter || {}).forEach(function (key) {
      var value = setter[key];
      if (isFn(value)) return;
      if (ReservedProperties.includes(key)) return;
      if (isSkipProperty(key)) return;
      model[key] = value;
    });
  }
  return model;
};
export var getModelState = function (model, getter) {
  if (isFn(getter)) {
    return getter(model);
  } else {
    return Object.keys(model || {}).reduce(function (buf, key) {
      var value = model[key];
      if (isFn(value)) {
        return buf;
      }
      if (ReservedProperties.includes(key)) return buf;
      if (key === 'address' || key === 'path') {
        buf[key] = value.toString();
        return buf;
      }
      buf[key] = toJS(value);
      return buf;
    }, {});
  }
};
export var modelStateSetter = function (model) {
  return batch.bound(function (state) {
    return setModelState(model, state);
  });
};
export var modelStateGetter = function (model) {
  return function (getter) {
    return getModelState(model, getter);
  };
};
export var createFieldStateSetter = function (form) {
  return batch.bound(function (pattern, payload) {
    if (isQuery(pattern)) {
      pattern.forEach(function (field) {
        field.setState(payload);
      });
    } else if (isGeneralField(pattern)) {
      pattern.setState(payload);
    } else {
      var matchCount_1 = 0,
        path = FormPath.parse(pattern);
      form.query(path).forEach(function (field) {
        field.setState(payload);
        matchCount_1++;
      });
      if (matchCount_1 === 0 || path.isWildMatchPattern) {
        subscribeUpdate(form, path, payload);
      }
    }
  });
};
export var createFieldStateGetter = function (form) {
  return function (pattern, payload) {
    if (isQuery(pattern)) {
      return pattern.take(payload);
    } else if (isGeneralField(pattern)) {
      return pattern.getState(payload);
    } else {
      return form.query(pattern).take(function (field) {
        return field.getState(payload);
      });
    }
  };
};
export var applyValuesPatch = function (form, path, source) {
  var merge = function (path, source) {
    if (path.length) {
      form.setValuesIn(path, toJS(source));
    } else {
      Object.assign(form.values, toJS(source));
    }
  };
  var patch = function (source, path) {
    if (path === void 0) {
      path = [];
    }
    var targetValue = form.getValuesIn(path);
    var targetField = form.query(path).take();
    if (isEmpty(targetValue)) {
      if (isEmpty(source)) return;
      merge(path, source);
    } else {
      var arrA = isArr(targetValue);
      var arrB = isArr(source);
      var objA = isPlainObj(targetValue);
      var objB = isPlainObj(source);
      if ((arrA && arrA === arrB) || (objA && objA === objB)) {
        each(source, function (value, key) {
          if (isEmpty(value)) return;
          patch(value, path.concat(key));
        });
      } else {
        if (targetField) {
          if (!isVoidField(targetField) && !targetField.modified) {
            merge(path, source);
          }
        } else {
          merge(path, source);
        }
      }
    }
  };
  if (GlobalState.initializing) return;
  patch(source, path);
};
export var triggerFormInitialValuesChange = function (form, change) {
  var path = change.path;
  if (path[path.length - 1] === 'length') return;
  if (path[0] === 'initialValues') {
    if (change.type === 'add' || change.type === 'set') {
      applyValuesPatch(form, path.slice(1), change.value);
    }
    form.notify(LifeCycleTypes.ON_FORM_INITIAL_VALUES_CHANGE);
  }
};
export var triggerFormValuesChange = function (form, change) {
  var path = change.path;
  if (path[path.length - 1] === 'length') return;
  if (path[0] === 'values') {
    form.notify(LifeCycleTypes.ON_FORM_VALUES_CHANGE);
  }
};
//# sourceMappingURL=internals.js.map
