export var LifeCycleTypes;
(function (LifeCycleTypes) {
  /**
   * Form LifeCycle
   **/
  LifeCycleTypes['ON_FORM_INIT'] = 'onFormInit';
  LifeCycleTypes['ON_FORM_MOUNT'] = 'onFormMount';
  LifeCycleTypes['ON_FORM_UNMOUNT'] = 'onFormUnmount';
  LifeCycleTypes['ON_FORM_SUBMIT'] = 'onFormSubmit';
  LifeCycleTypes['ON_FORM_RESET'] = 'onFormReset';
  LifeCycleTypes['ON_FORM_SUBMIT_START'] = 'onFormSubmitStart';
  LifeCycleTypes['ON_FORM_SUBMITTING'] = 'onFormSubmitting';
  LifeCycleTypes['ON_FORM_SUBMIT_END'] = 'onFormSubmitEnd';
  LifeCycleTypes['ON_FORM_SUBMIT_VALIDATE_START'] = 'onFormSubmitValidateStart';
  LifeCycleTypes['ON_FORM_SUBMIT_VALIDATE_SUCCESS'] = 'onFormSubmitValidateSuccess';
  LifeCycleTypes['ON_FORM_SUBMIT_VALIDATE_FAILED'] = 'onFormSubmitValidateFailed';
  LifeCycleTypes['ON_FORM_SUBMIT_VALIDATE_END'] = 'onFormSubmitValidateEnd';
  LifeCycleTypes['ON_FORM_SUBMIT_SUCCESS'] = 'onFormSubmitSuccess';
  LifeCycleTypes['ON_FORM_SUBMIT_FAILED'] = 'onFormSubmitFailed';
  LifeCycleTypes['ON_FORM_VALUES_CHANGE'] = 'onFormValuesChange';
  LifeCycleTypes['ON_FORM_INITIAL_VALUES_CHANGE'] = 'onFormInitialValuesChange';
  LifeCycleTypes['ON_FORM_VALIDATE_START'] = 'onFormValidateStart';
  LifeCycleTypes['ON_FORM_VALIDATING'] = 'onFormValidating';
  LifeCycleTypes['ON_FORM_VALIDATE_SUCCESS'] = 'onFormValidateSuccess';
  LifeCycleTypes['ON_FORM_VALIDATE_FAILED'] = 'onFormValidateFailed';
  LifeCycleTypes['ON_FORM_VALIDATE_END'] = 'onFormValidateEnd';
  LifeCycleTypes['ON_FORM_INPUT_CHANGE'] = 'onFormInputChange';
  LifeCycleTypes['ON_FORM_GRAPH_CHANGE'] = 'onFormGraphChange';
  /**
   * Field LifeCycle
   **/
  LifeCycleTypes['ON_FIELD_INIT'] = 'onFieldInit';
  LifeCycleTypes['ON_FIELD_INPUT_VALUE_CHANGE'] = 'onFieldInputValueChange';
  LifeCycleTypes['ON_FIELD_VALUE_CHANGE'] = 'onFieldValueChange';
  LifeCycleTypes['ON_FIELD_INITIAL_VALUE_CHANGE'] = 'onFieldInitialValueChange';
  LifeCycleTypes['ON_FIELD_VALIDATE_START'] = 'onFieldValidateStart';
  LifeCycleTypes['ON_FIELD_VALIDATING'] = 'onFieldValidating';
  LifeCycleTypes['ON_FIELD_VALIDATE_SUCCESS'] = 'onFieldValidateSuccess';
  LifeCycleTypes['ON_FIELD_VALIDATE_FAILED'] = 'onFieldValidateFailed';
  LifeCycleTypes['ON_FIELD_VALIDATE_END'] = 'onFieldValidateEnd';
  LifeCycleTypes['ON_FIELD_LOADING'] = 'onFieldLoading';
  LifeCycleTypes['ON_FIELD_RESET'] = 'onFieldReset';
  LifeCycleTypes['ON_FIELD_MOUNT'] = 'onFieldMount';
  LifeCycleTypes['ON_FIELD_UNMOUNT'] = 'onFieldUnmount';
})(LifeCycleTypes || (LifeCycleTypes = {}));
//# sourceMappingURL=types.js.map
