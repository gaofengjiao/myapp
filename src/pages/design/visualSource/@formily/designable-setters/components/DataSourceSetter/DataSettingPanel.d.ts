import React from 'react';
import { ITreeDataSource } from './types';
import './styles.less';
export interface IDataSettingPanelProps {
  treeDataSource: ITreeDataSource;
}
export declare const DataSettingPanel: React.FC<IDataSettingPanelProps>;
