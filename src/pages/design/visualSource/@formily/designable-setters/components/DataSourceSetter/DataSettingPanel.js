import React, { useMemo, Fragment } from 'react';
import { Button } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { ArrayItems, Form, Input, FormItem } from '@/pages/design/visualSource/@formily/antd';
import { createForm } from '@/pages/design/visualSource/@formily/core';
import { observer } from '@/pages/design/visualSource/@formily/reactive-react';
import { createSchemaField } from '@/pages/design/visualSource/@formily/react';
import { ValueInput } from '@/pages/design/visualSource/@designable/react-settings-form';
import { usePrefix, TextWidget } from '@/pages/design/visualSource/@designable/react';
import { Header } from './Header';
import { traverseTree } from './shared';
import './styles.less';
var SchemaField = createSchemaField({
  components: {
    FormItem: FormItem,
    Input: Input,
    ArrayItems: ArrayItems,
    ValueInput: ValueInput,
  },
});
export var DataSettingPanel = observer(function (props) {
  var prefix = usePrefix('data-source-setter');
  var form = useMemo(
    function () {
      var values;
      traverseTree(props.treeDataSource.dataSource, function (dataItem) {
        if (dataItem.key === props.treeDataSource.selectedKey) {
          values = dataItem;
        }
      });
      return createForm({
        values: values,
      });
    },
    [props.treeDataSource.selectedKey, props.treeDataSource.dataSource.length],
  );
  if (!props.treeDataSource.selectedKey)
    return React.createElement(
      Fragment,
      null,
      React.createElement(Header, {
        title: React.createElement(TextWidget, {
          token: 'SettingComponents.DataSourceSetter.nodeProperty',
        }),
        extra: null,
      }),
      React.createElement(
        'div',
        { className: '' + (prefix + '-layout-item-content') },
        React.createElement(TextWidget, {
          token: 'SettingComponents.DataSourceSetter.pleaseSelectNode',
        }),
      ),
    );
  return React.createElement(
    Fragment,
    null,
    React.createElement(Header, {
      title: React.createElement(TextWidget, {
        token: 'SettingComponents.DataSourceSetter.nodeProperty',
      }),
      extra: React.createElement(
        Button,
        {
          type: 'text',
          onClick: function () {
            form.setFieldState('map', function (state) {
              state.value.push({});
            });
          },
          icon: React.createElement(PlusOutlined, null),
        },
        React.createElement(TextWidget, {
          token: 'SettingComponents.DataSourceSetter.addKeyValuePair',
        }),
      ),
    }),
    React.createElement(
      'div',
      { className: '' + (prefix + '-layout-item-content') },
      React.createElement(
        Form,
        { form: form, labelWidth: 60, wrapperWidth: 160 },
        React.createElement(
          SchemaField,
          null,
          React.createElement(
            SchemaField.Array,
            { name: 'map', 'x-component': 'ArrayItems' },
            React.createElement(
              SchemaField.Object,
              { 'x-decorator': 'ArrayItems.Item', 'x-decorator-props': { type: 'divide' } },
              React.createElement(SchemaField.String, {
                title: React.createElement(TextWidget, {
                  token: 'SettingComponents.DataSourceSetter.label',
                }),
                'x-decorator': 'FormItem',
                name: 'label',
                'x-component': 'Input',
              }),
              React.createElement(SchemaField.String, {
                title: React.createElement(TextWidget, {
                  token: 'SettingComponents.DataSourceSetter.value',
                }),
                'x-decorator': 'FormItem',
                name: 'value',
                'x-component': 'ValueInput',
              }),
              React.createElement(SchemaField.Void, {
                'x-component': 'ArrayItems.Remove',
                'x-component-props': {
                  style: {
                    margin: 5,
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                  },
                },
              }),
            ),
          ),
        ),
      ),
    ),
  );
});
//# sourceMappingURL=DataSettingPanel.js.map
