import React from 'react';
import { observer } from '@/pages/design/visualSource/@formily/reactive-react';
import { usePrefix } from '@/pages/design/visualSource/@designable/react';
import './styles.less';
export var Header = observer(function (_a) {
  var extra = _a.extra,
    title = _a.title;
  var prefix = usePrefix('data-source-setter');
  return React.createElement(
    'div',
    { className: '' + (prefix + '-layout-item-header') },
    React.createElement('div', { className: '' + (prefix + '-layout-item-title') }, title),
    extra,
  );
});
//# sourceMappingURL=Header.js.map
