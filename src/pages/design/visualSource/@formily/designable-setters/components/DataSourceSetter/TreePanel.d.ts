import React from 'react';
import { ITreeDataSource } from './types';
import './styles.less';
export interface ITreePanelProps {
  treeDataSource: ITreeDataSource;
}
export declare const TreePanel: React.FC<ITreePanelProps>;
