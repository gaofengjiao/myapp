import React from 'react';
import { IDataSourceItem } from './types';
import './styles.less';
export interface IDataSourceSetterProps {
  className?: string;
  style?: React.CSSProperties;
  onChange: (dataSource: IDataSourceItem[]) => void;
  value: IDataSourceItem[];
}
export declare const DataSourceSetter: React.FC<IDataSourceSetterProps>;
