var __read =
  (this && this.__read) ||
  function (o, n) {
    var m = typeof Symbol === 'function' && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o),
      r,
      ar = [],
      e;
    try {
      while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    } catch (error) {
      e = { error: error };
    } finally {
      try {
        if (r && !r.done && (m = i['return'])) m.call(i);
      } finally {
        if (e) throw e.error;
      }
    }
    return ar;
  };
import React, { Fragment, useMemo, useState } from 'react';
import cls from 'classnames';
import { Modal, Button } from 'antd';
import { observable } from '@/pages/design/visualSource/@formily/reactive';
import { observer } from '@/pages/design/visualSource/@formily/reactive-react';
import { usePrefix, useTheme, TextWidget } from '@/pages/design/visualSource/@designable/react';
import { DataSettingPanel } from './DataSettingPanel';
import { TreePanel } from './TreePanel';
import { transformDataToValue, transformValueToData } from './shared';
import './styles.less';
export var DataSourceSetter = observer(function (props) {
  var className = props.className,
    _a = props.value,
    value = _a === void 0 ? [] : _a,
    onChange = props.onChange;
  var theme = useTheme();
  var prefix = usePrefix('data-source-setter');
  var _b = __read(useState(false), 2),
    modalVisible = _b[0],
    setModalVisible = _b[1];
  var treeDataSource = useMemo(
    function () {
      return observable({
        dataSource: transformValueToData(value),
        selectedKey: '',
      });
    },
    [value, modalVisible],
  );
  var openModal = function () {
    return setModalVisible(true);
  };
  var closeModal = function () {
    return setModalVisible(false);
  };
  return React.createElement(
    Fragment,
    null,
    React.createElement(
      Button,
      { block: true, onClick: openModal },
      React.createElement(TextWidget, {
        token: 'SettingComponents.DataSourceSetter.configureDataSource',
      }),
    ),
    React.createElement(
      Modal,
      {
        title: React.createElement(TextWidget, {
          token: 'SettingComponents.DataSourceSetter.configureDataSource',
        }),
        width: '65%',
        bodyStyle: { padding: 10 },
        transitionName: '',
        maskTransitionName: '',
        visible: modalVisible,
        onCancel: closeModal,
        onOk: function () {
          onChange(transformDataToValue(treeDataSource.dataSource));
          closeModal();
        },
      },
      React.createElement(
        'div',
        {
          className:
            cls(prefix, className) + ' ' + (prefix + '-' + theme) + ' ' + (prefix + '-layout'),
        },
        React.createElement(
          'div',
          { className: '' + (prefix + '-layout-item left') },
          React.createElement(TreePanel, { treeDataSource: treeDataSource }),
        ),
        React.createElement(
          'div',
          { className: '' + (prefix + '-layout-item right') },
          React.createElement(DataSettingPanel, { treeDataSource: treeDataSource }),
        ),
      ),
    ),
  );
});
//# sourceMappingURL=index.js.map
