var __awaiter =
  (this && this.__awaiter) ||
  function (thisArg, _arguments, P, generator) {
    function adopt(value) {
      return value instanceof P
        ? value
        : new P(function (resolve) {
            resolve(value);
          });
    }
    return new (P || (P = Promise))(function (resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
var __generator =
  (this && this.__generator) ||
  function (thisArg, body) {
    var _ = {
        label: 0,
        sent: function () {
          if (t[0] & 1) throw t[1];
          return t[1];
        },
        trys: [],
        ops: [],
      },
      f,
      y,
      t,
      g;
    return (
      (g = { next: verb(0), throw: verb(1), return: verb(2) }),
      typeof Symbol === 'function' &&
        (g[Symbol.iterator] = function () {
          return this;
        }),
      g
    );
    function verb(n) {
      return function (v) {
        return step([n, v]);
      };
    }
    function step(op) {
      if (f) throw new TypeError('Generator is already executing.');
      while (_)
        try {
          if (
            ((f = 1),
            y &&
              (t =
                op[0] & 2
                  ? y['return']
                  : op[0]
                  ? y['throw'] || ((t = y['return']) && t.call(y), 0)
                  : y.next) &&
              !(t = t.call(y, op[1])).done)
          )
            return t;
          if (((y = 0), t)) op = [op[0] & 2, t.value];
          switch (op[0]) {
            case 0:
            case 1:
              t = op;
              break;
            case 4:
              _.label++;
              return { value: op[1], done: false };
            case 5:
              _.label++;
              y = op[1];
              op = [0];
              continue;
            case 7:
              op = _.ops.pop();
              _.trys.pop();
              continue;
            default:
              if (
                !((t = _.trys), (t = t.length > 0 && t[t.length - 1])) &&
                (op[0] === 6 || op[0] === 2)
              ) {
                _ = 0;
                continue;
              }
              if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) {
                _.label = op[1];
                break;
              }
              if (op[0] === 6 && _.label < t[1]) {
                _.label = t[1];
                t = op;
                break;
              }
              if (t && _.label < t[2]) {
                _.label = t[2];
                _.ops.push(op);
                break;
              }
              if (t[2]) _.ops.pop();
              _.trys.pop();
              continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [6, e];
          y = 0;
        } finally {
          f = t = 0;
        }
      if (op[0] & 5) throw op[1];
      return { value: op[0] ? op[1] : void 0, done: true };
    }
  };
import { MonacoInput } from '@/pages/design/visualSource/@designable/react-settings-form';
var loadDependencies = function (deps) {
  return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
      return [
        2 /*return*/,
        Promise.all(
          deps.map(function (_a) {
            var name = _a.name,
              path = _a.path;
            return __awaiter(void 0, void 0, void 0, function () {
              var _b;
              return __generator(this, function (_c) {
                switch (_c.label) {
                  case 0:
                    _b = {
                      name: name,
                      path: path,
                    };
                    return [
                      4 /*yield*/,
                      fetch('//cdn.jsdelivr.net/npm/' + name + '/' + path).then(function (res) {
                        return res.text();
                      }),
                    ];
                  case 1:
                    return [2 /*return*/, ((_b.library = _c.sent()), _b)];
                }
              });
            });
          }),
        ),
      ];
    });
  });
};
MonacoInput.loader.init().then(function (monaco) {
  return __awaiter(void 0, void 0, void 0, function () {
    var deps;
    return __generator(this, function (_a) {
      switch (_a.label) {
        case 0:
          return [
            4 /*yield*/,
            loadDependencies([
              { name: '@/visualSource/@formily/core', path: 'dist/formily.core.all.d.ts' },
            ]),
          ];
        case 1:
          deps = _a.sent();
          deps === null || deps === void 0
            ? void 0
            : deps.forEach(function (_a) {
                var name = _a.name,
                  library = _a.library;
                monaco.languages.typescript.typescriptDefaults.addExtraLib(
                  "declare module '" + name + "'{ " + library + ' }',
                  'file:///node_modules/' + name + '/index.d.ts',
                );
              });
          monaco.languages.typescript.typescriptDefaults.addExtraLib(
            "\n    import { Form, Field } from '@/visualSource/@formily/core'\n    declare global {\n      /*\n       * Form Model\n       **/\n      declare var $form: Form\n      /*\n       * Field Model\n       **/\n      declare var $self: Field\n      /*\n       * create an persistent observable state object\n       **/\n      declare var $observable: <T>(target: T, deps?: any[]) => T\n      /*\n       * create a persistent data\n       **/\n      declare var $memo: <T>(callback: () => T, deps?: any[]) => T\n      /*\n       * handle side-effect logic\n       **/\n      declare var $effect: (callback: () => void | (() => void), deps?: any[]) => void\n      /*\n       * set initial component props to current field\n       **/\n      declare var $props: (props: any) => void\n    }\n    ",
            'file:///node_modules/formily_global.d.ts',
          );
          return [2 /*return*/];
      }
    });
  });
});
//# sourceMappingURL=declarations.js.map
