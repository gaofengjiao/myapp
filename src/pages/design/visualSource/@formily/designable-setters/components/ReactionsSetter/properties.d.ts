export declare const FieldProperties: (
  | {
      key: string;
      type: string;
      helpCode: string;
      token?: undefined;
    }
  | {
      key: string;
      token: string;
      type: string;
      helpCode: string;
    }
)[];
