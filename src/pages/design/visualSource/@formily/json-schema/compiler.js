import {
  isArr,
  isFn,
  isPlainObj,
  isStr,
  reduce,
} from '@/pages/design/visualSource/@formily/shared';
import { isObservable } from '@/pages/design/visualSource/@formily/reactive';
import { Schema } from './schema';
var ExpRE = /^\s*\{\{([\s\S]*)\}\}\s*$/;
var actionsSymbol = Symbol.for('__REVA_ACTIONS');
var ENVS = {
  silent: false,
  compile: function (expression, scope) {
    if (scope === void 0) {
      scope = {};
    }
    if (ENVS.silent) {
      try {
        return new Function('$root', 'with($root) { return (' + expression + '); }')(scope);
      } catch (_a) {}
    } else {
      return new Function('$root', 'with($root) { return (' + expression + '); }')(scope);
    }
  },
};
export var silent = function (value) {
  if (value === void 0) {
    value = true;
  }
  ENVS.silent = !!value;
};
export var registerCompiler = function (compiler) {
  if (isFn(compiler)) {
    ENVS.compile = compiler;
  }
};
export var shallowCompile = function (source, scope) {
  if (isStr(source)) {
    var matched = source.match(ExpRE);
    if (!matched) return source;
    return ENVS.compile(matched[1], scope);
  } else if (isArr(source)) {
    return source.map(function (item) {
      return shallowCompile(item, scope);
    });
  }
  return source;
};
export var compile = function (source, scope) {
  var seenObjects = new WeakMap();
  var compile = function (source) {
    if (isStr(source)) {
      return shallowCompile(source, scope);
    } else if (isArr(source)) {
      return source.map(function (value) {
        return compile(value);
      });
    } else if (isPlainObj(source)) {
      if ('$$typeof' in source && '_owner' in source) {
        return source;
      }
      if (source[actionsSymbol]) {
        return source;
      }
      if (source['_isAMomentObject']) {
        return source;
      }
      if (Schema.isSchemaInstance(source)) {
        return source.compile(scope);
      }
      if (isFn(source['toJS'])) {
        return source;
      }
      if (isFn(source['toJSON'])) {
        return source;
      }
      if (isObservable(source)) {
        return source;
      }
      if (seenObjects.get(source)) {
        return source;
      }
      seenObjects.set(source, true);
      var results = reduce(
        source,
        function (buf, value, key) {
          buf[key] = compile(value);
          return buf;
        },
        {},
      );
      seenObjects.set(source, false);
      return results;
    }
    return source;
  };
  return compile(source);
};
//# sourceMappingURL=compiler.js.map
