/// <reference types="@/visualSource/@formily/core" />
import * as Types from './types';
declare global {
  namespace Formily.Schema {
    export { Types };
  }
}
