export declare const registerVoidComponents: (components: string[]) => void;
export declare const registerTypeDefaultComponents: (maps: Record<string, string>) => void;
