import {
  ISchema,
  SchemaEnum,
  SchemaProperties,
  SchemaReaction,
  SchemaTypes,
  SchemaKey,
  ISchemaTransformerOptions,
} from './types';
import { IFieldFactoryProps } from '@/pages/design/visualSource/@formily/core';
export declare class Schema<
  Decorator = any,
  Component = any,
  DecoratorProps = any,
  ComponentProps = any,
  Pattern = any,
  Display = any,
  Validator = any,
  Message = any,
  ReactionField = any,
> implements ISchema
{
  parent?: Schema;
  root?: Schema;
  name?: SchemaKey;
  title?: Message;
  description?: Message;
  default?: any;
  readOnly?: boolean;
  writeOnly?: boolean;
  type?: SchemaTypes;
  enum?: SchemaEnum<Message>;
  const?: any;
  multipleOf?: number;
  maximum?: number;
  exclusiveMaximum?: number;
  minimum?: number;
  exclusiveMinimum?: number;
  maxLength?: number;
  minLength?: number;
  pattern?: string | RegExp;
  maxItems?: number;
  minItems?: number;
  uniqueItems?: boolean;
  maxProperties?: number;
  minProperties?: number;
  required?: string[] | boolean | string;
  format?: string;
  /** nested json schema spec **/
  definitions?: Record<
    string,
    Schema<
      Decorator,
      Component,
      DecoratorProps,
      ComponentProps,
      Pattern,
      Display,
      Validator,
      Message
    >
  >;
  properties?: Record<
    string,
    Schema<
      Decorator,
      Component,
      DecoratorProps,
      ComponentProps,
      Pattern,
      Display,
      Validator,
      Message
    >
  >;
  items?:
    | Schema<
        Decorator,
        Component,
        DecoratorProps,
        ComponentProps,
        Pattern,
        Display,
        Validator,
        Message
      >
    | Schema<
        Decorator,
        Component,
        DecoratorProps,
        ComponentProps,
        Pattern,
        Display,
        Validator,
        Message
      >[];
  additionalItems?: Schema<
    Decorator,
    Component,
    DecoratorProps,
    ComponentProps,
    Pattern,
    Display,
    Validator,
    Message
  >;
  patternProperties?: Record<
    string,
    Schema<
      Decorator,
      Component,
      DecoratorProps,
      ComponentProps,
      Pattern,
      Display,
      Validator,
      Message
    >
  >;
  additionalProperties?: Schema<
    Decorator,
    Component,
    DecoratorProps,
    ComponentProps,
    Pattern,
    Display,
    Validator,
    Message
  >;
  ['x-index']?: number;
  ['x-pattern']?: Pattern;
  ['x-display']?: Display;
  ['x-validator']?: Validator;
  ['x-decorator']?: Decorator;
  ['x-decorator-props']?: DecoratorProps;
  ['x-component']?: Component;
  ['x-component-props']?: ComponentProps;
  ['x-reactions']?: SchemaReaction<ReactionField>[];
  ['x-content']?: any;
  ['x-visible']?: boolean;
  ['x-hidden']?: boolean;
  ['x-disabled']?: boolean;
  ['x-editable']?: boolean;
  ['x-read-only']?: boolean;
  ['x-read-pretty']?: boolean;
  _isJSONSchemaObject: boolean;
  version: string;
  constructor(
    json: ISchema<
      Decorator,
      Component,
      DecoratorProps,
      ComponentProps,
      Pattern,
      Display,
      Validator,
      Message
    >,
    parent?: Schema,
  );
  addProperty: (
    key: SchemaKey,
    schema: ISchema<
      Decorator,
      Component,
      DecoratorProps,
      ComponentProps,
      Pattern,
      Display,
      Validator,
      Message
    >,
  ) => Schema<
    Decorator,
    Component,
    DecoratorProps,
    ComponentProps,
    Pattern,
    Display,
    Validator,
    Message,
    any
  >;
  removeProperty: (
    key: SchemaKey,
  ) => Schema<
    Decorator,
    Component,
    DecoratorProps,
    ComponentProps,
    Pattern,
    Display,
    Validator,
    Message,
    any
  >;
  setProperties: (
    properties: SchemaProperties<
      Decorator,
      Component,
      DecoratorProps,
      ComponentProps,
      Pattern,
      Display,
      Validator,
      Message
    >,
  ) => this;
  addPatternProperty: (
    key: SchemaKey,
    schema: ISchema<
      Decorator,
      Component,
      DecoratorProps,
      ComponentProps,
      Pattern,
      Display,
      Validator,
      Message
    >,
  ) => Schema<
    Decorator,
    Component,
    DecoratorProps,
    ComponentProps,
    Pattern,
    Display,
    Validator,
    Message,
    any
  >;
  removePatternProperty: (
    key: SchemaKey,
  ) => Schema<
    Decorator,
    Component,
    DecoratorProps,
    ComponentProps,
    Pattern,
    Display,
    Validator,
    Message,
    any
  >;
  setPatternProperties: (
    properties: SchemaProperties<
      Decorator,
      Component,
      DecoratorProps,
      ComponentProps,
      Pattern,
      Display,
      Validator,
      Message
    >,
  ) => this;
  setAdditionalProperties: (
    properties: ISchema<
      Decorator,
      Component,
      DecoratorProps,
      ComponentProps,
      Pattern,
      Display,
      Validator,
      Message
    >,
  ) => Schema<
    Decorator,
    Component,
    DecoratorProps,
    ComponentProps,
    Pattern,
    Display,
    Validator,
    Message,
    any
  >;
  setItems: (
    schema:
      | ISchema<
          Decorator,
          Component,
          DecoratorProps,
          ComponentProps,
          Pattern,
          Display,
          Validator,
          Message
        >
      | ISchema<
          Decorator,
          Component,
          DecoratorProps,
          ComponentProps,
          Pattern,
          Display,
          Validator,
          Message
        >[],
  ) =>
    | Schema<
        Decorator,
        Component,
        DecoratorProps,
        ComponentProps,
        Pattern,
        Display,
        Validator,
        Message,
        any
      >
    | Schema<
        Decorator,
        Component,
        DecoratorProps,
        ComponentProps,
        Pattern,
        Display,
        Validator,
        Message,
        any
      >[];
  setAdditionalItems: (
    items: ISchema<
      Decorator,
      Component,
      DecoratorProps,
      ComponentProps,
      Pattern,
      Display,
      Validator,
      Message
    >,
  ) => Schema<
    Decorator,
    Component,
    DecoratorProps,
    ComponentProps,
    Pattern,
    Display,
    Validator,
    Message,
    any
  >;
  findDefinitions: (ref: string) => any;
  mapProperties: <T>(
    callback?: (
      schema: Schema<
        Decorator,
        Component,
        DecoratorProps,
        ComponentProps,
        Pattern,
        Display,
        Validator,
        Message
      >,
      key: SchemaKey,
      index: number,
    ) => T,
  ) => T[];
  mapPatternProperties: <T>(
    callback?: (
      schema: Schema<
        Decorator,
        Component,
        DecoratorProps,
        ComponentProps,
        Pattern,
        Display,
        Validator,
        Message
      >,
      key: SchemaKey,
      index: number,
    ) => T,
  ) => T[];
  reduceProperties: <P, R>(
    callback?: (
      buffer: P,
      schema: Schema<
        Decorator,
        Component,
        DecoratorProps,
        ComponentProps,
        Pattern,
        Display,
        Validator,
        Message
      >,
      key: SchemaKey,
      index: number,
    ) => R,
    predicate?: P,
  ) => R;
  reducePatternProperties: <P, R>(
    callback?: (
      buffer: P,
      schema: Schema<
        Decorator,
        Component,
        DecoratorProps,
        ComponentProps,
        Pattern,
        Display,
        Validator,
        Message
      >,
      key: SchemaKey,
      index: number,
    ) => R,
    predicate?: P,
  ) => R;
  compile: (scope?: any) => Schema<any, any, any, any, any, any, any, any, any>;
  fromJSON: (
    json: ISchema<
      Decorator,
      Component,
      DecoratorProps,
      ComponentProps,
      Pattern,
      Display,
      Validator,
      Message
    >,
  ) =>
    | this
    | (import('./types').Stringify<{
        version?: string;
        name?: string | number;
        title?: Message;
        description?: Message;
        default?: any;
        readOnly?: boolean;
        writeOnly?: boolean;
        type?: SchemaTypes;
        enum?: SchemaEnum<Message>;
        const?: any;
        multipleOf?: number;
        maximum?: number;
        exclusiveMaximum?: number;
        minimum?: number;
        exclusiveMinimum?: number;
        maxLength?: number;
        minLength?: number;
        pattern?: string | RegExp;
        maxItems?: number;
        minItems?: number;
        uniqueItems?: boolean;
        maxProperties?: number;
        minProperties?: number;
        required?: string | boolean | string[];
        format?: string;
        $ref?: string;
        $namespace?: string;
        definitions?: Record<string, import('./types').Stringify<any>>;
        properties?: Record<string, import('./types').Stringify<any>>;
        items?: import('./types').SchemaItems<
          Decorator,
          Component,
          DecoratorProps,
          ComponentProps,
          Pattern,
          Display,
          Validator,
          Message
        >;
        additionalItems?: import('./types').Stringify<any>;
        patternProperties?: Record<string, import('./types').Stringify<any>>;
        additionalProperties?: import('./types').Stringify<any>;
        'x-index'?: number;
        'x-pattern'?: Pattern;
        'x-display'?: Display;
        'x-validator'?: Validator;
        'x-decorator'?: (string & {}) | ((...args: any[]) => any) | Decorator;
        'x-decorator-props'?: DecoratorProps;
        'x-component'?: (string & {}) | ((...args: any[]) => any) | Component;
        'x-component-props'?: ComponentProps;
        'x-reactions'?: import('./types').SchemaReactions<any>;
        'x-content'?: any;
        'x-visible'?: boolean;
        'x-hidden'?: boolean;
        'x-disabled'?: boolean;
        'x-editable'?: boolean;
        'x-read-only'?: boolean;
        'x-read-pretty'?: boolean;
      }> &
        Schema<any, any, any, any, any, any, any, any, any>);
  toJSON: (
    recursion?: boolean,
  ) => ISchema<
    Decorator,
    Component,
    DecoratorProps,
    ComponentProps,
    Pattern,
    Display,
    Validator,
    Message
  >;
  toFieldProps: (options?: ISchemaTransformerOptions) => IFieldFactoryProps<any, any>;
  static getOrderProperties: (schema?: ISchema, propertiesName?: keyof ISchema) => any[];
  static compile: (expression: any, scope?: any) => any;
  static shallowCompile: (expression: any, scope?: any) => any;
  static isSchemaInstance: (
    value: any,
  ) => value is Schema<any, any, any, any, any, any, any, any, any>;
  static registerCompiler: (compiler: (expression: string, scope: any) => any) => void;
  static registerPatches: (...args: import('./types').SchemaPatch[]) => void;
  static registerVoidComponents: (components: string[]) => void;
  static registerTypeDefaultComponents: (maps: Record<string, string>) => void;
  static registerPolyfills: (version: string, patch: import('./types').SchemaPatch) => void;
  static enablePolyfills: (versions?: string[]) => void;
  static silent: (value?: boolean) => void;
}
