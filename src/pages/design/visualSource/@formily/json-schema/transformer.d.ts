import { Schema } from './schema';
import { ISchemaFieldFactoryOptions } from './types';
import { IFieldFactoryProps } from '@/pages/design/visualSource/@formily/core';
export declare const transformSchemaToFieldProps: (
  schema: Schema,
  options: ISchemaFieldFactoryOptions,
) => IFieldFactoryProps<any, any>;
