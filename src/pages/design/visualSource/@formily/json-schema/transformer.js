var __assign =
  (this && this.__assign) ||
  function () {
    __assign =
      Object.assign ||
      function (t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
      };
    return __assign.apply(this, arguments);
  };
var __read =
  (this && this.__read) ||
  function (o, n) {
    var m = typeof Symbol === 'function' && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o),
      r,
      ar = [],
      e;
    try {
      while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    } catch (error) {
      e = { error: error };
    } finally {
      try {
        if (r && !r.done && (m = i['return'])) m.call(i);
      } finally {
        if (e) throw e.error;
      }
    }
    return ar;
  };
/* istanbul ignore file */
import { untracked, autorun, observable } from '@/pages/design/visualSource/@formily/reactive';
import {
  isBool,
  isArr,
  isStr,
  FormPath,
  isValid,
  toArr,
  isEqual,
  each,
  isFn,
  isPlainObj,
  reduce,
  isEmpty,
} from '@/pages/design/visualSource/@formily/shared';
import { getValidateLocale } from '@/pages/design/visualSource/@formily/validator';
import { Schema } from './schema';
import {
  onFieldInit,
  onFieldMount,
  onFieldUnmount,
  onFieldValueChange,
  onFieldInputValueChange,
  onFieldInitialValueChange,
  onFieldValidateStart,
  onFieldValidateEnd,
  onFieldValidateFailed,
  onFieldValidateSuccess,
} from '@/pages/design/visualSource/@formily/core';
var FieldEffects = {
  onFieldInit: onFieldInit,
  onFieldMount: onFieldMount,
  onFieldUnmount: onFieldUnmount,
  onFieldValueChange: onFieldValueChange,
  onFieldInputValueChange: onFieldInputValueChange,
  onFieldInitialValueChange: onFieldInitialValueChange,
  onFieldValidateStart: onFieldValidateStart,
  onFieldValidateEnd: onFieldValidateEnd,
  onFieldValidateFailed: onFieldValidateFailed,
  onFieldValidateSuccess: onFieldValidateSuccess,
};
var getValidator = function (schema) {
  var rules = [];
  if (schema.format) {
    rules.push({ format: schema.format });
  }
  if (isValid(schema.maxItems)) {
    rules.push({ max: schema.maxItems });
  }
  if (isValid(schema.minItems)) {
    rules.push({ min: schema.minItems });
  }
  if (isValid(schema.maxLength)) {
    rules.push({ max: schema.maxLength });
  }
  if (isValid(schema.minLength)) {
    rules.push({ min: schema.minLength });
  }
  if (isValid(schema.maximum)) {
    rules.push({ maximum: schema.maximum });
  }
  if (isValid(schema.minimum)) {
    rules.push({ minimum: schema.minimum });
  }
  if (isValid(schema.exclusiveMaximum)) {
    rules.push({ exclusiveMaximum: schema.exclusiveMaximum });
  }
  if (isValid(schema.exclusiveMinimum)) {
    rules.push({ exclusiveMinimum: schema.exclusiveMinimum });
  }
  if (isValid(schema.pattern)) {
    rules.push({ pattern: schema.pattern });
  }
  if (isValid(schema.const)) {
    rules.push({
      validator: function (value, rule, ctx, format) {
        if (isEmpty(value)) return '';
        return value === schema.const ? '' : format(getValidateLocale('schema.const'), schema);
      },
    });
  }
  if (isValid(schema.multipleOf)) {
    rules.push({
      validator: function (value, rule, ctx, format) {
        if (isEmpty(value)) return '';
        return value % schema.multipleOf === 0
          ? ''
          : format(getValidateLocale('schema.multipleOf'), schema);
      },
    });
  }
  if (isValid(schema.maxProperties)) {
    rules.push({
      validator: function (value, rule, ctx, format) {
        if (isEmpty(value)) return '';
        return Object.keys(value || {}).length <= schema.maxProperties
          ? ''
          : format(getValidateLocale('schema.maxProperties'), schema);
      },
    });
  }
  if (isValid(schema.minProperties)) {
    rules.push({
      validator: function (value, rule, ctx, format) {
        if (isEmpty(value)) return '';
        return Object.keys(value || {}).length >= schema.minProperties
          ? ''
          : format(getValidateLocale('schema.minProperties'), schema);
      },
    });
  }
  if (isValid(schema.uniqueItems)) {
    rules.push({
      validator: function (value, rule, ctx, format) {
        value = toArr(value);
        return value.some(function (item, index) {
          for (var start = index; start < value.length; start++) {
            if (isEqual(value[start], item)) {
              return false;
            }
          }
        })
          ? format(getValidateLocale('schema.uniqueItems'), schema)
          : '';
      },
    });
  }
  if (isValid(schema['x-validator'])) {
    rules = rules.concat(schema['x-validator']);
  }
  if (rules.length) return rules;
};
var getDataSource = function (schema) {
  if (isArr(schema['enum'])) {
    return schema['enum'].map(function (item) {
      if (typeof item === 'object') {
        return item;
      } else {
        return {
          label: item,
          value: item,
        };
      }
    });
  }
};
var findComponent = function (type, path, options, state) {
  var _a;
  var component =
    (_a = state === null || state === void 0 ? void 0 : state[type]) === null || _a === void 0
      ? void 0
      : _a[0];
  if (path && (options === null || options === void 0 ? void 0 : options.components)) {
    if (FormPath.isPathPattern(path)) {
      component = FormPath.getIn(options.components, path);
      if (!component) {
        //Todo: need to use __DEV__ keyword
        console.error(
          "[Formily JSON Schema]: Cannot find the '" +
            path +
            "' component mapped by Schema.x-" +
            type,
        );
      }
    }
  }
  if (
    isFn(path) ||
    ((path === null || path === void 0 ? void 0 : path['$$typeof']) && isFn(path['type']))
  ) {
    return path;
  }
  return component;
};
var getBaseProps = function (schema, options, state) {
  var _a, _b;
  var props = {};
  var validator = getValidator(schema);
  var dataSource = getDataSource(schema);
  var editable = isValid(schema['x-editable']) ? schema['x-editable'] : schema['writeOnly'];
  var readOnly = isValid(schema['x-read-only']) ? schema['x-read-only'] : schema['readOnly'];
  var decoratorType = findComponent('decorator', schema['x-decorator'], options, state);
  var decoratorProps =
    schema['x-decorator-props'] ||
    ((_a = state === null || state === void 0 ? void 0 : state.decorator) === null || _a === void 0
      ? void 0
      : _a[1]);
  var componentType = findComponent('component', schema['x-component'], options, state);
  var componentProps =
    schema['x-component-props'] ||
    ((_b = state === null || state === void 0 ? void 0 : state.component) === null || _b === void 0
      ? void 0
      : _b[1]);
  if (isValid(schema.default)) {
    props.initialValue = schema.default;
  }
  if (isValid(schema.title)) {
    props.title = schema.title;
  }
  if (isValid(schema.description)) {
    props.description = schema.description;
  }
  if (isValid(schema['x-disabled'])) {
    props.disabled = schema['x-disabled'];
  }
  if (isValid(schema['x-read-pretty'])) {
    props.readPretty = schema['x-read-pretty'];
  }
  if (isValid(schema['x-visible'])) {
    props.visible = schema['x-visible'];
  }
  if (isValid(schema['x-hidden'])) {
    props.hidden = schema['x-hidden'];
  }
  if (isValid(schema['x-display'])) {
    props.display = schema['x-display'];
  }
  if (isValid(schema['x-pattern'])) {
    props.pattern = schema['x-pattern'];
  }
  if (isValid(validator)) {
    props.validator = validator;
  }
  if (isValid(dataSource)) {
    props.dataSource = dataSource;
  }
  if (isValid(editable)) {
    props.editable = editable;
  }
  if (isValid(readOnly)) {
    props.readOnly = readOnly;
  }
  if (isValid(decoratorType)) {
    props.decorator = [decoratorType, decoratorProps];
  }
  if (isValid(componentType)) {
    props.component = [componentType, componentProps];
  }
  return props;
};
var patchState = function (state, target) {
  var patch = function (target, path) {
    if (isPlainObj(target)) {
      each(target, function (value, key) {
        patch(value, path.concat(key));
      });
    } else {
      FormPath.setIn(state, path, target);
    }
  };
  untracked(function () {
    patch(target, []);
  });
  return state;
};
var getRequired = function (schema) {
  if (isBool(schema.required)) {
    return schema.required;
  }
  var parent = schema.parent;
  while (parent) {
    if (isStr(parent.required)) {
      if (FormPath.parse(parent.required).match(schema.name)) return true;
    } else if (isArr(parent.required)) {
      if (
        parent.required.some(function (parent) {
          return FormPath.parse(parent).match(schema.name);
        })
      ) {
        return true;
      }
    }
    parent = parent.parent;
  }
  return undefined;
};
var getReactions = function (schema, options) {
  var setSchemaFieldState = function (field, request, compile) {
    if (!request) return;
    if (request.state) {
      field.setState(function (state) {
        return patchState(state, compile(request.state));
      });
    }
    if (request.schema) {
      field.setState(function (state) {
        return patchState(
          state,
          getBaseProps(patchState({}, compile(request.schema)), options, state),
        );
      });
    }
  };
  var setSchemaFieldStateByTarget = function (form, target, request, compile) {
    if (!request) return;
    if (request.state) {
      form.setFieldState(target, function (state) {
        return patchState(state, compile(request.state, state));
      });
    }
    if (request.schema) {
      form.setFieldState(target, function (state) {
        return patchState(
          state,
          getBaseProps(patchState({}, compile(request.schema, state)), options, state),
        );
      });
    }
  };
  var queryDependency = function (field, pattern, property) {
    var _a = __read(String(pattern).split(/\s*#\s*/), 2),
      target = _a[0],
      path = _a[1];
    return field.query(target).getIn(path || property || 'value');
  };
  var parseDependencies = function (field, dependencies) {
    if (isArr(dependencies)) {
      var results_1 = [];
      dependencies.forEach(function (pattern) {
        if (isStr(pattern)) {
          results_1.push(queryDependency(field, pattern));
        } else if (isPlainObj(pattern)) {
          if (pattern.name && pattern.source) {
            results_1[pattern.name] = queryDependency(field, pattern.source, pattern.property);
          }
        }
      });
      return results_1;
    } else if (isPlainObj(dependencies)) {
      return reduce(
        dependencies,
        function (buf, pattern, key) {
          buf[key] = queryDependency(field, pattern);
          return buf;
        },
        {},
      );
    }
    return [];
  };
  return function (field) {
    var reactions = toArr(schema['x-reactions']);
    reactions.forEach(function (reaction) {
      var _a;
      if (!reaction) return;
      if (isFn(reaction)) {
        return reaction(field);
      }
      var run = function () {
        var _a, _b, _c, _d;
        var $self = field;
        var $form = field.form;
        var $deps = parseDependencies(field, reaction.dependencies);
        var $dependencies = $deps;
        var $observable = function (target, deps) {
          return autorun.memo(function () {
            return observable(target);
          }, deps);
        };
        var $props = function (props) {
          return field.setComponentProps(props);
        };
        var $effect = autorun.effect;
        var $memo = autorun.memo;
        var scope = __assign(__assign({}, options.scope), {
          $target: null,
          $form: $form,
          $self: $self,
          $deps: $deps,
          $dependencies: $dependencies,
          $observable: $observable,
          $effect: $effect,
          $memo: $memo,
          $props: $props,
        });
        var compile = function (expression, target) {
          if (target) {
            scope.$target = target;
          }
          return Schema.compile(expression, scope);
        };
        var when = Schema.compile(
          reaction === null || reaction === void 0 ? void 0 : reaction.when,
          scope,
        );
        var condition = isValid(reaction === null || reaction === void 0 ? void 0 : reaction.when)
          ? when
          : true;
        if (condition) {
          if (reaction.target) {
            setSchemaFieldStateByTarget(field.form, reaction.target, reaction.fulfill, compile);
          } else {
            setSchemaFieldState(field, reaction.fulfill, compile);
          }
          if (isStr((_a = reaction.fulfill) === null || _a === void 0 ? void 0 : _a.run)) {
            compile(
              '{{function(){' +
                ((_b = reaction.fulfill) === null || _b === void 0 ? void 0 : _b.run) +
                '}}}',
            )();
          }
        } else {
          if (reaction.target) {
            setSchemaFieldStateByTarget(field.form, reaction.target, reaction.otherwise, compile);
          } else {
            setSchemaFieldState(field, reaction.otherwise, compile);
          }
          if (isStr((_c = reaction.otherwise) === null || _c === void 0 ? void 0 : _c.run)) {
            compile(
              '{{async function(){' +
                ((_d = reaction.otherwise) === null || _d === void 0 ? void 0 : _d.run) +
                '}}}',
            )();
          }
        }
      };
      if (reaction.target) {
        reaction.effects = ((_a = reaction.effects) === null || _a === void 0 ? void 0 : _a.length)
          ? reaction.effects
          : ['onFieldInit', 'onFieldValueChange'];
      }
      if (reaction.effects) {
        each(reaction.effects, function (type) {
          if (FieldEffects[type]) {
            untracked(function () {
              FieldEffects[type](field.address, run);
            });
          }
        });
      } else {
        run();
      }
    });
  };
};
export var transformSchemaToFieldProps = function (schema, options) {
  var required = getRequired(schema);
  var reactions = getReactions(schema, options);
  var props = getBaseProps(schema, options);
  props.required = required;
  props.name = schema.name;
  props.reactions = [reactions];
  return props;
};
//# sourceMappingURL=transformer.js.map
