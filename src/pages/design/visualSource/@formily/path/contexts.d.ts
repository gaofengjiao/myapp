export declare type Context = {
  flag: string;
  [key: string]: any;
};
export declare const bracketContext: Context;
export declare const bracketArrayContext: Context;
export declare const bracketDContext: Context;
export declare const parenContext: Context;
export declare const braceContext: Context;
export declare const destructorContext: Context;
