import { Segments, Node, DestrcutorRules } from './types';
declare type Mutatators = {
  getIn: (segments: Segments, source: any) => any;
  setIn: (segments: Segments, source: any, value: any) => void;
  deleteIn?: (segments: Segments, source: any) => any;
  existIn?: (segments: Segments, source: any, start: number) => boolean;
};
export declare const getDestructor: (source: string) => any;
export declare const setDestructor: (source: string, rules: DestrcutorRules) => void;
export declare const parseDestructorRules: (node: Node) => DestrcutorRules;
export declare const setInByDestructor: (
  source: any,
  rules: DestrcutorRules,
  value: any,
  mutators: Mutatators,
) => void;
export declare const getInByDestructor: (
  source: any,
  rules: DestrcutorRules,
  mutators: Mutatators,
) => {};
export declare const deleteInByDestructor: (
  source: any,
  rules: DestrcutorRules,
  mutators: Mutatators,
) => void;
export declare const existInByDestructor: (
  source: any,
  rules: DestrcutorRules,
  start: number,
  mutators: Mutatators,
) => boolean;
export {};
