var __assign =
  (this && this.__assign) ||
  function () {
    __assign =
      Object.assign ||
      function (t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
      };
    return __assign.apply(this, arguments);
  };
var __read =
  (this && this.__read) ||
  function (o, n) {
    var m = typeof Symbol === 'function' && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o),
      r,
      ar = [],
      e;
    try {
      while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    } catch (error) {
      e = { error: error };
    } finally {
      try {
        if (r && !r.done && (m = i['return'])) m.call(i);
      } finally {
        if (e) throw e.error;
      }
    }
    return ar;
  };
var __spread =
  (this && this.__spread) ||
  function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
  };
import React, { Fragment } from 'react';
import { observer } from '@/pages/design/visualSource/@formily/reactive-react';
import { isFn } from '@/pages/design/visualSource/@formily/shared';
import { isVoidField } from '@/pages/design/visualSource/@formily/core';
var ReactiveInternal = function (props) {
  if (!props.field) {
    return React.createElement(
      Fragment,
      null,
      isFn(props.children) ? props.children(null, null) : props.children,
    );
  }
  var field = props.field;
  var children = isFn(props.children)
    ? props.children(props.field, props.field.form)
    : props.children;
  if (field.display !== 'visible') return null;
  var renderDecorator = function (children) {
    var _a;
    if (!field.decorator[0]) {
      return React.createElement(Fragment, null, children);
    }
    return React.createElement(
      field.decorator[0],
      __assign(__assign({}, field.decorator[1]), {
        style: __assign(
          {},
          (_a = field.decorator[1]) === null || _a === void 0 ? void 0 : _a.style,
        ),
      }),
      children,
    );
  };
  var renderComponent = function () {
    var _a, _b, _c, _d;
    if (!field.component[0]) return React.createElement(Fragment, null, children);
    var value = !isVoidField(field) ? field.value : undefined;
    var onChange = !isVoidField(field)
      ? function () {
          var _a, _b;
          var args = [];
          for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
          }
          field.onInput.apply(field, __spread(args));
          (_b = (_a = field.component[1]) === null || _a === void 0 ? void 0 : _a.onChange) ===
            null || _b === void 0
            ? void 0
            : _b.call.apply(_b, __spread([_a], args));
        }
      : (_a = field.component[1]) === null || _a === void 0
      ? void 0
      : _a.onChange;
    var onFocus = !isVoidField(field)
      ? function () {
          var _a, _b;
          var args = [];
          for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
          }
          field.onFocus.apply(field, __spread(args));
          (_b = (_a = field.component[1]) === null || _a === void 0 ? void 0 : _a.onFocus) ===
            null || _b === void 0
            ? void 0
            : _b.call.apply(_b, __spread([_a], args));
        }
      : (_b = field.component[1]) === null || _b === void 0
      ? void 0
      : _b.onFocus;
    var onBlur = !isVoidField(field)
      ? function () {
          var _a, _b;
          var args = [];
          for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
          }
          field.onBlur.apply(field, __spread(args));
          (_b = (_a = field.component[1]) === null || _a === void 0 ? void 0 : _a.onBlur) ===
            null || _b === void 0
            ? void 0
            : _b.call.apply(_b, __spread([_a], args));
        }
      : (_c = field.component[1]) === null || _c === void 0
      ? void 0
      : _c.onBlur;
    var disabled = !isVoidField(field)
      ? field.pattern === 'disabled' || field.pattern === 'readPretty'
      : undefined;
    var readOnly = !isVoidField(field) ? field.pattern === 'readOnly' : undefined;
    return React.createElement(
      field.component[0],
      __assign(__assign({ disabled: disabled, readOnly: readOnly }, field.component[1]), {
        style: __assign(
          {},
          (_d = field.component[1]) === null || _d === void 0 ? void 0 : _d.style,
        ),
        value: value,
        onChange: onChange,
        onFocus: onFocus,
        onBlur: onBlur,
      }),
      children,
    );
  };
  return renderDecorator(renderComponent());
};
ReactiveInternal.displayName = 'ReactiveField';
export var ReactiveField = observer(ReactiveInternal, {
  forwardRef: true,
});
//# sourceMappingURL=ReactiveField.js.map
