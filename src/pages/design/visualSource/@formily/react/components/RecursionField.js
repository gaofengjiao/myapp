var __assign =
  (this && this.__assign) ||
  function () {
    __assign =
      Object.assign ||
      function (t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
      };
    return __assign.apply(this, arguments);
  };
import React, { Fragment, useContext, useMemo } from 'react';
import { isFn, isValid } from '@/pages/design/visualSource/@formily/shared';
import { Schema } from '@/pages/design/visualSource/@formily/json-schema';
import { SchemaContext, SchemaOptionsContext, SchemaExpressionScopeContext } from '../shared';
import { useField } from '../hooks';
import { ObjectField } from './ObjectField';
import { ArrayField } from './ArrayField';
import { Field } from './Field';
import { VoidField } from './VoidField';
export var RecursionField = function (props) {
  var _a;
  var schema = new Schema(props.schema);
  var parent = useField();
  var options = useContext(SchemaOptionsContext);
  var scope = useContext(SchemaExpressionScopeContext);
  var fieldSchema = useMemo(
    function () {
      return schema === null || schema === void 0 ? void 0 : schema.compile(scope);
    },
    [schema],
  );
  var fieldProps = useMemo(
    function () {
      return fieldSchema === null || fieldSchema === void 0
        ? void 0
        : fieldSchema.toFieldProps(__assign(__assign({}, options), { scope: scope }));
    },
    [fieldSchema],
  );
  var getBasePath = function () {
    if (props.onlyRenderProperties) {
      return (
        props.basePath ||
        (parent === null || parent === void 0 ? void 0 : parent.address.concat(props.name))
      );
    }
    return props.basePath || (parent === null || parent === void 0 ? void 0 : parent.address);
  };
  var basePath = getBasePath();
  var children =
    fieldSchema['x-content'] ||
    ((_a = fieldSchema['x-component-props']) === null || _a === void 0 ? void 0 : _a['children']);
  var renderProperties = function (field) {
    if (props.onlyRenderSelf) return;
    return React.createElement(
      Fragment,
      null,
      fieldSchema.mapProperties(function (item, name, index) {
        var base = (field === null || field === void 0 ? void 0 : field.address) || basePath;
        var schema = item;
        if (isFn(props.mapProperties)) {
          var mapped = props.mapProperties(item, name);
          if (mapped) {
            schema = mapped;
          }
        }
        if (isFn(props.filterProperties)) {
          if (props.filterProperties(schema, name) === false) {
            return null;
          }
        }
        return React.createElement(RecursionField, {
          schema: schema,
          key: index + '-' + name,
          name: name,
          basePath: base,
        });
      }),
      children,
    );
  };
  var render = function () {
    if (!isValid(props.name)) return renderProperties();
    if (fieldSchema.type === 'object') {
      if (props.onlyRenderProperties) return renderProperties();
      return React.createElement(
        ObjectField,
        __assign({}, fieldProps, { name: props.name, basePath: basePath }),
        renderProperties,
      );
    } else if (fieldSchema.type === 'array') {
      return React.createElement(
        ArrayField,
        __assign({}, fieldProps, { name: props.name, basePath: basePath }),
        children,
      );
    } else if (fieldSchema.type === 'void') {
      if (props.onlyRenderProperties) return renderProperties();
      return React.createElement(
        VoidField,
        __assign({}, fieldProps, { name: props.name, basePath: basePath }),
        renderProperties,
      );
    }
    return React.createElement(
      Field,
      __assign({}, fieldProps, { name: props.name, basePath: basePath }),
      children,
    );
  };
  if (!fieldSchema) return React.createElement(Fragment, null);
  return React.createElement(SchemaContext.Provider, { value: fieldSchema }, render());
};
//# sourceMappingURL=RecursionField.js.map
