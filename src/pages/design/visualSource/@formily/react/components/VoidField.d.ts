/// <reference types="react" />
import { JSXComponent, IVoidFieldProps } from '../types';
export declare const VoidField: {
  <D extends JSXComponent, C extends JSXComponent>(
    props: IVoidFieldProps<
      D,
      C,
      import('@/pages/design/visualSource/@formily/core').VoidField<any, any, any>
    >,
  ): JSX.Element;
  displayName: string;
};
