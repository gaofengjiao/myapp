/// <reference types="@/visualSource/@formily/core" />
/// <reference types="@/visualSource/@formily/json-schema" />
import * as Types from './types';
declare global {
  namespace Formily.React {
    export { Types };
  }
}
