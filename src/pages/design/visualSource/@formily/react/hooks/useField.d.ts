import { GeneralField } from '@/pages/design/visualSource/@formily/core';
export declare const useField: <T = GeneralField>() => T;
