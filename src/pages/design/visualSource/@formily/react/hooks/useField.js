import { useContext } from 'react';
import { FieldContext } from '../shared';
export var useField = function () {
  return useContext(FieldContext);
};
//# sourceMappingURL=useField.js.map
