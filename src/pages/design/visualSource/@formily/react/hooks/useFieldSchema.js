import { useContext } from 'react';
import { SchemaContext } from '../shared';
export var useFieldSchema = function () {
  return useContext(SchemaContext);
};
//# sourceMappingURL=useFieldSchema.js.map
