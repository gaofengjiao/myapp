import { Form } from '@/pages/design/visualSource/@formily/core';
export declare const useForm: <T extends object = any>() => Form<T>;
