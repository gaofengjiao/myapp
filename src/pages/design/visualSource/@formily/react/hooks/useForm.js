import { useContext } from 'react';
import { FormContext } from '../shared';
export var useForm = function () {
  var form = useContext(FormContext);
  if (!form) {
    throw new Error('Can not found form instance from context.');
  }
  return form;
};
//# sourceMappingURL=useForm.js.map
