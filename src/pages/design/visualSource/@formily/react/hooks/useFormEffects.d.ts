import { Form } from '@/pages/design/visualSource/@formily/core';
export declare const useFormEffects: (effects?: (form: Form) => void) => void;
