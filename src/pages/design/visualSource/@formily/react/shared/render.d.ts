import React from 'react';
export declare const render: (
  element: React.ReactElement,
) => React.ReactPortal | React.DetailedReactHTMLElement<{}, HTMLElement>;
