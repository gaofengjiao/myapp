export declare const useDidUpdate: (callback?: () => void) => void;
