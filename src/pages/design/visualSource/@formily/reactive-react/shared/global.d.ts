export declare const globalThisPolyfill: Window;
declare global {
  export class FinalizationRegistry<T> {
    constructor(cleanup: (cleanupToken: T) => void);
    register(object: object, cleanupToken: T, token: T): void;
    unregister(object: object): void;
  }
}
