export declare const immediate: (callback?: () => void) => () => void;
