interface IValue<T = any> {
  value?: T;
}
export interface IComputed {
  <T>(compute: () => T): IValue<T>;
  <T>(compute: { get?: () => T; set?: (value: T) => void }): IValue<T>;
}
export declare const computed: IComputed;
export {};
