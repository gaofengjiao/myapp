import { batchStart, batchEnd, batchScopeStart, batchScopeEnd } from './reaction';
import { createBoundaryAnnotation } from './internals';
export var batch = createBoundaryAnnotation(batchStart, batchEnd);
batch.scope = createBoundaryAnnotation(batchScopeStart, batchScopeEnd);
//# sourceMappingURL=batch.js.map
