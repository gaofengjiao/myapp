import { ObservableListener, Reaction, ReactionsMap } from './types';
import { DataNode } from './datatree';
export declare const ProxyRaw: WeakMap<object, any>;
export declare const RawProxy: WeakMap<object, any>;
export declare const RawShallowProxy: WeakMap<object, any>;
export declare const RawNode: WeakMap<object, DataNode>;
export declare const RawReactionsMap: WeakMap<object, ReactionsMap>;
export declare const ReactionStack: Reaction[];
export declare const BatchCount: {
  value: number;
};
export declare const UntrackCount: {
  value: number;
};
export declare const BatchScope: {
  value: boolean;
};
export declare const PendingReactions: Set<Reaction>;
export declare const PendingScopeReactions: Set<Reaction>;
export declare const MakeObservableSymbol: unique symbol;
export declare const ObserverListeners: Set<ObservableListener>;
