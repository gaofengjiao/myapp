export declare const collectionHandlers: {
  get(target: any, key: PropertyKey, receiver: any): any;
};
export declare const baseHandlers: ProxyHandler<any>;
