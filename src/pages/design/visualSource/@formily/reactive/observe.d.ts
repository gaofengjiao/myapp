import { DataChange } from './datatree';
export declare const observe: (
  target: object,
  observer?: (change: DataChange) => void,
  deep?: boolean,
) => () => void;
