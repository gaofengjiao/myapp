export declare function deprecate<P1 = any, P2 = any, P3 = any, P4 = any, P5 = any>(
  method: any,
  message?: string,
  help?: string,
): (p1?: P1, p2?: P2, p3?: P3, p4?: P4, p5?: P5) => any;
