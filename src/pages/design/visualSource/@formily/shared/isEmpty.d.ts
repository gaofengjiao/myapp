export declare const isValid: (val: any) => boolean;
export declare function isEmpty(val: any, strict?: boolean): boolean;
