import {
  Path as FormPath,
  Pattern as FormPathPattern,
} from '@/pages/design/visualSource/@formily/path';
export { FormPath, FormPathPattern };
