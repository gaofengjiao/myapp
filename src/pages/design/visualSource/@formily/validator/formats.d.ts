declare const _default: {
  url: RegExp;
  email: RegExp;
  ipv6: RegExp;
  ipv4: RegExp;
  number: RegExp;
  integer: RegExp;
  qq: RegExp;
  phone: RegExp;
  idcard: RegExp;
  money: RegExp;
  zh: RegExp;
  date: RegExp;
  zip: RegExp;
};
export default _default;
