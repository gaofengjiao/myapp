import { IValidateResult, IValidatorRules } from './types';
export declare const render: (result: IValidateResult, rules: IValidatorRules) => IValidateResult;
