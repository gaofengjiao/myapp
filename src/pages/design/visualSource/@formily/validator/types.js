export var isValidateResult = function (obj) {
  return !!obj['type'] && !!obj['message'];
};
//# sourceMappingURL=types.js.map
