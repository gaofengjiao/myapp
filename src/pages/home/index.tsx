import type { FC } from 'react';
import React, { useState, useEffect } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { Row, Col, Card, Tabs, Divider, Typography, Input, Button, message, Tooltip } from 'antd';
import ProList from '@ant-design/pro-list';
import _ from 'lodash';
import type { Dispatch} from 'umi';
import { Link } from 'umi';
import './index.less';

import cj1 from '@/assets/icons/cj1.png';
import yw1 from '@/assets/icons/yw1.png';
import person1 from '@/assets/icons/persion1.png';
import person2 from '@/assets/icons/person2.png';
import person3 from '@/assets/icons/person3.png';
import person4 from '@/assets/icons/person4.png';
import person5 from '@/assets/icons/person5.png';
import more1 from '@/assets/icons/more1.png';

const { Meta } = Card;
const { TabPane } = Tabs;
const { Paragraph } = Typography;
const { Search } = Input;
const { Grid } = Card;
const headPics = [person1, person2, person3, person4, person5];
interface AtomicHomeProp {
  dispatch?: Dispatch;
}

const AtomicHome: FC<AtomicHomeProp> = (props) => {
  const { dispatch } = props;
  const [formData, setFormData] = useState<any>(); // 业务流程
  const [bpmData, setbpmData] = useState<any>(); // 流程组件
  const [ruleData, setRuleData] = useState<any>(); // 规则组件
  const [interfaceConf, setInterfaceConf] = useState<any>(); // 接口组件

  const [latelyForJson, setLatelyForJson] = useState<any>([]); // 最近使用
  const [oftenForJson, setOftenForJson] = useState<any>([]); // 最常使用
  const [operationLog, setOperationLog] = useState([]); // 基础列表

  const [HLWGoods, setHLWGoods] = useState([]);
  const [YTWGoods, setYTWGoods] = useState([]);
  useEffect(() => {
    dispatch &&
      dispatch({
        type: 'componentManage/home/getStatistic',
        callback: (data: any) => {
          if (data.code === 200 && data.data) {
            const { form, bpm, interfaceConf, rule } = data.data;

            form && setFormData(data.data.form);
            rule && setRuleData(data.data.rule);
            bpm && setbpmData(data.data.bpm);
            interfaceConf && setInterfaceConf(data.data.interfaceConf);
          } else {
            message.error(data?.msg);
          }
        },
      });

    dispatch &&
      dispatch({
        type: 'componentManage/home/getUsageHistory',
        callback: (data: any) => {
          if (data.code === 200) {
            if (data.data && data.data.latelyForJson && Array.isArray(data.data.latelyForJson)) {
              setLatelyForJson(data.data.latelyForJson);
            }
            if (data.data && data.data.oftenForJson && Array.isArray(data.data.oftenForJson)) {
              setOftenForJson(data.data.oftenForJson);
            }
          } else {
            message.error(data?.msg);
          }
        },
      });

    dispatch &&
      dispatch({
        type: 'componentManage/home/getOperationLog',
        callback: (data: any) => {
          if (data.code === 200) {
            if (data.data && Array.isArray(data.data)) {
              setOperationLog(data.data);
            }
          } else {
            message.error(data?.msg);
          }
        },
      });

    dispatch &&
      dispatch({
        type: 'componentManage/home/getBpmMatchingByGoodsNodeId',
        callback: (data: any) => {
          if (data.code === 200) {
            console.log(data.data);
            if (data.data && data.data.HLWGoods && Array.isArray(data.data.HLWGoods)) {
              setHLWGoods(data.data.HLWGoods);
            }
            if (data.data && data.data.YTWGoods && Array.isArray(data.data.YTWGoods)) {
              setYTWGoods(data.data.YTWGoods);
            }
          } else {
            message.error(data?.msg);
          }
        },
      });
  }, [dispatch]);

  const handleRenderCardFooter = (data: any) => {
    const nodes = data.map((item: any, index: number) => (
      <span key={index}>
        {item.name}:{item.typeCount || 0}
      </span>
    ));
    return (
      <Tooltip title={nodes}>
        <ul className="footer ellipsisRowText">{nodes}</ul>
      </Tooltip>
    );
  };
  return (
    <PageContainer title={false} className="tydic-componentsManage-home">
      <Row style={{ marginTop: 12 }}>
        <Col span={17}>
          <Row gutter={16} className="comp-head-static">
            <Col span={6} key="1">
              <Card key="1" bordered={false} className="screen">
                <Meta
                  style={{ textAlign: 'center' }}
                  title="视图"
                  description={
                    <span className="blue">
                      {formData && formData.formCount ? formData.formCount : 0}
                    </span>
                  }
                />
              </Card>
              {formData && formData.formInfo && handleRenderCardFooter(formData.formInfo)}
            </Col>
            <Col span={6} key="2">
              <Card bordered={false} className="pageT">
                <Meta
                  style={{ textAlign: 'center' }}
                  title="接口"
                  description={
                    <span className="green">
                      {interfaceConf && interfaceConf.interfaceConfCount
                        ? interfaceConf.interfaceConfCount
                        : 0}
                    </span>
                  }
                />
              </Card>
              {interfaceConf &&
                interfaceConf.interfaceConfInfo &&
                handleRenderCardFooter(interfaceConf.interfaceConfInfo)}
            </Col>
            <Col span={6} key="3">
              <Card bordered={false} className="atom">
                <Meta
                  style={{ textAlign: 'center' }}
                  title="流程"
                  description={
                    <span className="violet">
                      {bpmData && bpmData.bpmCount ? bpmData.bpmCount : 0}
                    </span>
                  }
                />
              </Card>
              {bpmData && bpmData.bpmInfo && handleRenderCardFooter(bpmData.bpmInfo)}
            </Col>
            <Col span={6} key="4">
              <Card bordered={false} className="pageY">
                <Meta
                  style={{ textAlign: 'center' }}
                  title="规则"
                  description={
                    <span className="violet">
                      {ruleData && ruleData.ruleCount ? ruleData.ruleCount : 0}
                    </span>
                  }
                />
              </Card>
              {ruleData && ruleData.ruleInfo && handleRenderCardFooter(ruleData.ruleInfo)}
            </Col>
          </Row>

          <div className="tempContent">
            <Tabs>
              <TabPane tab="最近使用" key="latest">
                <Card>
                  {latelyForJson.map((item: any, index: number) => {
                    return (
                      <Grid key={`latest-${index}`} hoverable={false} style={{ width: '33.333%' }}>
                        <span className="title">
                          <img
                            width="28"
                            height="28"
                            src={index % 2 === 0 ? cj1 : yw1}
                            alt={item.title}
                          />
                          <span style={{ paddingLeft: 8 }}>{item.title}</span>
                        </span>
                        <Paragraph>描述: {item.describe}</Paragraph>
                        <Row justify="space-between">
                          <span className="auxiliary">操作人:{item.user}</span>
                          <span className="auxiliary">{item.createTime}</span>
                        </Row>
                      </Grid>
                    );
                  })}
                </Card>
                {latelyForJson && latelyForJson.length ? (
                  <Row justify="center" style={{ marginTop: 16, marginBottom: 16 }}>
                    <Link to="/configMgt/processConfig/processMgt" target="_blank">
                      <img width="16" height="16" src={more1} />
                      更多模板
                    </Link>
                  </Row>
                ) : null}
              </TabPane>
              <TabPane tab="最常使用" key="useful">
                <Card>
                  {oftenForJson.map((item: any, index: number) => {
                    return (
                      <Grid key={`latest-${index}`} hoverable={false} style={{ width: '33.333%' }}>
                        <span className="title">
                          <img
                            width="28"
                            height="28"
                            src={index % 2 === 0 ? cj1 : yw1}
                            alt={item.title}
                          />
                          <span style={{ paddingLeft: 8 }}>{item.title}</span>
                        </span>
                        <Paragraph>{item.describe}</Paragraph>
                        <Row justify="space-between">
                          <span className="auxiliary">操作人:{item.user}</span>
                          <span className="auxiliary">
                            {item.createTime && item.createTime ? item.createTime : null}
                          </span>
                        </Row>
                      </Grid>
                    );
                  })}
                </Card>
                {oftenForJson && oftenForJson.length ? (
                  <Row justify="center" style={{ marginTop: 16, marginBottom: 16 }}>
                    <Link to="/configMgt/processConfig/processMgt">
                      <img width="16" height="16" src={more1} />
                      更多模板
                    </Link>
                  </Row>
                ) : null}
              </TabPane>
            </Tabs>
          </div>
          <div className="dtContent">
            <ProList<any>
              toolBarRender={() => {
                return [
                  <Link to="#">
                    <img width="16" height="16" src={more1} />
                    更多日志
                  </Link>,
                ];
              }}
              rowKey="name"
              headerTitle="日志列表"
              dataSource={operationLog}
              showActions="hover"
              metas={{
                title: {
                  dataIndex: 'operName',
                },
                avatar: {
                  dataIndex: 'image',
                  render: (text, record, index) => (
                    <img src={headPics[index % 5]} width="48" height="48" />
                  ),
                },
                description: {
                  dataIndex: 'operTime',
                },
                subTitle: {
                  dataIndex: 'title',
                  // render:(text:any,record:any)=>{
                  //   if(Number(record.businessType)=== 10){
                  //     return `添加${text}`
                  //   }else if(Number(record.businessType)=== 11){
                  //     return `删除${text}`
                  //   }
                  //   else if(Number(record.businessType)=== 12){
                  //     return `发布${text}`
                  //   }
                  //   return null
                  // }
                },
              }}
            />
          </div>
        </Col>

        <Col span={7} className="rightCol">
          <div className="rightTop">
            <Card size="small" title="快捷操作" bordered={false}>
              <Row>
                <Col span="8">
                  <Button type="primary">
                    <Link to="/visualization/process">新建流程</Link>
                  </Button>
                </Col>
                <Col span="8">
                  <Button type="primary">
                    <Link to="/visualization/form?from=business">新建表单</Link>
                  </Button>
                </Col>
                <Col span="8">
                  <Button type="primary">
                    <Link to="/configMgt/ruleConfig/rule/add">新建规则</Link>
                  </Button>
                </Col>
              </Row>
              <Row style={{ marginTop: 8 }}>
                <Col span="8">
                  <Button type="primary">
                    <Link to="/configMgt/processConfig/processMgt">发布流程</Link>
                  </Button>
                </Col>
                <Col span="8">
                  <Button type="primary">
                    <Link to="/configMgt/compManageConfig/atomicManage'">发布表单</Link>
                  </Button>
                </Col>
                <Col span="8">
                  <Button type="primary">
                    <Link to="/configMgt/ruleConfig/rule">发布规则</Link>
                  </Button>
                </Col>
              </Row>
            </Card>

            <Card size="small" title="页面模板场景分类" bordered={false} style={{ marginTop: 16 }}>
              <Search placeholder="请输入" />
              <div className="cate">
                <h4 className="subTitle"> {`基础通信类 > 基础通信类 > 以太网专线`}</h4>

                <Row style={{ rowGap: 6 }}>
                  {YTWGoods.map((item: any) => (
                    <Col span={8} key={item.id}>
                      <Button size="small" type="default">
                        <Link to={`/configMgt/processConfig/processMgt?id=${item.id}`}>
                          {item.tradeTypeName}
                        </Link>
                      </Button>
                    </Col>
                  ))}
                </Row>
              </div>
              <div className="cate">
                <h4 className="subTitle"> {`基础通信类 > 互联网接入类 > 互联网专线`}</h4>
                <Row style={{ rowGap: 6 }}>
                  {HLWGoods.map((item: any) => (
                    <Col span={8} key={item.id}>
                      <Button size="small" type="default">
                        <Link to={`/configMgt/processConfig/processMgt?id=${item.id}`}>
                          {item.tradeTypeName}
                        </Link>
                      </Button>
                    </Col>
                  ))}
                </Row>
              </div>
            </Card>
          </div>
          <Card size="small" title="帮助" bordered={false} className="help">
            <a className="help-item">场景模板库是什么？</a>
            <a className="help-item">页面模板库是什么？</a>
            <a className="help-item">业务组件库是什么？</a>
            <a className="help-item">原子组件库是什么？</a>
            <a className="help-item">如何快速同步新商品的原子组件？</a>
            <a className="help-item">页面模板库发布异常如何处理？</a>
            <a className="help-item">模板可视化配置操作指南。</a>
            <a className="help-item">如何快速克隆拷贝已有模板。</a>
            <Divider />
            <a className="help-item">查看更多</a>
          </Card>
        </Col>
      </Row>
    </PageContainer>
  );
};

export default AtomicHome;
