import request from '@/utils/request';
import { serviceConfigUrls } from '@/api/application';

export async function serviceList(data: any, options?: Record<string, any>) {
  return request<any>(serviceConfigUrls.serviceList, {
    method: 'POST',
    data,
    ...(options || {}),
  });
}

export async function modelTypeList(options?: Record<string, any>) {
  return request<any>(serviceConfigUrls.modelTypeList, {
    method: 'POST',
    ...(options || {}),
  });
}

export async function modelBusinessAttrList(data: any, options?: Record<string, any>) {
  return request<any>(serviceConfigUrls.modelBusinessAttrList, {
    method: 'POST',
    data,
    ...(options || {}),
  });
}

export async function modelBusinessSave(data: any, options?: Record<string, any>) {
  return request<any>(serviceConfigUrls.modelBusinessSave, {
    method: 'POST',
    data,
    ...(options || {}),
  });
}
export async function modelTypeDiagramDetail(data: any, options?: Record<string, any>) {
  return request<any>(serviceConfigUrls.modelTypeDiagramDetail, {
    method: 'POST',
    data,
    ...(options || {}),
  });
}
