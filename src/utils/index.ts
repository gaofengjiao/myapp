import { parse } from 'querystring';
import { message } from 'antd';

export const getPageQuery = () => parse(window.location.href.split('?')[1]);

export const getStatus = (result: any, fun?: any) => {
  if (result && result.status === '0') {
    fun && fun();
    message.success(result.message);
  } else {
    fun && fun();
    message.error(result.message);
  }
};
